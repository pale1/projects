package frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import gui.DrawingPane;
import gui.SimulationWorker;
import models.CornerGroup;

public class ChaosGame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private static final int WIDTH = 600;
	private static final int HEIGHT = 600;
	
	private Point cursor = new Point(WIDTH/2, HEIGHT/2);
	
	private Map<Color, CornerGroup> cornerGroups = new HashMap<>();
	
	private CornerGroupChooser cgc = new CornerGroupChooser();
	
	private SimulationWorker worker;
	private boolean isRunning = false;
	
	public ChaosGame() {
		setLayout(new BorderLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocation(50, 50);
		setTitle("Chaos Game");
		
		DrawingPane dp = new DrawingPane(WIDTH, HEIGHT);

		// NORTH PANEL //

		JSlider simSpeed = new JSlider();
		simSpeed.setMinimum(0);
		simSpeed.setMaximum(1000);
		
		JButton selectCornerGroup = new JButton("Corner group");
		JToggleButton addCorner = new JToggleButton("Add corner");
		JButton startSim = new JButton("Start simulation");
		JButton refresh = new JButton(new ImageIcon(new ImageIcon(ChaosGame.class.getResource("/refresh.png")).getImage().getScaledInstance(17, 17, java.awt.Image.SCALE_SMOOTH)));
		JButton dotColor = new JButton("Dot Color");
		
		dp.addColorChangedListener(c -> dotColor.setBackground(c));
		dp.setDotColor(Color.WHITE);
		cgc.addColorChangedListener(c -> selectCornerGroup.setBackground(c));
		cgc.selectDefaultColor();
		
		selectCornerGroup.addActionListener(e -> {
			if(cgc.isVisible())
				cgc.setVisible(false);
			else
				cgc.setVisible(true);
		});
		startSim.addActionListener(e -> {
			if(isRunning) {
				worker.cancel(true);
				simSpeed.removeChangeListener(worker);
				
				startSim.setText("Start Simulation");
				
				isRunning = false;
			} else {
				worker = new SimulationWorker(cursor, dp.getImageModel(), simSpeed.getValue(), cornerGroups.values());
				simSpeed.addChangeListener(worker);
				worker.execute();
				
				startSim.setText("Stop Simulation");
				
				isRunning = true;
			}
		});
		dp.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				if(!addCorner.isSelected()) // if not in add mode, return
					return;
				
				// inconsistency between image size and panel size
				int modX = (int)(e.getX()
						*dp.getImageModel().getImage().getWidth(null)
						/dp.getSize().getWidth());
				int modY = (int)(e.getY()
						*dp.getImageModel().getImage().getHeight(null)
						/dp.getSize().getHeight());
				
				// draw corner
				Color col = selectCornerGroup.getBackground();
				dp.drawCorner(modX, modY, col);
				
				// add it to corner group
				CornerGroup group = cornerGroups.get(col);
				if(group == null) group = new CornerGroup();
				group.addCorner(new Point(modX, modY));
				cornerGroups.put(col, group);
			}
			
		});
		refresh.addActionListener(e -> {
			if(isRunning)
				startSim.doClick(); // stop simulation
			
			cornerGroups.clear();
			dp.clearImage();
		});
		dotColor.addActionListener(e -> {
			dp.setDotColor(JColorChooser.showDialog(
                    null,
                    "Choose dot color",
                    Color.WHITE));
		});
		
		JPanel np = new JPanel(new FlowLayout());
		np.add(selectCornerGroup);
		np.add(addCorner);
		np.add(startSim);
		np.add(simSpeed);
		np.add(refresh);
		np.add(dotColor);
		add(np, BorderLayout.NORTH);

		// CENTER PANEL //

		add(dp, BorderLayout.CENTER);
		
		pack();

	}

	public static void main(String[] args) {

		SwingUtilities.invokeLater(() -> new ChaosGame().setVisible(true));
	}
}
