package frames;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JRadioButton;

import listeners.ColorChangedListener;

public class CornerGroupChooser extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private List<ColorChangedListener> listeners = new ArrayList<>();
	
	private static final Color[] COLORS = {
			Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW,
			Color.ORANGE, Color.RED, Color.PINK, Color.MAGENTA
	};
	
	private ButtonGroup buttonGroup = new ButtonGroup();
	
	public CornerGroupChooser() {
		setLayout(new GridLayout(0, 2));
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		setSize(150, 100);
		
		for(Color c : COLORS) {
			JRadioButton rb = new JRadioButton();
			rb.setBackground(c);
			rb.addActionListener(l -> {
				for(ColorChangedListener li : listeners)
					li.colorChanged(c);
			});
			
			buttonGroup.add(rb);
			add(rb);
		}
	}
	
	public Color getSelectedColor() {
		JRadioButton selected = (JRadioButton) buttonGroup.getSelection();
		return selected != null ? selected.getBackground() : COLORS[0];
	}
	
	public void selectDefaultColor() {
		((JRadioButton)buttonGroup.getElements().nextElement()).doClick();		
	}
	
	public void addColorChangedListener(ColorChangedListener l) {
		listeners.add(l);
	}
	
	public void removeColorChangedListener(ColorChangedListener l) {
		listeners.remove(l);
	}
}
