package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import listeners.ColorChangedListener;
import models.BufferedImageModel;
import models.ImageModel;

public class DrawingPane extends JLabel {
	private static final long serialVersionUID = 1L;
	
	private List<ColorChangedListener> listeners = new ArrayList<>();
	
	private ImageModel image;
	
	public DrawingPane(int w, int h) {
		image = new BufferedImageModel(w, h);
		image.addImageChangedListener(e -> repaint());
		
		setIcon(new ImageIcon(image.getImage()));
	}
	
	@Override
	public void paint(Graphics g) {
		g.drawImage(image.getImage(), 0, 0, getWidth(), getHeight(), this);
	}
	
	public ImageModel getImageModel() {
		return image;
	}

	public void drawCorner(int x, int y, Color color) {
		image.drawCorner(x, y, color);
	}
	
	public void clearImage() {
		image.clearImage();
	}
	
	public void setDotColor(Color color) {
		if(color == null)
			return;
		
		image.setDotColor(color);
		
		for(ColorChangedListener l : listeners)
			l.colorChanged(color);
	}
	
	public Color getDotColor() {
		return image.getDotColor();
	}
	
	public void addColorChangedListener(ColorChangedListener ccl) {
		listeners.add(ccl);
	}
	
	public void removeColorChangedListener(ColorChangedListener ccl) {
		listeners.remove(ccl);
	}
	
}
