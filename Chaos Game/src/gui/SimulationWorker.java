package gui;

import java.awt.Point;
import java.util.Collection;

import javax.swing.JSlider;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import models.CornerGroup;
import models.ImageModel;

public class SimulationWorker extends SwingWorker<Void, Void> implements ChangeListener {

    private Point cursor;
    private ImageModel image;
    private int value;
    private Collection<CornerGroup> cornerGroups;
    
    private CornerGroup activeGroup;

    public SimulationWorker(Point cursor, ImageModel image, int value, Collection<CornerGroup> cornerGroups) {
        this.cursor = cursor;
        this.image = image;
        this.value = value;
        this.cornerGroups = cornerGroups;
    }

    @Override
    protected Void doInBackground() throws Exception {
    	activeGroup = cornerGroups.iterator().next(); // set first group as active
    	
    	while(!isCancelled()){
    		
    		paintPixel();
    		
    		sleep();
    	}
    	return null;
    }

    private void paintPixel() {
    	
    	// in 20% of cases chooses another group
    	if(Math.random() < 0.2) {
            int i = (int) (Math.random()*cornerGroups.size());
            activeGroup = (CornerGroup) cornerGroups.toArray()[i];
    	}
    	
        int i = (int) (Math.random()*activeGroup.getCorners().size());
        Point corner = activeGroup.getCorners().get(i);

        cursor.setLocation(cursor.x + (corner.x-cursor.x)/2, cursor.y + (corner.y-cursor.y)/2);

        image.drawDot(cursor.x, cursor.y);
    }

    private void sleep() throws InterruptedException {
        if(value > 480) {
            double rand = Math.random();
            if(value <= (Math.log(1-rand)/-8.)*520+480)
                Thread.sleep(1);
        } else if(value > 100) {
            Thread.sleep((int)(25.-0.05*value));
        } else
            Thread.sleep((int)(500.-value*4.8));
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        value = ((JSlider)e.getSource()).getValue();
    }
}

