package listeners;

import java.awt.Color;

public interface ColorChangedListener {
	
	void colorChanged(Color color);

}
