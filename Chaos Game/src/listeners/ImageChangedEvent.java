package listeners;

import java.awt.Color;

public class ImageChangedEvent {
	public int x, y, w, h;
	public Color c;
	
	public ImageChangedEvent(int x, int y, int w, int h, Color c) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.c = c;
	}

}
