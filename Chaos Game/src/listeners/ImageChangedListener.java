package listeners;

public interface ImageChangedListener {
	
	/**
	 * Executed when image has been changed
	 * @param x where image change occured
	 * @param y where image change occured
	 * @param w width of area changed
	 * @param h height of area changed
	 * @param c color the area has been repainted to
	 */
	void imageChanged(ImageChangedEvent e);
	
}
