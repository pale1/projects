package models;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import listeners.ImageChangedEvent;
import listeners.ImageChangedListener;

public class BufferedImageModel implements ImageModel {
	
	private List<ImageChangedListener> listeners = new ArrayList<>();
	
	private static final Color DEF_BG_COLOR = Color.BLACK;
	private static final Color DEF_DOT_COLOR = Color.WHITE;
	
	private BufferedImage image;
	private Graphics graphics;
	
	private Color bgColor = DEF_BG_COLOR;
	private Color dotColor = DEF_DOT_COLOR;
	private int dotRGB;
	
	public BufferedImageModel(int w, int h) {
		this.dotRGB = dotColor.getRGB();
		this.image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		this.graphics = image.getGraphics();
		
		clearImage();
	}

	@Override
	public void drawDot(int x, int y) {
		image.setRGB(x, y, dotRGB);
		
		for(ImageChangedListener l : listeners)
			l.imageChanged(new ImageChangedEvent(x, y, 1, 1, dotColor));
	}
	
	@Override
	public void drawCorner(int x, int y, Color color) {
		graphics.setColor(color);
		graphics.fillRect(x-1, y-1, 3, 3);
		
		for(ImageChangedListener l : listeners)
			l.imageChanged(new ImageChangedEvent(x-1, y-1, 3, 3, dotColor));
	}
	
	@Override
	public void clearImage() {
		graphics.setColor(bgColor);
		graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		
		for(ImageChangedListener l : listeners)
			l.imageChanged(new ImageChangedEvent(0, 0, image.getWidth(), image.getHeight(), bgColor));
	}

	@Override
	public void setDotColor(Color color) {
		this.dotColor = color;
		this.dotRGB = color.getRGB();
	}

	@Override
	public Color getDotColor() {
		return dotColor;
	}

	@Override
	public Image getImage() {
		return image;
	}

	@Override
	public void addImageChangedListener(ImageChangedListener icl) {
		listeners.add(icl);
	}

	@Override
	public void removeImageChangedListener(ImageChangedListener icl) {
		listeners.remove(icl);
	}

}
