package models;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class CornerGroup {
	private List<Point> corners = new ArrayList<>(5);
	
	public CornerGroup() {}
	
	public CornerGroup(Point... corners) {
		for(Point c : corners)
			this.corners.add(c);
	}
	
	public CornerGroup(List<Point> corners) {
		this.corners = corners;
	}

	public void addCorner(Point corner) {
		corners.add(corner);
	}
	
	public List<Point> getCorners() {
		return corners;
	}
}
