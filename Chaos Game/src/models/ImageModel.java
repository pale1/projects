package models;

import java.awt.Color;
import java.awt.Image;

import listeners.ImageChangedListener;

public interface ImageModel {
	
	void drawDot(int x, int y);
	
	void drawCorner(int x, int y, Color color);
	
	void clearImage();
	
	void setDotColor(Color color);
	
	Color getDotColor();
	
	Image getImage();
	
	void addImageChangedListener(ImageChangedListener icl);
	
	void removeImageChangedListener(ImageChangedListener icl);
}
