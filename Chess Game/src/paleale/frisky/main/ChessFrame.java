package paleale.frisky.main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;

public class ChessFrame extends JFrame {
	
	private static final long serialVersionUID = -5470311483056436162L;
	private JButton newMatchButton = new JButton("New Match");
	private JToggleButton[][] tiles = new JToggleButton[8][8];
	private JToggleButton selected = null;
	private ButtonGroup grp = new ButtonGroup();
	private Border greenBorder = BorderFactory.createLineBorder(Color.GREEN, 3),
				   orangeBorder = BorderFactory.createLineBorder(Color.ORANGE, 3);
	private JLabel turnLabel = new JLabel();
	private JPanel np = new JPanel();
	private JPanel cp = new JPanel(new GridLayout(0, 8));
	private boolean turn; //True for white, false for black
	private boolean[] pawns = new boolean[16];
	
	public ChessFrame() {
		ActionListener l = e -> {
			JToggleButton source = (JToggleButton) e.getSource();
			boolean moveMade = false;
			int y = (int)source.getMinimumSize().getWidth(), 
				x = (int)source.getMinimumSize().getHeight();
			if(source.getBorder().equals(orangeBorder) || source.getBorder().equals(greenBorder)) {
				if(pawns[x] && source.getActionCommand().equals("WPawn")) pawns[x] = false;
				else if(pawns[x+8] && source.getActionCommand().equals("BPawn")) pawns[x+8] = false;
				source.setIcon(selected.getIcon());	
				selected.setIcon(null);
				if(source.getActionCommand().endsWith("King")) {
					String winner = source.getActionCommand().startsWith("W") ? "Black" : "White";
					JOptionPane.showMessageDialog(this, winner+" player won!", "Match end", JOptionPane.PLAIN_MESSAGE);
				}
				source.setActionCommand(selected.getActionCommand());
				selected.setActionCommand("");
				moveMade = true;
				changeTurn();
			}
			selected = source;
			for(int i = 0; i < 8; i++) for(int j = 0; j < 8; j++) tiles[i][j].setBorder(UIManager.getBorder("Button.border"));
			if(!moveMade) {	
				String type = e.getActionCommand();
				if(!type.isEmpty()) {
					if(turn) { //White turn
						if(type.equals("WPawn")) {
							if(tiles[(y+7)%8][(x+7)%8].getActionCommand().startsWith("B")) tiles[(y+7)%8][(x+7)%8].setBorder(orangeBorder);
							if(tiles[(y+7)%8][(x+9)%8].getActionCommand().startsWith("B")) tiles[(y+7)%8][(x+9)%8].setBorder(orangeBorder);
							if(tiles[(y+7)%8][x].getActionCommand().isEmpty())tiles[(y+7)%8][x].setBorder(greenBorder);
							if(y == 6 && pawns[x] && tiles[y-2][x].getActionCommand().isEmpty()) tiles[y-2][x].setBorder(greenBorder);
						} else if(type.equals("WRook")) {
							checkRook(x, y, "B");
						} else if(type.equals("WKnight")) {
							checkKnight(x, y, "B");
						} else if(type.equals("WBishop")) {
							checkBishop(x, y, "B");
						} else if(type.equals("WQueen")) {
							checkRook(x, y, "B");
							checkBishop(x, y, "B");
						} else if(type.equals("WKing")) {
							checkKing(x, y, "B");
						}
					} else { //Black turn
						if(type.equals("BPawn")) {
							if(tiles[(y+9)%8][(x+7)%8].getActionCommand().startsWith("W")) tiles[(y+9)%8][(x+7)%8].setBorder(orangeBorder);
							if(tiles[(y+9)%8][(x+9)%8].getActionCommand().startsWith("W")) tiles[(y+9)%8][(x+9)%8].setBorder(orangeBorder);
							if(tiles[(y+9)%8][x].getActionCommand().isEmpty())tiles[(y+9)%8][x].setBorder(greenBorder);
							if(y == 1 && pawns[x+8] && tiles[y+2][x].getActionCommand().isEmpty()) tiles[y+2][x].setBorder(greenBorder);
						} else if(type.equals("BRook")) {
							checkRook(x, y, "W");
						} else if(type.equals("BKnight")) {
							checkKnight(x, y, "W");
						} else if(type.equals("BBishop")) {
							checkBishop(x, y, "W");
						} else if(type.equals("BQueen")) {
							checkRook(x, y, "W");
							checkBishop(x, y, "W");
						} else if(type.equals("BKing")) {
							checkKing(x, y, "W");
						}
					}
				}
			}
		};
		for(int i = 0; i < 8; i++) {
			for(int j = 0; j < 8; j++) {
				tiles[i][j] = new JToggleButton();
				tiles[i][j].setPreferredSize(new Dimension(50, 50));
				tiles[i][j].addActionListener(l);
				tiles[i][j].setMinimumSize(new Dimension(i, j)); //Da se zna index gumba
				grp.add(tiles[i][j]);
				if((i+j)%2 == 1) tiles[i][j].setBackground(Color.BLACK);
				else tiles[i][j].setBackground(Color.WHITE);
				cp.add(tiles[i][j]);
			}
		}
		turnLabel.setOpaque(true);
		np.add(turnLabel);
		np.add(newMatchButton);
		add(np, BorderLayout.NORTH);
		add(cp, BorderLayout.CENTER);
		
		newMatchButton.addActionListener(li -> {
			Object[] options = {"Yes", "No"};
			int answer = JOptionPane.showOptionDialog(this,
				"Do you want to start a new match?", "",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null,
				options, options[1]);
			if (answer == JOptionPane.YES_OPTION) {
				createMatch();
			}
        });
	}
	
	private void changeTurn() {
		turn = !turn;
		if(turn) {
			turnLabel.setBackground(Color.WHITE);
			turnLabel.setForeground(Color.BLACK);
			turnLabel.setText("White turn");
		} else {
			turnLabel.setBackground(Color.BLACK);
			turnLabel.setForeground(Color.WHITE);
			turnLabel.setText("Black turn");
		}
	}
	
	private void checkBishop(int x, int y, String prefix) {
		int cnt;
		for(int i = 0; i < 4; i++) { // 4 orientations : NW NE SE SW
			try {
				cnt = 1;
				if(i==0) do { // NW
					if(checkTile(tiles[y-cnt][x-cnt], prefix)) break;
					cnt++;
				} while(true);
				else if(i==1) do { // NE
					if(checkTile(tiles[y-cnt][x+cnt], prefix)) break;
					cnt++;
				} while(true);
				else if(i==2) do { // SE
					if(checkTile(tiles[y+cnt][x+cnt], prefix)) break;
					cnt++;
				} while(true);
				else if(i==3) do { // SW
					if(checkTile(tiles[y+cnt][x-cnt], prefix)) break;
					cnt++;
				} while(true);
			} catch(Exception ex) {}
		}
	}
	
	private void checkKing(int x, int y, String prefix) {
		if(y-1 >= 0 && x-1 >= 0) checkTile(tiles[y-1][x-1], prefix);
		if(y-1 >= 0) checkTile(tiles[y-1][x], prefix);
		if(y-1 >= 0 && x+1 < 8) checkTile(tiles[y-1][x+1], prefix);
		if(x+1 < 8) checkTile(tiles[y][x+1], prefix);
		if(y+1 < 8 && x+1 < 8) checkTile(tiles[y+1][x+1], prefix);
		if(y+1 < 8) checkTile(tiles[y+1][x], prefix);
		if(y+1 < 8 && x-1 >= 0) checkTile(tiles[y+1][x-1], prefix);
		if(x-1 >= 0) checkTile(tiles[y][x-1], prefix);
	}
	
	private void checkKnight(int x, int y, String prefix) {
		if(y-2 >= 0 && x-1 >= 0) checkTile(tiles[y-2][x-1], prefix);
		if(y-2 >= 0 && x+1 < 8) checkTile(tiles[y-2][x+1], prefix);
		if(y-1 >= 0 && x+2 < 8) checkTile(tiles[y-1][x+2], prefix);
		if(y+1 < 8 && x+2 < 8) checkTile(tiles[y+1][x+2], prefix);
		if(y+2 < 8 && x-1 >= 0) checkTile(tiles[y+2][x-1], prefix);
		if(y+2 < 8 && x+1 < 8) checkTile(tiles[y+2][x+1], prefix);
		if(y+1 < 8 && x-2 >= 0) checkTile(tiles[y+1][x-2], prefix);
		if(y-1 >= 0 && x-2 >= 0) checkTile(tiles[y-1][x-2], prefix);
	}
	
	private void checkRook(int x, int y, String prefix) {
		int cnt;
		for(int i = 0; i < 4; i++) { // 4 orientations : N E S W
			try {
				cnt = 1;
				if(i==0) do { // N
					if(checkTile(tiles[y-cnt][x], prefix)) break;
					cnt++;
				} while(true);
				else if(i==1) do { // E
					if(checkTile(tiles[y][x+cnt], prefix)) break;
					cnt++;
				} while(true);
				else if(i==2) do { // S
					if(checkTile(tiles[y+cnt][x], prefix)) break;
					cnt++;
				} while(true);
				else if(i==3) do { // W
					if(checkTile(tiles[y][x-cnt], prefix)) break;
					cnt++;
				} while(true);
			} catch(Exception ex) {}
		}
	}
	
	/**
	 * Paints tile orange and returns true if it has enemy figure, else paints it green and returns false
	 */
	private boolean checkTile(JToggleButton tile, String prefix) {
		if(tile.getActionCommand().isEmpty()) tile.setBorder(greenBorder);
		else if(tile.getActionCommand().startsWith(prefix)) {
			tile.setBorder(orangeBorder);
			return true;
		} else return true;
		return false;
	}
	
	private void createMatch() {
		for(int i = 0; i < 16; i++) pawns[i] = true;
		setTile(tiles[0][0], "BRook");
		setTile(tiles[0][1], "BKnight");
		setTile(tiles[0][2], "BBishop");
		setTile(tiles[0][3], "BQueen");
		setTile(tiles[0][4], "BKing");
		setTile(tiles[0][5], "BBishop");
		setTile(tiles[0][6], "BKnight");
		setTile(tiles[0][7], "BRook");
		for(int i = 0; i < 8; i++) setTile(tiles[1][i], "BPawn");
		for(int i = 2; i < 6; i++) {
			for(int j = 0; j < 8; j++) {
				resetTile(tiles[i][j]);
			}
		}
		setTile(tiles[7][0], "WRook");
		setTile(tiles[7][1], "WKnight");
		setTile(tiles[7][2], "WBishop");
		setTile(tiles[7][3], "WQueen");
		setTile(tiles[7][4], "WKing");
		setTile(tiles[7][5], "WBishop");
		setTile(tiles[7][6], "WKnight");
		setTile(tiles[7][7], "WRook");
		for(int i = 0; i < 8; i++) setTile(tiles[6][i], "WPawn");
		
		turn = Math.random() < 0.5;
		changeTurn();
	}
	
	private void resetTile(JToggleButton tile) {
		tile.setActionCommand("");
		tile.setIcon(null);
	}
	
	private void setTile(JToggleButton tile, String command) {
		tile.setActionCommand(command);
		tile.setIcon(new ImageIcon(ChessFrame.class.getResource("/"+command+".png")));
	}
	
	public static void main(String[] args) {
		try {
			SwingUtilities.invokeAndWait(() -> {
				ChessFrame frame = new ChessFrame();
				frame.setIconImage(new ImageIcon(ChessFrame.class.getResource("/WKing.png")).getImage());
				frame.setTitle("JChess");
				frame.setLocation(100, 100);
				frame.pack();
				frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
				frame.setVisible(true);
			});
		} catch (InvocationTargetException | InterruptedException e) {}
	}
}