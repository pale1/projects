package paleale.frisky.listener;

import paleale.frisky.model.SingleDocumentModel;

/**
 * Listener that listens for changes to number of documents and currently selected document
 * @author alenp
 *
 */
public interface MultipleDocumentListener {
	
	/**
	 * Executed when currently selected document changes
	 * @param previousModel previously selected document
	 * @param currentModel currently selected document
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);
	
	/**
	 * Executed when a document is added
	 * @param model added document
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * Executed when a document is removed
	 * @param model removed document
	 */
	void documentRemoved(SingleDocumentModel model);

}
