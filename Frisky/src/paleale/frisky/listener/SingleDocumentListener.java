package paleale.frisky.listener;

import paleale.frisky.model.SingleDocumentModel;

/**
 * Listener that listens for changes to document modify status and path
 * @author alenp
 *
 */
public interface SingleDocumentListener {
	
	/**
	 * Executed when a document has its modify status updated
	 * @param model that had its modify status updated
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);
	
	/**
	 * Executed when document has its file path updated
	 * @param model that had its file path updated
	 */
	void documentFilePathUpdated(SingleDocumentModel model);

}
