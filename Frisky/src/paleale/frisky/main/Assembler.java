package paleale.frisky.main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.swing.SwingWorker;

import paleale.frisky.utils.CheckUtils;
import paleale.frisky.utils.CodingUtils;
import paleale.frisky.utils.PseudoUtils;

public class Assembler extends SwingWorker<Boolean, Void> {
	
	private final String code;
	private final FRISC frisc;
	private final Map<String, Integer> labels = new HashMap<>();
	private final ArrayList<ArrayList<String>> listOfInstrParts = new ArrayList<>();

	public Assembler(String code, FRISC frisc) {
		this.code = code;
		this.frisc = frisc;
	}
	
	@Override
	protected Boolean doInBackground() throws Exception {
		String[] instructions = code.split("\n");
		int row = 0; // Row counter
		int locCnt = 0;
		try {
			for(String instruction : instructions) { // Trim instructions and find labels
				ArrayList<String> instrParts = trimInstruction(instruction);
				locCnt = labelWork(instrParts, locCnt);
				row++;
			}
			
			row = 0;
			for(ArrayList<String> instrParts : listOfInstrParts) { // Code instructions
				int binInstr = codeInstruction(instrParts);
				if (binInstr != -1)
					frisc.storeInstr(binInstr);
				row++;
			}
		} catch(IllegalArgumentException e) {
			throw new IllegalArgumentException(e.getMessage()+" at row "+(row+1));
		}
		return true;
	}
	
	/**
	 * Check if instruction begins with non-recognizable text and adds it as a label.	<br>
	 * Then, remove label from instruction and store instruction parts in a list
	 * @param instrParts trimmed instruction
	 * @param addr8 address of byte where the instruction would be if it were in FRISC memory
	 * @return next addr8
	 */
	private int labelWork(ArrayList<String> instrParts, int addr8) {
		try {

			if(instrParts == null) {
				listOfInstrParts.add(null);
				return addr8;
			}
			if(getOpcodeForType(instrParts.get(0))!=-1 ||
					isConditionalOP(instrParts.get(0).split("_")[0])) { // Just instruction, no labels
				listOfInstrParts.add(instrParts);
				return addr8+4;
			}
			
			if(PseudoUtils.checkForORG(instrParts)) { // If ORG, change reading address
				listOfInstrParts.add(instrParts);
				return CheckUtils.getValue(instrParts.get(1), "address", false, null);
			}
			
			if(PseudoUtils.checkForEQU(instrParts, labels)) { // If EQU, add label
				listOfInstrParts.add(null);
				return addr8;
			}
			
			String label = instrParts.get(0);
			if(PseudoUtils.isPseudo(label)) { // If it's DW, DH, DB, DS, it's not a label
				listOfInstrParts.add(instrParts);
				return addr8+4;
			}
			try {
				label = label.substring(0, 10);
			} catch(IndexOutOfBoundsException ignored) {}
			CheckUtils.checkLabelFormat(label);
			if(labels.containsKey(label))
				throw new IllegalArgumentException("Label of the same name already exists");
			labels.put(label, addr8);
			instrParts.remove(0);
			
			listOfInstrParts.add(instrParts);
			return addr8+4;
			
		} catch(NumberFormatException e) {
			throw new IllegalArgumentException("Bad format");
		} catch(IndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Missing arguments");
		}
	}
	
	/**
	 * Translates assembly instruction into machine code
	 * @param instrParts parts of instruction
	 * @return 32-bit machine code, or -1 if it's not an instruction
	 * @throws IllegalArgumentException if such instruction does not exist
	 */
	private int codeInstruction(ArrayList<String> instrParts) throws IllegalArgumentException {
		if(instrParts == null || instrParts.size()==0) // Blank row
			return -1;
		String op = instrParts.get(0).split("_")[0];
		int opcode = getOpcodeForType(op);
		
		if(opcode == -1) { // Is not opcode, assume a pseudo
			if(PseudoUtils.checkPseudos(instrParts, frisc))
				return -1;
			else
				throw new IllegalArgumentException("Unknown instruction");
		}

		int binInstr = opcode << 27;
		for(int i = 1; i < instrParts.size(); i++) { // Replace SP with R7
			if(instrParts.get(i).equals("SP"))
				instrParts.set(i, "R7");
		}
		try {
			if(opcode <= 11) { // AL-instruction
				binInstr = CodingUtils.codeAL(instrParts, binInstr);
			} else if(opcode == 12) { // CMP
				binInstr = CodingUtils.codeCMP(instrParts, binInstr, labels);
			} else if(opcode == 13) { // MOVE
				binInstr = CodingUtils.codeMOVE(instrParts, binInstr, labels);
			} else if(opcode <= 16) { // LOAD-instruction
				binInstr = CodingUtils.codeMEM(instrParts, binInstr, "dest", labels);
			} else if(opcode <= 19) { // STORE-instruction
				binInstr = CodingUtils.codeMEM(instrParts, binInstr, "src", labels);
			} else if(opcode == 20) { // PUSH
				binInstr = CodingUtils.codeStackOP(instrParts, binInstr, "src");
			} else if(opcode == 21) { // POP
				binInstr = CodingUtils.codeStackOP(instrParts, binInstr, "dest");
			} else if(opcode <= 23) { // JP, CALL
				binInstr = CodingUtils.codeJumpOP(instrParts, binInstr, labels);
			} else if(opcode == 24) { // JR
				binInstr = CodingUtils.codeJR(instrParts, binInstr, labels, frisc);
			} else if(opcode <= 28) { // RET, RETI, RETN, HALT
				binInstr = CodingUtils.codeRETHALT(instrParts, binInstr);
			}
		} catch(NumberFormatException e) {
			throw new IllegalArgumentException("Bad format");
		} catch(IndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Missing arguments");
		}
		
		return binInstr;
	}
	
	/**
	 * Divides instruction into a list of parts, where part can be word/+/-/,/(/)
	 * @param instruction to be trimmed
	 * @return a list of parts
	 */
	private ArrayList<String> trimInstruction(String instruction) {

		String noComment = instruction.split(";")[0];

		if(noComment.isBlank())
			return null; // Blank row

		noComment = noComment.replace(",", " , ")
									.replace("(", " ( ")
									.replace(")", " ) ")
									.replace("+", " + ")
									.replace("%B ", "%B")
									.replace("%D ", "%D")
									.replace("%H ", "%H")
									.trim();

		String[] parts = noComment.split("\\s+");

		ArrayList<String> instrParts = new ArrayList<>(Arrays.asList(parts));

		for(int i = 1; i < instrParts.size(); i++) { // Replace SP with R7
			if(instrParts.get(i).equals("SP"))
				instrParts.set(i, "R7");
		}
		return instrParts;
	}

	/**
	 * Checks whether instruction name is of a conditional instruction
	 * @param instrName instruction name
	 * @return true if instruction with this name is conditional, false otherwise
	 */
	private static boolean isConditionalOP(String instrName) {
		return switch (instrName) {
			case "JP", "JR", "CALL", "RET", "RETI", "RETN", "HALT" -> true;
			default -> false;
		};
	}
	
	/**
	 * Get 5-bit number identifying instruction type
	 * @param instrName instruction name
	 * @return opcode
	 */
	private int getOpcodeForType(String instrName) {
		return switch (instrName) {
			case "ADD" -> 0;
			case "ADC" -> 1;
			case "SUB" -> 2;
			case "SBC" -> 3;
			case "AND" -> 4;
			case "OR" -> 5;
			case "XOR" -> 6;
			case "SHL" -> 7;
			case "SHR" -> 8;
			case "ASHR" -> 9;
			case "ROTL" -> 10;
			case "ROTR" -> 11;
			case "CMP" -> 12;
			case "MOVE" -> 13;
			case "LOAD" -> 14;
			case "LOADH" -> 15;
			case "LOADB" -> 16;
			case "STORE" -> 17;
			case "STOREH" -> 18;
			case "STOREB" -> 19;
			case "PUSH" -> 20;
			case "POP" -> 21;
			case "JP" -> 22;
			case "CALL" -> 23;
			case "JR" -> 24;
			case "RET" -> 25;
			case "RETI" -> 26;
			case "RETN" -> 27;
			case "HALT" -> 28;
			default -> -1;
		};
	}
}
