package paleale.frisky.main;

import paleale.frisky.utils.FRISCUtils;

public class FRISC extends FRISCUtils implements Runnable {
	
	private volatile boolean running = true;
	private volatile boolean paused = false;
	
	private final Object pauseLock = new Object();
	
	private volatile boolean debugMode = false;
	
	public FRISC(MemoryFrame memoryFrame, MainFrame mainFrame) {
		this.memoryFrame = memoryFrame;
		this.mainFrame = mainFrame;
	}
	
	/**
	 * Tries to store instruction into memory on the next write location
	 * @param binInstr instruction to store
	 * @return true if successful, false otherwise
	 */
	public void storeInstr(int binInstr) {
		store(binInstr, 4);
	}
	
	/**
	 * Shifts memory writing location to address specified by ORG pseudoinstruction
	 * @param address8 shift address
	 */
	public void org(int address8) { // For ORG
		if(Integer.compareUnsigned(address8, memwr<=3 ? 0 : memwr-3) < 0) // ORG points to earlier location, not possible
			throw new IllegalArgumentException("Invalid ORG address");
		memwr = address8;
		toNextWord();
	}
	
	@Override
	public void run() { // Execute the program
		int cnt = 0;
		long start = System.nanoTime();

		while(running) {
			synchronized (pauseLock) {
				if (!running) {break;}
				if (paused) {
					try {
						synchronized (pauseLock) {
							pauseLock.wait();
							}
						} catch (InterruptedException ex) {
							break;
						}
					if (!running) {break;}
				}
			}
			cnt++;

			executeInstruction();
			
			if(debugMode) {
				refresh();
				pause();
			}
		}
		System.out.println(System.nanoTime()-start);
		System.out.println((System.nanoTime()-start)/cnt);

		mainFrame.stopExecuting();
		refresh();
	}
	
	private void executeInstruction() {
		int binInstr;
		try {
			binInstr = mem.load(PC);
		} catch(Exception e) {
			error("PC value out of memory bounds");
			return;
		}
		IR = binInstr;
		PC += 4;
		
		int opcode = binInstr >>> 27;

		try {
			switch(opcode) {
			case 0: // ADD
				A(1, false);
				break;
			case 1: // ADC
				A(1, true);
				break;
			case 2: // SUB
				A(-1, false);
				break;
			case 3: // SBC
				A(-1, true);
				break;
			case 4: // AND
				L(0);
				break;
			case 5: // OR
				L(1);
				break;
			case 6: // XOR
				L(2);
				break;
			case 7: // SHL
				SH(0);
				break;
			case 8: // SHR
				SH(1);
				break;
			case 9: // ASHR
				SH(2);
				break;
			case 10: // ROTL
				ROT(true);
				break;
			case 11: // ROTR
				ROT(false);
				break;
			case 12:
				CMP();
				break;
			case 13:
				MOVE();
				break;
			case 14:
				LOAD(4);
				break;
			case 15:
				LOAD(2);
				break;
			case 16:
				LOAD(1);
				break;
			case 17:
				STORE(4);
				break;
			case 18:
				STORE(2);
				break;
			case 19:
				STORE(1);
				break;
			case 20:
				PUSH();
				break;
			case 21:
				POP();
				break;
			case 22:
				JP();
				break;
			case 23:
				CALL();
				break;
			case 24:
				JR();
				break;
			case 25:
				RET(0);
				break;
			case 26:
				RET(1);
				break;
			case 27:
				RET(2);
				break;
			case 28: // HALT
				if(isConditionFulfilled()) {
					running = false;
					printSuccess();
				}
			}
		} catch(IllegalArgumentException e) {
			errorWAddr(e.getMessage());
			return;
		} catch(Exception e) {
			errorWAddr(e.getMessage());
			return;
		}
	}
		
	/**
	 * Set memwr on the beginning of next memory word
	 */
	public void toNextWord() {
		if(memwr%4 != 0)
			memwr += 4-memwr%4;
	}
	
	private void refresh() {
		updateSR();
		memoryFrame.refresh();
	}
	
	public void stop() {
        running = false;
        resume();
    }

    public void pause() {
        paused = true;
    }
    
    public void debug(boolean b) {
    	debugMode = b;
    }

    public void resume() {
        synchronized (pauseLock) {
            paused = false;
            pauseLock.notifyAll(); // Unblocks thread
        }
    }
    
    private void errorWAddr(String s) {
    	error(s+" at memory address "+Integer.toHexString(PC-4).toUpperCase());
    }
    
    private void error(String s) {
    	printError(s);
		mainFrame.stopExecuting();
		refresh();
    }
    
    private void printError(String s) {
    	mainFrame.logArea.append(MainFrame.getDate()+"Execution error: "+s+"\n");
    }
    
    private void printSuccess() {
    	mainFrame.logArea.append(MainFrame.getDate()+"Execution successful.\n");
    }
}
