package paleale.frisky.main;

import paleale.frisky.model.DefaultMultipleDocumentModel;
import paleale.frisky.model.MultipleDocumentModel;
import paleale.frisky.utils.IOUtils;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.prefs.Preferences;

// TODO refactoring, nothing added except .listener and .model packages
public class MainFrame extends JFrame {
	private static final long serialVersionUID = -1616367561107980353L;
	
	private MultipleDocumentModel mdm = new DefaultMultipleDocumentModel();
	
	protected MemoryFrame memoryFrame = new MemoryFrame();

	private FRISC 		frisc = new FRISC(memoryFrame, this);
	private boolean 	first = true;
	
	private JTabbedPane tp = new JTabbedPane();
	private JTextArea 	codeArea = new JTextArea();
	JTextArea			logArea = new JTextArea();
	private JScrollPane scrollPane = new JScrollPane(codeArea),
						logScrollPane = new JScrollPane(logArea);
	private JSplitPane 	splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPane, logScrollPane);
	private JMenuBar 	menuBar = new JMenuBar();
	private JMenu 		fileMenu = new JMenu("File"),
						friscMenu = new JMenu("FRISC");
	private JMenuItem 	newFile = new JMenuItem("New"),
						load = new JMenuItem("Load"),
						save = new JMenuItem("Save"),
						saveAs = new JMenuItem("Save As"),
						chWorkspace = new JMenuItem("Change Workspace"),
						assemble = new JMenuItem("Assemble"),
						execute = new JMenuItem("Execute"),
						memory = new JMenuItem("Memory");
	
	private JCheckBoxMenuItem debugMode = new JCheckBoxMenuItem("Debug Mode");
	
	private final UndoManager undo = new UndoManager();
	
	private JButton nextInstr = new JButton("Next instruction");
	private String workspace, activeFile = "";
	private boolean isChanged = false;
	static Preferences userPref = Preferences.userRoot();

	public MainFrame() {
		setKeys();
		
		execute.setEnabled(false);
		memory.setEnabled(false);
		
		fileMenu.add(newFile);
		fileMenu.add(load);
		fileMenu.add(save);
		fileMenu.add(saveAs);
		fileMenu.add(chWorkspace);
		friscMenu.add(assemble);
		friscMenu.add(execute);
		friscMenu.add(memory);
		friscMenu.add(debugMode);
		menuBar.add(fileMenu);
		menuBar.add(friscMenu);
		nextInstr.setVisible(false);
		nextInstr.setEnabled(false);
		nextInstr.setMnemonic(KeyEvent.VK_ENTER);
		menuBar.add(nextInstr);
		newFile.addActionListener(l -> newFile());
		load.addActionListener(l -> loadFile());
		save.addActionListener(l -> saveFile(false));
		saveAs.addActionListener(l -> saveFile(true));
		chWorkspace.addActionListener(l -> selectWorkspace());
		assemble.addActionListener(l -> assemble());
		execute.addActionListener(l -> {
			if(execute.getText().startsWith("E"))
				execute();
			else
				stopExecuting();
		});
		setKeys();
		memory.addActionListener(l -> {
			if(memoryFrame.isVisible())
				memoryFrame.setVisible(false);
			else
				memoryFrame.setVisible(true);
		});
		debugMode.addActionListener(l -> {
			frisc.debug(debugMode.getState());
			if(!debugMode.isSelected()) {
				nextInstr.setVisible(false);
				nextInstr.setEnabled(false);
				frisc.resume();
			}
		});
		nextInstr.addActionListener(l -> {
			frisc.resume();
		});
		
		codeArea.setFont(new Font(Font.MONOSPACED, 0, 12));
		codeArea.getDocument().addUndoableEditListener(evt -> undo.addEdit(evt.getEdit()));
		codeArea.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				if(!isChanged) {
					isChanged = true;
					tp.setTitleAt(0, "*"+tp.getTitleAt(0));
				}
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				if(!isChanged) {
					isChanged = true;
					tp.setTitleAt(0, "*"+tp.getTitleAt(0));
				}
			}
			@Override
			public void changedUpdate(DocumentEvent e) {}
	    });
		TextLineNumber tln = new TextLineNumber(codeArea);
		
	    workspace = userPref.get("workspace", ""); // Load workspace location

		splitPane.setDividerLocation(400);
		scrollPane.setRowHeaderView(tln);
		logArea.setEditable(false);
		setJMenuBar(menuBar);
		tp.addTab(activeFile.isBlank() ? "Untitled" : activeFile, splitPane);
		add(tp, BorderLayout.CENTER);
	}
	
	private void newFile() {
		if(askToSaveChanges()) // If saving was failed/cancelled
			return;
		
		activeFile = "";
		codeArea.setText("");
		updateTitle();
	}
	
	private void loadFile() {
		if(askToSaveChanges()) // If saving was failed/cancelled
			return;
		
		JFileChooser fc = new JFileChooser(workspace);
		fc.setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));
		fc.setDialogTitle("Load file");
		
		int i = fc.showOpenDialog(null);
		if(i == JFileChooser.APPROVE_OPTION) {
	    	try {
				codeArea.setText(IOUtils.readFromFile(fc.getSelectedFile()));
			} catch (FileNotFoundException e) {
				JOptionPane.showMessageDialog(this, "File not found");
				return;
			}
		    activeFile = fc.getSelectedFile().getName();
	    	undo.discardAllEdits();
			updateTitle();
		}
	}
	
	/**
	 * Open a saving dialog and save the file
	 * @param saveAs specifies if the file should be saved as another file
	 * @return true if the file was saved successfully, false otherwise
	 */
	private boolean saveFile(boolean saveAs) {
		if(workspace.isBlank())
			if(!selectWorkspace()) // If selecting workspace was failed/cancelled
				return false;
		
		if(activeFile.isBlank() || saveAs) {
			JFileChooser fc = new JFileChooser(workspace);
			fc.setFileFilter(new FileNameExtensionFilter("*.txt", "txt"));
			if(!activeFile.isBlank())
				fc.setSelectedFile(new File(workspace+"/"+activeFile));
			fc.setDialogTitle(saveAs ? "Save As" : "Save file");
			
			int i = fc.showSaveDialog(null);
			if(i == JFileChooser.APPROVE_OPTION) {
				if(!fc.getSelectedFile().getParent().equals(workspace)) {
					JOptionPane.showMessageDialog(this, "File must be in workspace folder");
					return false;
				}
				activeFile = fc.getSelectedFile().getName();
				if(!activeFile.endsWith(".txt")) {
					activeFile += ".txt";
				}
			} else return false;
		}

		IOUtils.writeToFile(codeArea.getText(), new File(workspace+"/"+activeFile));
		updateTitle();
		return true;
	}
	
	private boolean selectWorkspace() {
		JFileChooser fc = new JFileChooser(System.getProperty("user.home")+"/Desktop");
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.setDialogTitle("Select workspace location");
		
		int i = fc.showSaveDialog(null);
		if(i == JFileChooser.APPROVE_OPTION) {
			workspace = fc.getSelectedFile().getAbsolutePath();
			if(!Files.exists(Path.of(workspace))) {
				workspace = "";
				JOptionPane.showMessageDialog(this, "Selected folder doesn't exist");
				return false;
			}
			userPref.put("workspace", workspace); // Store workspace location
		} else return false;
		return true;
	}
	
	private boolean askToSaveChanges() {
		if(isChanged) {
			int i = JOptionPane.showConfirmDialog(this, "Do you want to save changes"+(activeFile.isBlank() ? "" : " in "+activeFile)+"?", "New file", JOptionPane.YES_OPTION);
			if(i == JOptionPane.YES_OPTION) {
				if(!saveFile(false)) // If saving failed, don't discard changes
					return true;
			} else if(i == JOptionPane.CANCEL_OPTION) // If saving was cancelled, don't discard changes
				return true;
		}
		return false;
	}
	
	private void updateTitle() {
		isChanged = false;
		tp.setTitleAt(0, activeFile.isBlank() ? "Untitled" : activeFile);
	}
	
	private void assemble() {
		if(first)
			first = false;
		else frisc = new FRISC(memoryFrame, this);
		
		Assembler a = new Assembler(codeArea.getText().toUpperCase(), frisc);
		a.execute();
		try {
			if(a.get()) {
				memoryFrame.storeFRISC(frisc);
				memoryFrame.refresh();
				
				execute.setEnabled(true);
				memory.setEnabled(true);
				
				logArea.append(getDate()+"Assembly successful!\n");
			}
			else {
				execute.setEnabled(false);
				memory.setEnabled(false);
			}
		} catch(Exception e) {
			logArea.append(getDate()+"Assembly error: "+e.getCause().getMessage()+"\n");
		}
	}
	
	private void execute() {		
		execute.setText("Stop executing");
		assemble.setEnabled(false);
		fileMenu.setEnabled(false);
		frisc.debug(debugMode.getState());
		if(debugMode.isSelected()) {
			nextInstr.setVisible(true);
			nextInstr.setEnabled(true);
		}
		Thread exe = new Thread(frisc);
		exe.start();
	}
	
	void stopExecuting() {
		frisc.stop();
		execute.setText("Execute");
		execute.setEnabled(false);
		assemble.setEnabled(true);
		fileMenu.setEnabled(true);
		nextInstr.setVisible(false);
		nextInstr.setEnabled(false);
	}
	
	private void setKeys() {
		newFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
		load.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK));
		save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
		saveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK));
		assemble.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_DOWN_MASK));
		memory.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_DOWN_MASK));
		debugMode.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_DOWN_MASK));

		JPanel contentPane = (JPanel)this.getContentPane();
		
		contentPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK), "exe");
		contentPane.getActionMap().put("exe", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				execute.doClick();
			}
		});
		
		contentPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enter");
		contentPane.getActionMap().put("enter", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				nextInstr.doClick();
			}
		});
		
		codeArea.getInputMap().put(KeyStroke.getKeyStroke("control Z"), "Undo");
		codeArea.getActionMap().put("Undo", new AbstractAction("Undo") {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent evt) {
				try {
					if (undo.canUndo())
						undo.undo();
					} catch (CannotUndoException e) {}
			}
		});
	}
	
	public static String getDate() {
    	return "("+new SimpleDateFormat("HH:mm:ss").format(new Date())+") ";
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			MainFrame frame = new MainFrame();
			frame.setTitle("Frisky");
			frame.setSize(new Dimension(600, 600));
			frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setFocusable(true);
			frame.setIconImage(new ImageIcon(MainFrame.class.getResource("/FRISC.png")).getImage());
			frame.setVisible(true);
		});
	}
}