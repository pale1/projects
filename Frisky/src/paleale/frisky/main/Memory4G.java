package paleale.frisky.main;

import java.util.TreeMap;

import paleale.frisky.utils.CheckUtils;

public class Memory4G {
	public static final int Ki16 = 16384;
	public static final int Ki64 = 65536;
	
	public class Memory64K {
		public int pageID;
		public int[] mem = new int[Ki16];
		
		private Memory64K(int pageID) {
			this.pageID = pageID;
		}
		
		@Override
		public String toString() {
			return String.format("  %8s-%-8s",
					Integer.toHexString(pageID*Ki64).toUpperCase(),
					Integer.toHexString((pageID+1)*Ki64-4).toUpperCase());
		}
	}
	
	private TreeMap<Integer, Memory64K> pages = new TreeMap<>();
	
	public void store(int address8, int value) {
		store(address8, value, 4);
	}
	
	public int load(int address8) {
		return load(address8, 4);
	}
	
	public void store(int address8, int value, int size) {
		if(size == 4) { // Memory word
			set(Integer.divideUnsigned(address8, 4), value);
			return;
		}
		int mask = 0;

		if(size == 2) { // Halfword
			value &= Integer.parseInt("ffff", 16); // Cut to 16-bit
			if(address8 % 4 <= 1) { // First halfword of the memory word
				value <<= 16;
				mask = Integer.parseInt("ffff", 16);
			}
			else 			 	 // Second halfword of the memory word
				mask = CheckUtils.parseInt("ffff0000", 16);
			
		} else if(size == 1) { // Byte
			value &= Integer.parseInt("ff", 16); // Cut to 8-bit
			
			switch(address8%4) {
			case 0:
				value <<= 24;
				mask = CheckUtils.parseInt("00ffffff", 16);
				break;
			case 1:
				value <<= 16;
				mask = CheckUtils.parseInt("ff00ffff", 16);
				break;
			case 2:
				value <<= 8;
				mask = CheckUtils.parseInt("ffff00ff", 16);
				break;
			case 3:
				mask = CheckUtils.parseInt("ffffff00", 16);
			}
		} else return;
		
		set(Integer.divideUnsigned(address8, 4), get(Integer.divideUnsigned(address8, 4)) & mask);

		set(Integer.divideUnsigned(address8, 4), get(Integer.divideUnsigned(address8, 4)) | value);
	}
	
	public int load(int address8, int size) {
		if(size == 4) { // Memory word
			return get(Integer.divideUnsigned(address8, 4));
		} else if(size == 2) { // Halfword
			if(address8%4 <= 1) // First halfword of the memory word
				return get(Integer.divideUnsigned(address8, 4)) >> 16;
			else				// Second halfword of the memory word
				return get(Integer.divideUnsigned(address8, 4) << 16) >> 16;
		} else if(size == 1) { // Byte
			switch(address8%4) {
			case 0:
				return get(Integer.divideUnsigned(address8, 4)) >> 24;
			case 1:
				return (get(Integer.divideUnsigned(address8, 4)) << 8) >> 24;
			case 2:
				return (get(Integer.divideUnsigned(address8, 4)) << 16) >> 24;
			case 3:
			default:
				return (get(Integer.divideUnsigned(address8, 4)) << 24) >> 24;
			}
		} else throw new IllegalArgumentException("Size can be 4, 2, 1");
	}
	
	private void set(int address32, int value) {

		long pageID = Integer.divideUnsigned(address32, Ki16);
		Memory64K page = pages.get((int)pageID);
		
		int addrOnPage = (int)(Integer.toUnsignedLong(address32)-pageID*Ki16);

		if(page != null)
			page.mem[addrOnPage] = value;
		else if(pageID > Ki64)
			throw new IllegalArgumentException("Address too big");
		else {
			page = new Memory64K((int)pageID);
			page.mem[addrOnPage] = value;
			pages.put((int)pageID, page);
		}
	}
	
	private int get(int address32) {
		
		long pageID = Integer.divideUnsigned(address32 ,Ki16);
		Memory64K page = pages.get((int)pageID);
		
		if(page != null)
			return page.mem[(int)(Integer.toUnsignedLong(address32)-pageID*Ki16)];
		if(pageID >= Ki64)
			throw new IllegalArgumentException("Address too big");
		return 0;
	}
	
	public Memory64K getPageForAddress(int address32) {
		return pages.get(Integer.divideUnsigned(address32, Ki16));
	}
	
	public Memory64K getPage(int pageID) {
		return pages.get(pageID);
	}
	
	public TreeMap<Integer, Memory64K> getPages() {
		return pages;
	}
}
