package paleale.frisky.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import paleale.frisky.main.Memory4G.Memory64K;

public class MemoryFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private JPanel 		cp = new JPanel(),
						ep = new JPanel(),
						ncp = new JPanel(),
						scp = new JPanel(),
						nep = new JPanel(),
						cep = new JPanel(),
						wncp = new JPanel(),
						cncp = new JPanel(),
						wscp = new JPanel(),
						cscp = new JPanel(),
						wcep = new JPanel(),
						ccep = new JPanel();
						
	
	protected JTextField SR = new JTextField(),
						PC = new JTextField(),
						SP = new JTextField();
	
	protected JTextField[] registers = new JTextField[8];
	
	protected JLabel[] addresses = new JLabel[16];
	protected JTextField[] memory = new JTextField[16];
	
	protected JSlider memSlider = new JSlider();
	private JComboBox<Memory64K> memCombo = new JComboBox<>();
	private ActionListener al = e -> refreshMemory();
	
	private FRISC frisc;

	private final Font DEF_FONT = new Font(Font.MONOSPACED, Font.BOLD, 11);

	public MemoryFrame() {
		
		setTitle("Memory");
		setLocation(500, 200);
		setIconImage(new ImageIcon(MainFrame.class.getResource("/Memory.png")).getImage());
		setResizable(false);
		
		cp.setLayout(new BorderLayout());
		ep.setLayout(new BorderLayout());
		ncp.setLayout(new BorderLayout());
		scp.setLayout(new BorderLayout());
		nep.setLayout(new GridLayout(1, 0));
		cep.setLayout(new BorderLayout());
		wncp.setLayout(new GridLayout(0, 1));
		cncp.setLayout(new GridLayout(0, 1));
		wscp.setLayout(new GridLayout(0, 1));
		cscp.setLayout(new GridLayout(0, 1));
		wcep.setLayout(new GridLayout(0, 1));
		ccep.setLayout(new GridLayout(0, 1));
		
		wncp.add(new JLabel());
		cncp.add(new JLabel());
		
		cp.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(Color.black), "CPU"));
		JLabel la = new JLabel("    SR: ");
		la.setToolTipText("State register: lowest 5 bits are flags - GIE, Z, V, C, N");
		wncp.add(la);
		SR.setFont(DEF_FONT);
		SR.setEditable(false);
		cncp.add(SR);
		la = new JLabel("    PC: ");
		la.setToolTipText("Program counter: points to address of the next instruction to be executed");
		wncp.add(la);
		PC.setFont(DEF_FONT);
		PC.setEditable(false);
		cncp.add(PC);
		la = new JLabel("    SP: ");
		la.setToolTipText("Stack pointer: points to address of the value on top of the stack");
		wncp.add(la);
		SP.setFont(DEF_FONT);
		SP.setEditable(false);
		cncp.add(SP);
		
		scp.setBorder(BorderFactory.createTitledBorder("Registers"));
		
		for(int i = 0; i < 8; i++) {
			registers[i] = new JTextField();
			registers[i].setFont(DEF_FONT);
			registers[i].setEditable(false);
			wscp.add(new JLabel(" R"+i+": "));
			cscp.add(registers[i]);
		}
		
		ep.setBorder(BorderFactory.createTitledBorder(
				BorderFactory.createLineBorder(Color.black), "Memory"));
		
		nep.add(memSlider);
		nep.add(memCombo);
		
		for(int i = 0; i < 16; i++) {
			addresses[i] = new JLabel();
			addresses[i].setFont(DEF_FONT);
			wcep.add(addresses[i]);
			memory[i] = new JTextField(standardDisplay(0));
			memory[i].setFont(DEF_FONT);
			memory[i].setEditable(false);
			ccep.add(memory[i]);
		}
		
		setPreferredSize(new Dimension(710, 400));
		wcep.setPreferredSize(new Dimension(80, wcep.getHeight()+320));
		ccep.setPreferredSize(new Dimension(280, ccep.getHeight()+320));
		nep.setPreferredSize(new Dimension(280, nep.getHeight()+20));
		
		ncp.add(wncp, BorderLayout.WEST);
		ncp.add(cncp, BorderLayout.CENTER);
		
		scp.add(wscp, BorderLayout.WEST);
		scp.add(cscp, BorderLayout.CENTER);
		
		cep.add(wcep, BorderLayout.WEST);
		cep.add(ccep, BorderLayout.CENTER);
		
		cp.add(ncp, BorderLayout.NORTH);
		cp.add(scp, BorderLayout.SOUTH);
		
		ep.add(nep, BorderLayout.NORTH);
		ep.add(cep, BorderLayout.CENTER);
		
		add(cp, BorderLayout.CENTER);
		add(ep, BorderLayout.EAST);
		
		memSlider.setMinimum(0);
		memSlider.setMaximum(1023);
		memSlider.setValue(0);
		memSlider.addChangeListener(l -> refreshMemory());

		memCombo.setFont(DEF_FONT);
		memCombo.addActionListener(al);
		
		pack();
	}
	
	public void storeFRISC(FRISC frisc) {
		this.frisc = frisc;
		memCombo.removeAllItems();
	}
	
	/**
	 * Refreshes all memory displayed.
	 * This method requires prior call of storeFrisc(FRISC frisc)
	 */
	public void refresh() {
		refreshMemory();
		
		SR.setText(standardDisplay(frisc.getSR()));
		PC.setText(standardDisplay(frisc.getPC()));
		SP.setText(standardDisplay(frisc.getReg(7)));
		
		for(int i = 0; i < 8; i++) {
			if(frisc.isInit(i))
				registers[i].setText(standardDisplay(frisc.getReg(i)));
			else
				registers[i].setText(standardDisplay(0).replace("0", "x"));
		}
		cncp.repaint();
		cscp.repaint();
	}
	
	private void refreshMemory() {
		memCombo.removeActionListener(al);
		Memory64K selectedPage = (Memory64K) memCombo.getSelectedItem();
		
		memCombo.removeAllItems();
		for(Map.Entry<Integer, Memory64K> item : frisc.mem.getPages().entrySet()) {
			memCombo.addItem(item.getValue());
		}
		if(selectedPage == null && memCombo.getItemCount()>0)
			memCombo.setSelectedIndex(0);
		else
			memCombo.setSelectedItem(selectedPage);
		
		memCombo.addActionListener(al);
		
		int value = memSlider.getValue();
		TitledBorder tb = (TitledBorder) ep.getBorder();
		
		int beginMem = selectedPage == null ? 0 : selectedPage.pageID*Memory4G.Ki64;
		
		tb.setTitle(tb.getTitle().split(" ")[0]+" "+Integer.toHexString(beginMem+value*64).toUpperCase()+
												"-"+Integer.toHexString(beginMem+value*64+60).toUpperCase());
		
		for(int i = 0; i < 16; i++) {
			addresses[i].setText(String.format(" %8s ", Integer.toHexString(beginMem+value*64+4*i).toUpperCase()));
			memory[i].setText(standardDisplay(selectedPage == null ? 0 : selectedPage.mem[value*16+i]));
		}
		ep.repaint();
	}
	
	/**
	 * Converts 32-bit number into a string with format hexValue-binaryValue
	 * @param i number to convert
	 * @return string containing hexadecimal and binary value
	 */
	private String standardDisplay(int i) {
		return String.format("%8s-%32s", Integer.toHexString(i).toUpperCase(),
				Integer.toBinaryString(i)).replace(" ", "0");
	}
}
