package paleale.frisky.model;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import paleale.frisky.listener.MultipleDocumentListener;
import paleale.frisky.listener.SingleDocumentListener;

/**
 * Default implementation of MultipleDocumentModel used as a container for multiple SingleDocumentModels
 * @author alenp
 *
 */
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {
	private static final long serialVersionUID = 1L;
	
	private List<SingleDocumentModel> documents = new ArrayList<>();
	private List<MultipleDocumentListener> listeners = new ArrayList<>();
	
	private SingleDocumentModel currentDocument;
	
	
	private SingleDocumentListener listener = new SingleDocumentListener() {
		
		@Override
		public void documentModifyStatusUpdated(SingleDocumentModel model) {
			if(model.isModified())
				setIconAt(documents.indexOf(model), readIcon("icons/red_disk.png"));
			else
				setIconAt(documents.indexOf(model), readIcon("icons/green_disk.png"));
		}

		@Override
		public void documentFilePathUpdated(SingleDocumentModel model) {
			setToolTipTextAt(documents.indexOf(model), model.getFilePath().toString());
			setTitleAt(documents.indexOf(model), model.getFilePath().getFileName().toString());
		}
	
	};
	
	/**
	 * Creates new DefaultMultipleDocumentModel with localization provider specified
	 * @param lp localization provider used for localization of various words
	 */
	public DefaultMultipleDocumentModel() {
		
		addChangeListener(e -> {
			SingleDocumentModel oldSelected = currentDocument;
			currentDocument = getSelectedIndex()!= -1 ? documents.get(getSelectedIndex()) : null;
			
			for(MultipleDocumentListener l : listeners)
				l.currentDocumentChanged(oldSelected, currentDocument);
		});
	}

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documents.iterator();
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel document = new DefaultSingleDocumentModel(null, "");
		document.setModified(true);
		document.addSingleDocumentListener(listener);

		JScrollPane sp = new JScrollPane();
		sp.setViewportView(document.getTextComponent());
		
		documents.add(document);
		addTab("(unnamed)",
				readIcon("icons/red_disk.png"),
				sp,
				"(unnamed)"
		);
		setSelectedIndex(getTabCount()-1);
		
		for(MultipleDocumentListener l : listeners)
			l.documentAdded(document);
		
		return document;
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return currentDocument;
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		if(path == null)
			throw new NullPointerException("Path cannot be null!");
		
		for(int i = 0; i < documents.size(); i++) {
			if(path.equals(documents.get(i).getFilePath())) {
				setSelectedIndex(i);
				return documents.get(i);
			}
		}
		
		SingleDocumentModel document = new DefaultSingleDocumentModel(path, readFromFile(path));
		document.setModified(false);
		document.addSingleDocumentListener(listener);
		
		JScrollPane sp = new JScrollPane();
		sp.setViewportView(document.getTextComponent());
		
		documents.add(document);
		addTab(document.getFilePath().getFileName().toString(),
				readIcon("icons/green_disk.png"),
				sp,
				document.getFilePath().toString()
		);
		setSelectedIndex(getTabCount()-1);
		
		for(MultipleDocumentListener l : listeners)
			l.documentAdded(document);
		
		return document;
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		if(newPath == null)
			newPath = model.getFilePath();
		
		writeToFile(model.getTextComponent().getText(), newPath);
		model.setModified(false);
		model.setFilePath(newPath);
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
		int i = documents.indexOf(model);
		
		documents.remove(i);
		remove(i);
		
		for(MultipleDocumentListener l : listeners)
			l.documentRemoved(model);
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);
	}

	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return documents.get(index);
	}
	
	/**
	 * Read text from a file
	 * @param path from where to read
	 * @return text that is read
	 */
	private String readFromFile(Path path) {
		StringBuilder sb = new StringBuilder();
		try {
			for(String line : Files.readAllLines(path, Charset.forName("UTF-8")))
				sb.append(line).append("\n");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	/**
	 * Write text to a file
	 * @param text what to write
	 * @param path where to write
	 */
	private void writeToFile(String text, Path path) {
		try {
			Files.writeString(path, text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private ImageIcon readIcon(String name) {		
		
		byte[] bytes = null;
		try(InputStream is = this.getClass().getResourceAsStream(name)) {
			if(is==null) 
				throw new IOException();
			
			bytes = is.readAllBytes();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new ImageIcon(bytes);
	}

}
