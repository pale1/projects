package paleale.frisky.model;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import paleale.frisky.listener.SingleDocumentListener;

/**
 * Default mplementation of SingleDocumentModel
 * @author alenp
 *
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {
	private List<SingleDocumentListener> listeners = new ArrayList<>();
	
	private JTextArea textComponent;
	private Path filePath;
	
	private boolean modified;
	
	/**
	 * Creates new DefaultSingleDocumentModel
	 * @param path initial file path associated with this document
	 * @param text initial text that the text component will have
	 */
	public DefaultSingleDocumentModel(Path path, String text) {
		filePath = path != null ? path.toAbsolutePath() : null;
		textComponent = new JTextArea();
		textComponent.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				setModified(true);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				setModified(true);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				setModified(true);
			}
		});
		textComponent.setText(text);
	}

	@Override
	public JTextArea getTextComponent() {
		return textComponent;
	}

	@Override
	public Path getFilePath() {
		return filePath;
	}

	@Override
	public void setFilePath(Path path) {
		if(path == null)
			throw new NullPointerException("Path cannot be null!");
		filePath = path.toAbsolutePath();
		
		for(SingleDocumentListener l : listeners)
			l.documentFilePathUpdated(this);
	}

	@Override
	public boolean isModified() {
		return modified;
	}

	@Override
	public void setModified(boolean modified) {
		if(this.modified == modified)
			return;
		
		this.modified = modified;
		
		for(SingleDocumentListener l : listeners)
			l.documentModifyStatusUpdated(this);
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(l);
	}

}
