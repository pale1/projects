package paleale.frisky.model;

import java.nio.file.Path;

import paleale.frisky.listener.MultipleDocumentListener;

/**
 * Document model defining a container for multiple SingleDocumentModels
 * @author alenp
 *
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	
	/**
	 * Creates new SingleDocumentModel with null file path, adds it to itself and returns it
	 * @return newly created SingleDocumentModel
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Returns currently selected document
	 * @return currently selected document
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Creates new SingleDocumentModel with text loaded from path specified, adds it to itself and returns it.
	 * If a document with same path already exists, just switches to it instead
	 * @param path file path 
	 * @return newly created SingleDocumentModel
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Saves document specified into a file with path specified.
	 * If path is null, uses document's predefined path
	 * @param model SingleDocumentModel whose text content is to be saved
	 * @param newPath new path at which file will be saved, or null to use document's predefined path
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Closes document specified, removing it from this model
	 * @param model SingleDocumentModel that is to be closed
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Adds a MultipleDocumentListener to listen to changes to this model's number of documents and currently selected document
	 * @param l listener
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Removes a MultipleDocumentListener from this model
	 * @param l listener
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Returns number of documents contained in this model
	 * @return number of documents contained in this model
	 */
	int getNumberOfDocuments();
	
	/**
	 * Returns document at index specified
	 * @param index of document
	 * @return document at index specified
	 */
	SingleDocumentModel getDocument(int index);

}
