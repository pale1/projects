package paleale.frisky.model;

import java.nio.file.Path;

import javax.swing.JTextArea;

import paleale.frisky.listener.SingleDocumentListener;

/**
 * Document model defining a single document
 * @author alenp
 *
 */
public interface SingleDocumentModel {
	
	/**
	 * Returns text component used in this document model
	 * @return text component used in this document model
	 */
	JTextArea getTextComponent();
	
	/**
	 * Returns file path associated with opened document, or null if document is not stored in a file
	 * @return file path associated with opened document, or null if document is not stored in a file
	 */
	Path getFilePath();
	
	/**
	 * Sets file path associated with this document to a new one
	 * @param path new file path
	 */
	void setFilePath(Path path);
	
	/**
	 * Returns true if the document is modified, false otherwise
	 * @return true if the document is modified, false otherwise
	 */
	boolean isModified();
	
	/**
	 * Sets the modified status of this document
	 * @param modified status of this document
	 */
	void setModified(boolean modified);
	
	/**
	 * Adds a SingleDocumentListener to listen to changes to this document's modified status and file path
	 * @param l listener
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Removes a SingleDocumentListener from this document
	 * @param l listener
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);

}
