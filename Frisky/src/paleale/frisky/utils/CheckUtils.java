package paleale.frisky.utils;

import java.util.Map;


public abstract class CheckUtils {
	private static final String LABEL_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

	/**
	 * Checks whether given value is a char specified
	 * @param value to check
	 * @param c
	 */
	public static void checkForChar(String value, String c)
			throws IllegalArgumentException {
		if(!value.equals(c))
			throw new IllegalArgumentException("Missing '"+c+"'");
	}
	/**
	 * Checks for comma
	 * @param comma should be comma
	 * @throws IllegalArgumentException
	 */
	public static void checkForComma(String comma) 
			throws IllegalArgumentException {
		checkForChar(comma, ",");
	}
	
	/**
	 * Checks if register is properly written and parses it's index
	 * @param regName register name
	 * @param displayName src/dest
	 * @return index of register
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 */
	public static int checkRegFormat(String regName, String displayName)
			throws IllegalArgumentException, NumberFormatException {
		int reg = Integer.parseInt(regName.replaceFirst("R", ""));
		if(reg > 7 || reg < 0)
			throw new IllegalArgumentException("Register for "+displayName+" does not exist");
		return reg;
	}

	/**
	 * Checks if value has been written properly and parses it
	 * @param strValue value in string
	 * @param displayName src/dest
	 * @param canHaveLabel if value can be a label
	 * @param labels map containing labels and values, if canHaveLabel is true
	 * @return parsed value in int
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 */
	public static int check20BitValueFormat(String strValue, String displayName, boolean canHaveLabel, Map<String, Integer> labels)
			throws IllegalArgumentException, NumberFormatException {
		int bin = getValue(strValue, displayName, canHaveLabel, labels);
		if((bin | Integer.parseInt("7ffff", 16)) != -1 && (bin >> 19) != 0)
			throw new IllegalArgumentException("Not a 20-bit value");
		bin &= Integer.parseInt("fffff", 16);
		return bin;
	}
	
	/**
	 * Can parse big numbers as unsigned int
	 * @param s
	 * @param radix
	 * @return
	 * @throws IllegalArgumentException
	 * @throws NumberFormatException
	 */
	public static int parseInt(String s, int radix)
            throws IllegalArgumentException, NumberFormatException {
		if(s.isBlank())
			throw new IllegalArgumentException("Missing value");
		int bin = 0;
	    try {
	    	bin = Integer.parseInt(s, radix);
	    } catch(NumberFormatException e) { // Perhaps number is very large but parsable as unsigned int
	    	long l = Long.parseLong(s, radix);
	    	
	    	int check = (int) (l >> 32);
			if(check!=0 && check!=-1) {
				
				if(check < 0)
					throw new IllegalArgumentException("Too small number");
				else
					throw new IllegalArgumentException("Too large number");
			}
			bin = (int) l;
	    }
	    return bin;
	}
	
	/**
	 * Checks whether label is written properly
	 * @param label to check
	 */
	public static void checkLabelFormat(String label)
			throws IllegalArgumentException {
		char[] chars = label.toCharArray();
		
		if(Character.isDigit(chars[0]))
			throw new IllegalArgumentException("Label name cannot start with a digit");
		
		for(char c : chars) {
			if(!LABEL_CHARS.contains(Character.toString(c)))
				throw new IllegalArgumentException("Forbidden character in label name");
		}
	}
	
	/**
	 * Checks if value can be parsed, then parses binary, decimal or hex value into an int.	<br>
	 * Value can be a label
	 * @param strValue string representing value
	 * @param displayName how value should be displayed in case of exception
	 * @param canHaveLabel true if strValue might be a label, false otherwise
	 * @param labels map containing labels and associated values
	 * @return parsed value
	 */
	public static int getValue(String strValue, String displayName, boolean canHaveLabel, Map<String, Integer> labels) 
			throws IllegalArgumentException, NumberFormatException {
		if(canHaveLabel) {
			if(!Character.isDigit(strValue.charAt(0)) && strValue.charAt(0) != '%' && strValue.charAt(0) != '-') {
				if(labels.containsKey(strValue))
					return labels.get(strValue);
				else
					throw new IllegalArgumentException("Unknown label");
			}
		}

		if(strValue.startsWith("%")) {	// Value is in a different base
			if(strValue.startsWith("%D"))
				return CheckUtils.parseInt(strValue.replaceFirst("%D", ""), 10);// Base 10
			else if(strValue.startsWith("%B"))
				return CheckUtils.parseInt(strValue.replaceFirst("%B", ""), 2); // Base 2
			else if(!strValue.startsWith("%H"))									// Not base 16
				throw new IllegalArgumentException("Unknown base for "+displayName);
		}
		// Value is in base 16
		strValue = strValue.replaceFirst("%H", "");
		if(strValue.charAt(0) != '-' && !Character.isDigit(strValue.charAt(0)) ||
		   strValue.charAt(0) == '-' && !Character.isDigit(strValue.charAt(1)))
			throw new IllegalArgumentException("Hex number has to start with a digit");
		
		return CheckUtils.parseInt(strValue, 16);
	}
	
	public static int checkForCondition(String instr_cond) {
		int value = 0;
		String parts[] = instr_cond.split("_");
		
		if(parts.length>1) { // Has condition
			value = CheckUtils.getCodeForCondition(parts[1]);
			if(value == -1)
				throw new IllegalArgumentException("Unknown condition suffix");
		}
		
		if(parts.length>2)
			throw new NumberFormatException();
		
		return value;
	}
	
	public static int getCodeForCondition(String cond) {
		switch(cond) {
		case "C":
			return 1;
		case "NC":
			return 2;
		case "V":
			return 3;
		case "NV":
			return 4;
		case "N":
			return 5;
		case "NN":
			return 6;
		case "M":
			return 5;
		case "P":
			return 6;
		case "Z":
			return 7;
		case "NZ":
			return 8;
		case "EQ":
			return 7;
		case "NE":
			return 8;
		case "ULE":
			return 9;
		case "UGT":
			return 10;
		case "ULT":
			return 2;
		case "UGE":
			return 1;
		case "SLE":
			return 11;
		case "SGT":
			return 12;
		case "SLT":
			return 13;
		case "SGE":
			return 14;
		default:
			return -1;
		}
	}
}
