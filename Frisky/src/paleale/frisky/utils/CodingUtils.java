package paleale.frisky.utils;

import java.util.List;
import java.util.Map;

import paleale.frisky.main.FRISC;

/**
 * AL-instructions											<br>
 * <5opcode>(1flag)[3dest][3src1][3/20src2]					<br>
 * 															<br>
 * CMP instruction											<br>
 * <5opcode>(1flag){3}[3src1][3/20src2]						<br>
 * 															<br>
 * MOVE instruction											<br>
 * <5opcode>(1flag)(2flag){1}[0/3dest][3/20src/dest]		<br>
 * 2flag:													<br>
 * 00 - Reg/Val-Reg											<br>
 * 01 - Reg/Val-SR											<br>
 * 10 - SR-Reg												<br>
 * 															<br>
 * MEM-instructions											<br>
 * <5opcode>(1flag)[3src/dest][3adrreg][20src/dest]			<br>
 * 															<br>
 * PUSH-POP instructions									<br>
 * <5opcode>{1}[3src/dst]{23}								<br>
 * 															<br>
 * JP/CALL instructions										<br>
 * <5opcode>(1flag)<4cond>{2}[3/20address]					<br>
 * 															<br>
 * JR instruction											<br>
 * <5opcode>{1}<4cond>{2}[20relAddress]						<br>
 * 															<br>
 * RET/RETI/RETN/HALT										<br>
 * <5opcode>{1}<4cond>{22}
 */
public abstract class CodingUtils {
	
	public static int codeAL(List<String> instrParts, int binInstr) 
			throws IllegalArgumentException, NumberFormatException {
		//src1
		binInstr |= CheckUtils.checkRegFormat(instrParts.get(1), "src1") << 20;
		
		CheckUtils.checkForComma(instrParts.get(2));
		
		//src2
		if(instrParts.get(3).startsWith("R"))
			binInstr |= CheckUtils.checkRegFormat(instrParts.get(3), "src2") << 17;
		else {
			binInstr |= CheckUtils.check20BitValueFormat(instrParts.get(3), "src2", false, null);
			binInstr |= 1 << 26;

		}
		
		CheckUtils.checkForComma(instrParts.get(4));
		
		//dst
		binInstr |= CheckUtils.checkRegFormat(instrParts.get(5), "dst") << 23;
		
		if(instrParts.size() > 6)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeCMP(List<String> instrParts, int binInstr, Map<String, Integer> labels) 
			throws IllegalArgumentException, NumberFormatException {
		//src1
		binInstr |= CheckUtils.checkRegFormat(instrParts.get(1), "src1") << 20;
		
		CheckUtils.checkForComma(instrParts.get(2));
		
		//src2
		if(instrParts.get(3).startsWith("R"))
			binInstr |= CheckUtils.checkRegFormat(instrParts.get(3), "src2") << 17;
		else {
			binInstr |= CheckUtils.check20BitValueFormat(instrParts.get(3), "src2", true, labels);
			binInstr |= 1 << 26;

		}
		
		if(instrParts.size() > 4)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeMOVE(List<String> instrParts, int binInstr, Map<String, Integer> labels) 
			throws IllegalArgumentException, NumberFormatException {
		if(instrParts.get(1).startsWith("R")) { // Reg
			
			//src
			binInstr |= CheckUtils.checkRegFormat(instrParts.get(1), "src") << 17;
			
			CheckUtils.checkForComma(instrParts.get(2));
			
			if(instrParts.get(3).startsWith("R")) {
				
				//dest
				binInstr |= CheckUtils.checkRegFormat(instrParts.get(3), "dest") << 20;
			} else if(instrParts.get(3).equals("SR")) {
				binInstr |= 1 << 24;
			} else
				throw new IllegalArgumentException("dest cannot be a value");
		} else if(instrParts.get(1).equals("SR")){ // SR
			binInstr |= 1 << 25;
			
			CheckUtils.checkForComma(instrParts.get(2));

			if(instrParts.get(3).startsWith("R")) {
				
				//dest
				binInstr |= CheckUtils.checkRegFormat(instrParts.get(3), "dest") << 20;
			} else if(!instrParts.get(3).equals("SR"))
				throw new IllegalArgumentException("dest cannot be a value");
		} else { // Value
			
			//src
			binInstr |= CheckUtils.check20BitValueFormat(instrParts.get(1), "src", true, labels);
			binInstr |= 1 << 26;
			
			CheckUtils.checkForComma(instrParts.get(2));
			
			if(instrParts.get(3).startsWith("R")) {
				
				//dest
				binInstr |= CheckUtils.checkRegFormat(instrParts.get(3), "dest") << 20;
			} else if(instrParts.get(3).equals("SR")) {
				binInstr |= 1 << 24;
			} else
				throw new IllegalArgumentException("dest cannot be a value");
		}
		if(instrParts.size() > 4)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeMEM(List<String> instrParts, int binInstr, String displayName, Map<String, Integer> labels)
			throws IllegalArgumentException, NumberFormatException {
		int index = 1;
		
		//src/dest
		binInstr |= CheckUtils.checkRegFormat(instrParts.get(index++), displayName) << 23;
		
		CheckUtils.checkForComma(instrParts.get(index++));
		
		CheckUtils.checkForChar(instrParts.get(index++), "(");
		
		int value = -1;
		try{
			value = CheckUtils.check20BitValueFormat(instrParts.get(index), "address", true, labels);
		} catch(Exception e){}
		
		if(value==-1) { // Register (with offset)
			
			//Separate the minus from the enclosing arguments
			if(instrParts.get(index).endsWith("-")) { // i.e R0- %D6
				instrParts.add(index+1, "-");
				instrParts.set(index, instrParts.get(index).replace("-", ""));
			} else if(instrParts.get(index+1).startsWith("-")
					&& instrParts.get(index+1).length()>1) { // i.e R0 -%D6
				instrParts.add(index+1, "-");
				instrParts.set(index+2, instrParts.get(index+2).replace("-", ""));
			} else if(instrParts.get(index).contains("-")) { // i.e R0-%D6
				String[] parts = instrParts.get(index).split("-", 2);
				instrParts.set(index, parts[0]);
				instrParts.add(index+1, "-");
				instrParts.add(index+2, parts[1]);
			}
			
			//dest/src
			binInstr |= CheckUtils.checkRegFormat(instrParts.get(index++), displayName.equals("src")?"dest":"src") << 20;
			
			if(instrParts.get(index).equals("-")) { // Negative offset
				
				binInstr |= (-CheckUtils.check20BitValueFormat(instrParts.get(index+1), "offset", false, null) & Integer.parseInt("fffff", 16));
				index+=2;
			} else if(!instrParts.get(index).equals(")")) { // Positive offset
				binInstr |= CheckUtils.check20BitValueFormat(instrParts.get(index+1), "offset", false, null);
				index+=2;
			}
		} else { // 20-bit value
			binInstr |= value;
			binInstr |= 1 << 26;
			index++;
		}
		CheckUtils.checkForChar(instrParts.get(index), ")");
		
		if(instrParts.size() > index+1)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeStackOP(List<String> instrParts, int binInstr, String displayName)
			throws IllegalArgumentException, NumberFormatException {
		int reg = CheckUtils.checkRegFormat(instrParts.get(1), displayName);
		binInstr |= reg << 23;
		
		if(instrParts.size() > 2)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeJumpOP(List<String> instrParts, int binInstr, Map<String, Integer> labels)
			throws IllegalArgumentException, NumberFormatException {
		binInstr |= CheckUtils.checkForCondition(instrParts.get(0)) << 22;
		
		int index = 1;
		if(instrParts.get(index).equals("(")) { // Register with address
			index++;
			//adrreg
			binInstr |= CheckUtils.checkRegFormat(instrParts.get(index++), "adrreg");
			
			CheckUtils.checkForChar(instrParts.get(index), ")");
			
		} else { // Value
			binInstr |= CheckUtils.check20BitValueFormat(instrParts.get(index), "address", true, labels);
			binInstr |= 1 << 26;
		}
		
		if(instrParts.size() > index+1)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeJR(List<String> instrParts, int binInstr, Map<String, Integer> labels, FRISC frisc)
			throws IllegalArgumentException, NumberFormatException {
		binInstr |= CheckUtils.checkForCondition(instrParts.get(0)) << 22;
		
		int address = CheckUtils.getValue(instrParts.get(1), "address", true, labels);
		int realAddress = address - frisc.memwr;
		if((realAddress | Integer.parseInt("7ffff", 16)) != -1 && (realAddress >> 19) != 0)
			throw new IllegalArgumentException("Address too far");
		realAddress &= Integer.parseInt("fffff", 16);
		
		binInstr |= realAddress;
		binInstr |= 1 << 26;
		
		if(instrParts.size()>2)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
	
	public static int codeRETHALT(List<String> instrParts, int binInstr)
			throws IllegalArgumentException {
		binInstr |= CheckUtils.checkForCondition(instrParts.get(0)) << 22;
		
		if(instrParts.size() > 1)
			throw new IllegalArgumentException("Too many arguments");
		
		return binInstr;
	}
}
