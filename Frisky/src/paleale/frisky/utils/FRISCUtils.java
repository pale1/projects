package paleale.frisky.utils;

import paleale.frisky.main.MainFrame;
import paleale.frisky.main.Memory4G;
import paleale.frisky.main.MemoryFrame;

public abstract class FRISCUtils {
	
	protected boolean IIF = true;
	
	/**Instruction register
	 */
	protected int IR = 0;
	
	/**Program counter
	 */
	protected int PC = 0;
	
	/**Status register
	 */
	protected int SR = 0;
	
	protected boolean GIE, Z, V, C, N;
	
	
	
	public Memory4G mem = new Memory4G();
	protected int[] registers = new int[8];
	protected boolean[] initRegisters = new boolean[8];
	
	protected int memwr = 0;
	protected MemoryFrame memoryFrame;
	protected MainFrame mainFrame;
	
	protected void A(int sign, boolean carry) {
		int a = getRegister((IR >> 20)&7),
			b = sign*getB(17);
				
		setFlagsA(a, b);
		setRegister((IR >> 23)&7, a + b + (carry ? (C ? 1 : 0) : 0));
	}
	
	protected void L(int mode) {
		int res;
		
		if(mode == 0)
			res = getRegister((IR >> 20)&7) & getB(17);
		else if(mode == 1)
			res = getRegister((IR >> 20)&7) | getB(17);
		else
			res = getRegister((IR >> 20)&7) ^ getB(17);
		
		setFlagsL(res);
		setRegister((IR >> 23)&7, res);
	}
	
	protected void SH(int mode) {
		int res, 
			a = getRegister((IR >> 20)&7),
			b = getB(17);
		
		if(b == 0)
			return;
		if(b < 0)
			throw new IllegalArgumentException("Illegal negative argument");
		if(mode == 0)
			res = a << b-1;
		else if(mode == 1)
			res = a >>> b-1;
		else
			res = a >> b-1;
			
		boolean c;
		if(mode == 0)
			c = (res & Integer.MIN_VALUE) != 0;
		else
			c = (res & 1) != 0;
		
		if(mode == 0)
			res <<= 1;
		else if(mode == 1)
			res >>>= 1;
		else
			res >>= 1;
		
		setFlagsSH(res, c);
		setRegister((IR >> 23)&7, res);
	}
	
	protected void ROT(boolean left) {
		String a = String.format("%32s",
				Integer.toBinaryString(getRegister((IR >> 20)&7))).replace(" ", "0");
		int b = getB(17) % 32;
		if(b == 0)
			return;
		if(b < 0)
			throw new IllegalArgumentException("Illegal negative argument");
		String res;
		if(left)
			res = a.substring(b) + a.substring(0, b);
		else
			res = a.substring(32-b) + a.substring(0, 32-b);
		int iRes = Integer.parseInt(res, 2);
		
		setFlagsSH(iRes, left ? (iRes&1) != 0 : (iRes&Integer.MIN_VALUE) != 0);
		setRegister((IR >> 23)&7, iRes);
	}
	
	protected void CMP() {
		setFlagsA(getRegister((IR >> 20)&7), -getB(17));
	}
	
	protected void MOVE() {
		int mode = (IR >> 24)&3;

		if(mode == 0) { // Reg/Val-Reg
			setRegister((IR >> 20)&7, getB(17));
		} else if(mode == 1) { // Reg/Val-SR
			SR = getB(17);
			updateFlags();
		} else { // SR-Reg
			updateSR();
			setRegister((IR >> 20)&7, SR);
		}
	}

	protected void LOAD(int size) {
		int addr8 = getMemAddress();
		setRegister((IR >> 23)&7, mem.load(addr8, size));
	}
	
	protected void STORE(int size) {
		int addr8 = getMemAddress();
		mem.store(addr8, getRegister((IR >> 23)&7), size);
	}
	
	protected void PUSH() {
		checkInit(7);
		
		registers[7] -= 4;
		mem.store(registers[7], registers[(IR >> 23)&7]); // register can be uninitialized
	}
	
	protected void POP() {
		checkInit(7);

		setRegister((IR >> 23)&7, mem.load(registers[7]));
		registers[7] += 4;
	}
	
	protected void JP() {
		if(!isConditionFulfilled())
			return;
		PC = getB(17);
	}
	
	protected void JR() {
		if(!isConditionFulfilled())
			return;
		PC += getB(-1)-4;
	}

	protected void CALL() {
		if(!isConditionFulfilled())
			return;
		checkInit(7);

		registers[7] -= 4;
		mem.store(registers[7], PC);
		PC = getB(17);
	}
	
	protected void RET(int mode) {
		if(!isConditionFulfilled())
			return;
		checkInit(7);

		PC = mem.load(registers[7]);
		registers[7] += 4;
		if(mode == 1)
			GIE = true;
		else if(mode == 2)
			IIF = true;
	}
	
	/**
	 * Tries to store data into memory on the next write location
	 * @param bin data to store
	 * @param size 1,2,4 how many bytes the data has
	 */
	protected void store(int bin, int size) {
		mem.store(memwr, bin, size);
		memwr += size;
	}
	
	protected void updateSR() {
		SR = (SR >> 5) << 5; // Set lower 5 bits to 0
		if(GIE) SR |= 16;
		if(Z) SR |= 8;
		if(V) SR |= 4;
		if(C) SR |= 2;
		if(N) SR |= 1;
	}
	
	private void updateFlags() {
		GIE = ((SR >> 4)&1) != 0;
		Z = ((SR >> 3)&1) != 0;
		V = ((SR >> 2)&1) != 0;
		C = ((SR >> 1)&1) != 0;
		N = ((SR >> 0)&1) != 0;
	}
	

	private void setRegister(int index, int value) {
		registers[index] = value;
		initRegisters[index] = true;
	}
	
	private int getRegister(int index) {
		checkInit(index);
		return registers[index];
	}
	
	private void checkInit(int index) {
		if(!initRegisters[index])
			throw new IllegalArgumentException("Reading uninitialized register");
	}
	
	private int getB(int offset) {
		if((IR << 5) < 0) // 20-bit value
			return (IR << 12) >> 12;
		else {
			return getRegister((IR >> offset)&7);
		}
	}
	
	private int getMemAddress() {
		if((IR << 5) < 0) // 20-bit value
			return (IR << 12) >> 12;
		else
			return getRegister((IR >> 20)&7) + ((IR << 12) >> 12);
	}
	
	private void setFlagsA(int a, int b) {
		long res = a + b;
		int MIN = Integer.MIN_VALUE;
		Z = res == 0;
		V = res > Integer.MAX_VALUE || res < MIN;
		C = ((a&MIN) != 0 && (b&MIN) != 0) || (a&MIN | b&MIN) != 0 && (res&MIN) != 0;
		N = res < 0;
	}
	
	private void setFlagsL(int res) {
		Z = res == 0;
		V = false;
		C = false;
		N = res < 0;
	}
	
	private void setFlagsSH(int res, boolean c) {
		Z = res == 0;
		V = false;
		C = c;
		N = res < 0;
	}
	
	protected boolean isConditionFulfilled() {
		int condCode = (IR >> 22)&15;
		switch(condCode) {
		case 0: return true;
		case 1: return C;
		case 2: return !C;
		case 3: return V;
		case 4: return !V;
		case 5: return N;
		case 6: return !N;
		case 7: return Z;
		case 8: return !Z;
		case 9: return !C || Z;
		case 10: return C && !Z;
		case 11: return (N ^ V) || Z;
		case 12: return !(N ^ V) && !Z;
		case 13: return N ^ V;
		case 14: return !(N ^ V);
		}
		return false;
	}
	
	public int getPC() {return PC;}
	public int getSR() {return SR;}
	public boolean isInit(int i) {return initRegisters[i];}
	public int getReg(int i) {return registers[i];}

}
