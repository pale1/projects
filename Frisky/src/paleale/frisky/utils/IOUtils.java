package paleale.frisky.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public abstract class IOUtils {
	
	/**
	 * Write text to a file
	 * @param text what to write
	 * @param file where to write
	 */
	public static void writeToFile(String text, File file) {
		try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) { // File is created
			bw.write(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read text from a file
	 * @param file from where to read
	 * @return text that is read
	 * @throws FileNotFoundException 
	 */
	public static String readFromFile(File file) throws FileNotFoundException {
		String line;
		StringBuilder sb = new StringBuilder();
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
		    while ((line = br.readLine()) != null) {
		    	sb.append(line);
		    	sb.append("\n");
		    }
		} catch (FileNotFoundException e) {
			throw e;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
