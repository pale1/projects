package paleale.frisky.utils;

import java.util.List;
import java.util.Map;

import paleale.frisky.main.FRISC;

public abstract class PseudoUtils {
	private static final String[] PSEUDOS = {"EQU", "ORG", "DW", "DH", "DB", "DS"};
	
	public static boolean checkPseudos(List<String> instrParts, FRISC frisc) {
		switch(instrParts.get(0)) {
		case "EQU":
			return true;
		case "ORG": // ORG, Tell FRISC to change writing location
			frisc.org(CheckUtils.getValue(instrParts.get(1), "address", false, null));
			return true;
		case "DW":
			storeWHB(instrParts, 4, frisc);
			return true;
		case "DH":
			storeWHB(instrParts, 2, frisc);
			return true;
		case "DB":
			storeWHB(instrParts, 1, frisc);
			return true;
		}
		return false;
	}
	
	public static boolean isPseudo(String instrPart) {
		for(String pseudo : PSEUDOS) {
			if(instrPart.equals(pseudo))
				return true;
		}
		return false;
	}
	
	/**
	 * Pseudoinstructions DW, DH, DB come here trimmed without label.		<br>
	 * Store memory words, halfwords or bytes at next writing locations.
	 * @param instrParts
	 * @param size
	 */
	public static void storeWHB(List<String> instrParts, int size, FRISC frisc)
				throws IllegalArgumentException {
		instrParts.remove(0);
		
		try {
		int value = CheckUtils.getValue(instrParts.get(0), "value", false, null);
		frisc.store(value, size);
		
		int i = 1;
		while(i < instrParts.size()) {
			CheckUtils.checkForComma(instrParts.get(i));
			i++;
			value = CheckUtils.getValue(instrParts.get(i), "value", false, null);
			i++;
			frisc.store(value, size);
		}
		} catch(IndexOutOfBoundsException e) {
			throw new IllegalArgumentException("Missing arguments");
		} catch(NumberFormatException e) {
			throw new IllegalArgumentException("Bad syntax");
		} catch(IllegalArgumentException e) {
			throw e;
		}
		frisc.toNextWord();
	}
	
	public static boolean checkForORG(List<String> instrParts) {
		if(instrParts.get(0).equals("ORG")) {
			if(instrParts.size()>2)
				throw new IllegalArgumentException("Too many arguments in ORG");
			return true;
		}
		return false;
	}
	
	public static boolean checkForEQU(List<String> instrParts, Map<String, Integer> labels) {
		if(instrParts.size()>1) {
			if(instrParts.get(1).equals("EQU")) {
				if(instrParts.size()>3)
					throw new IllegalArgumentException("Too many arguments in EQU");
				labels.put(instrParts.get(0), CheckUtils.getValue(instrParts.get(2), "address", false, null));
				return true;
			}
		}
		return false;
	}
}
