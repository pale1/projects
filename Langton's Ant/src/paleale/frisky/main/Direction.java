package paleale.frisky.main;

public enum Direction {
	None, Left, Right, Forward, Turn
}
