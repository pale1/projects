package paleale.frisky.main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.KeyStroke;

public class Frame extends JFrame {
	static final long serialVersionUID = 306680691130333107L;
	
	ColorDir[]		 			cycle;
	private Ant 				ant = new Ant();
	
	private SimulationWorker 	worker;
	private boolean 			isRunning = false;
	private BufferedImage 		image = new BufferedImage(550, 550, BufferedImage.TYPE_INT_RGB);
	private Graphics2D 			graphics = image.createGraphics();
	private JPanel 				np = new JPanel();
	private JSlider 			speedBar = new JSlider();
	private JButton 			startSim = new JButton("Start Simulation"),
								editRules = new JButton("Edit Simulation Rules"),
								refresh = new JButton();
	
	private JLabel 				picture = new JLabel(new ImageIcon(image));
	
	JFrame 						rulesFrame = new JFrame();
	private RulesPanel 			rulesPanel = new RulesPanel(this);
	
	Frame(String title){
		super(title);
		
		bindKeys();
		
		graphics.setColor(new Color(0,0,0));
		graphics.drawRect(0, 0, image.getWidth(), image.getHeight());
		ant.x = image.getWidth()/2;
		ant.y = image.getHeight()/2;
		
		
		speedBar.setMinimum(1);
		speedBar.setMaximum(1000);
		speedBar.setValue(100);
		speedBar.setToolTipText("Simulation speed(fps)");
		add(picture, BorderLayout.CENTER);
		
		Image img = new ImageIcon(Frame.class.getResource("/refresh.png")).getImage().getScaledInstance(17, 17, java.awt.Image.SCALE_SMOOTH);  
		refresh.setIcon(new ImageIcon(img));
		refresh.addActionListener(l -> refreshSim());
		
		np.add(startSim);
		np.add(speedBar);
		np.add(editRules);
		np.add(refresh);
		add(np, BorderLayout.NORTH);
		rulesFrame.setLocation(500, 200);
		rulesFrame.add(rulesPanel);
		rulesFrame.pack();
		
		startSim.addActionListener(l -> {
			if(isRunning) {
				worker.cancel(true);
				startSim.setText("Start Simulation");
				isRunning = false;
			} else {
				worker = new SimulationWorker(ant, picture, image, speedBar, cycle);
				worker.execute();
				startSim.setText("Stop Simulation");
				isRunning = true;
			}
		});
		editRules.addActionListener(l -> {
			if(rulesFrame.isVisible())
				rulesFrame.setVisible(false);
			else rulesFrame.setVisible(true);
		});
	}
	
	private void refreshSim() {
		if(isRunning) {
			worker.cancel(true);
			startSim.setText("Start Simulation");
			isRunning = false;
		}
		graphics.setColor(new Color(0,0,0));
		graphics.fillRect(0, 0, 550, 550);
		picture.repaint();
		ant.x = image.getWidth()/2;
		ant.y = image.getHeight()/2;
		ant.orient = 0;
		
	}
	
	private void bindKeys() {
		speedBar.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), "decrease");
		speedBar.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), "increase");
		speedBar.getActionMap().put("decrease", new AbstractAction() {
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				speedBar.setValue(speedBar.getValue()-20);
			}
		});
		speedBar.getActionMap().put("increase", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				speedBar.setValue(speedBar.getValue()+20);
			}
		});

		startSim.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "start");
		startSim.getActionMap().put("start", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				startSim.doClick();
			}
		});
		
		editRules.setMnemonic('E');
		editRules.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "none");
		editRules.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_E, 0), "edit");
		editRules.getActionMap().put("edit", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				editRules.doClick();
			}
		});
		
		refresh.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "none");
		refresh.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_R, 0), "ref");
		refresh.getActionMap().put("ref", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				refresh.doClick();
			}
		});
	}
	
	public static void main(String[] args) {
		Frame frame = new Frame("Langton's Ant");
		frame.setLocation(300, 60);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setIconImage(new ImageIcon(Frame.class.getResource("/logo.png")).getImage());
		frame.pack();
		frame.setVisible(true);
	}
}
