package paleale.frisky.main;

public enum Preset {
	None, Simple, Chaotic,
	Highway, ColorHighway, WideHighway, ZigzagHighway,
	Triangle, Triangle2, Pyramid, RightTriangle, Fill, BounceFill, SpiralFill, SpiralFill2,
	MandelbrotArt, DepthArt, DepthArt2, DepthArt3, InsaneArt, FractalArt, FractalArt2, FractalArt3
}
