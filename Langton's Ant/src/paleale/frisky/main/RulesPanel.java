package paleale.frisky.main;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class RulesPanel extends JPanel{
	private static final long serialVersionUID = -5759470523820053537L;
	private static final Color[] DEF_PRESET_COLORS = {new Color(0,0,0), new Color(255,0,0), new Color(0,255,0),
			new Color(0,255,255), new Color(255,255,0), new Color(255,0,255), new Color(125,125,125),
			new Color(125,0,0), new Color(0,125,0), new Color(0,0,255), new Color(125,125,0), new Color(125,0,125)
			};
	
	JPanel cp = new JPanel(new GridLayout(0, 2, 5, 5));
	JPanel sp = new JPanel();
	JButton btnOK = new JButton("Ok");
	JButton[] colorBtn = new JButton[12];
	@SuppressWarnings("unchecked")
	JComboBox<Direction>[] directionCombo = new JComboBox[12];
	JComboBox<Preset> presetCombo = new JComboBox<>(Preset.values());
	
	public RulesPanel(Frame frame) {
		setLayout(new BorderLayout());
		add(cp, BorderLayout.CENTER);
		sp.add(btnOK);
		add(sp, BorderLayout.SOUTH);
		ActionListener l = e -> {
			Color c = JColorChooser.showDialog(null, "Choose a color", Color.RED);
			((JButton)e.getSource()).setBackground(c == null ? Color.BLACK : c);
			
		};
		colorBtn[0] = new JButton();
		colorBtn[0].setEnabled(false);
		colorBtn[0].setBackground(DEF_PRESET_COLORS[0]);
		for(int i = 1; i < 12; i++) {
			colorBtn[i] = new JButton();
			colorBtn[i].addActionListener(l);
		}
		cp.add(new JLabel("Presets"));
		cp.add(presetCombo);
		for(int i = 0; i < 12; i++) {
			directionCombo[i] = new JComboBox<>(Direction.values());
			cp.add(colorBtn[i]);
			cp.add(directionCombo[i]);
		}
		getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "ok");
		getActionMap().put("ok", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				btnOK.doClick();
			}
		});
		btnOK.addActionListener(e -> {
			int size = 0;
			for(int i = 0; i < 12; i++)
				if(directionCombo[i].getSelectedIndex() != 0)
					size++;
			frame.cycle = new ColorDir[size];
			size = 0;
			for(int i = 0; i < 12; i++)
				if(directionCombo[i].getSelectedIndex() != 0)
					frame.cycle[size++] = new ColorDir(colorBtn[i].getBackground().getRGB(), (Direction)directionCombo[i].getSelectedItem());
			frame.rulesFrame.setVisible(false);
		});
		presetCombo.addActionListener(e -> {
			Preset p = (Preset) presetCombo.getSelectedItem();
			directionCombo[0].setSelectedIndex(0);
			for(int i = 1; i < 12; i++) {
					colorBtn[i].setBackground(null);
					directionCombo[i].setSelectedIndex(0);
			}
			switch(p) {
			case Simple:
				parse("RL");
				break;
			case Chaotic:
				parse("RLR");
				break;
			case Highway:
				parse("LLR");
				break;
			case ColorHighway:
				parse("RLRLRLLRLR");
				break;
			case WideHighway:
				parse("RRLRLLRLRR");
				break;
			case ZigzagHighway:
				parse("LLRRRLRLRLLR");
				break;
			case Triangle:
				parse("RRLRLRR");
				break;
			case Triangle2:
				parse("RRLLLRRRRRLR");
				break;
			case Pyramid:
				parse("RRLLLRLLLRRR");
				break;
			case RightTriangle:
				parse("RRLRLLRRRRRR");
				break;
			case Fill:
				parse("RRLRR");
				break;
			case BounceFill:
				parse("LRRRRRLLR");
				break;
			case SpiralFill:
				parse("LRRRRLLLRRR");
				break;
			case SpiralFill2:
				parse("RLLLLRRRLLLR");
				break;
			case MandelbrotArt:
				parse("LLRR");
				break;
			case DepthArt:
				parse("RLLR");
				break;
			case DepthArt2:
				parse("RRRRLRRRLLRR");
				break;
			case DepthArt3:
				parse("LLRRLRRRRRRR");
				break;
			case InsaneArt:
				parse("LLRLLLRRRRR");
				break;
			case FractalArt:
				parse("LLRRRLRRRRR");
				break;
			case FractalArt2:
				parse("LRLLRRRRR");
				break;
			case FractalArt3:
				parse("LRLRLLLLLLLR");
			default:
			}
		});
	}
	
	/**Parses string specifying a preset - consecutive turns the ant will be making.
	 * Then sets values of buttons/combos to store the preset.
	 * @param s string to parse
	 */
	private void parse(String s) {
		char[] dirs = s.toCharArray();
		directionCombo[0].setSelectedIndex(findDirection(dirs[0]));
		for(int i = 1, l = dirs.length; i < l; i++) {
			colorBtn[i].setBackground(DEF_PRESET_COLORS[i]);
			directionCombo[i].setSelectedIndex(findDirection(dirs[i]));
		}
	}
	
	/**Converts char associated with direction to integer.
	 * @param c char to convert
	 * @return integer representing a direction
	 */
	private int findDirection(char c) {
		if(c == 'L') return 1;
		if(c == 'R') return 2;
		if(c == 'F') return 3;
		if(c == 'U') return 4;
		return -1;
	}
}
