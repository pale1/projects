package paleale.frisky.main;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.SwingWorker;

public class SimulationWorker extends SwingWorker<Void, Void> {

	private Ant ant;
	private JLabel picture;
	private JSlider slider;
	private ColorDir[] cycle;
	private BufferedImage image;

	public SimulationWorker(Ant ant, JLabel picture, BufferedImage image, JSlider slider, ColorDir[] cycle) {
		this.ant = ant;
		this.picture = picture;
		this.image = image;
		this.slider = slider;
		this.cycle = cycle;
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		while(!isCancelled()){
			playOneIteration();
			
			int s = slider.getValue();
			if(s < 100)
				picture.repaint();
			else publish();
			if(s > 480) {
				double rand = Math.random();
				if(s <= (Math.log(1-rand)/-8.)*520+480)
					Thread.sleep(1);
			} else if(s > 100) {
				Thread.sleep((int)(25.-0.05*s));
			} else
				Thread.sleep((int)(500.-s*4.8));
		}
		return null;
	}
	@Override
	protected void process(List<Void> chunks) {
		picture.repaint();
		return;
	}
	
	/**Plays one iteration of Langton's Ant: <br>
	 * Ant changes orientation depending on color it's standing on <br>
	 * Ant repaints the tile it's standing on with the next color in the cycle <br>
	 * Ant steps one pixel forward
	 */
	private void playOneIteration() {
		int color = image.getRGB(ant.x, ant.y);
		for(int i = 0, l = cycle.length; i < l; i++) {
			if(color == cycle[i].color) {
				switch(cycle[i].dir) {
				case Left: ant.orient = (ant.orient+3)%4; break;
				case Right: ant.orient = (ant.orient+1)%4; break;
				case Turn: ant.orient = (ant.orient+2)%4; break;
				default:
				}
				image.setRGB(ant.x, ant.y, cycle[(i+1)%l].color);
				break;
			}
		}
		switch(ant.orient) {
		case 0: ant.y--; break;
		case 1: ant.x++; break;
		case 2: ant.y++; break;
		case 3: ant.x--; break;
		}
	}
}
