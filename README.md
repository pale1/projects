This project contains various apps that I've made throughout the years.
Used languages: Java, C#

## Projects

- [ ] Chaos Game - graphical simulation inspired by a [youtube video](https://www.youtube.com/watch?v=kbKtFN71Lfs)
- [ ] Chess Game - simple chess with GUI written in Java
- [ ] Frisky - text editor for assembly and execution of mnemonic code for FRISC(FER-RISC) processor architecture
- [ ] Langton's Ant - graphical simulation inspired by a [youtube video](https://www.youtube.com/watch?v=1X-gtr4pEBU)
- [ ] Solution/Game - a skeleton of a 2D game, written in C#, with inventory system and ability to move on the map. Discontinued due to college obligations
- [ ] WowApp - application with GUI, inspired by game World of Warcraft
