using System;
using System.Drawing;
using System.Windows.Forms;
using Game.IO;
using Game.Rendering;

namespace Game.Component
{
    
    /**
     * Serves as a base building component for classes that contain multiple components that can be interacted with a mouse
     */
    public abstract class ComponentHolder : IMouseListener, IDisposable, IRenderable 
    {
        protected GComponent[] Components;
        protected GComponent GcMouseOver;

        public void OnMouseEnter() { }

        public void OnMouseLeave() { }

        public void OnMouseMoved(MouseEventArgs e)
        {
            Point loc = e.Location;
            foreach (var c in Components)
            {
                if (c.ContainsPoint(loc)) // is mouse over this component
                {
                    if (GcMouseOver != null) // was mouse previously over another component
                    {
                        if (GcMouseOver != c) // previous component is not this one, leave previous component and enter new component
                        {
                            GcMouseOver.OnMouseLeave();

                            GcMouseOver = c;
                            GcMouseOver.OnMouseEnter();
                        }
                    }
                    else // mouse previously wasn't over another component
                    {
                        GcMouseOver = c;
                        GcMouseOver.OnMouseEnter();
                    }

                    GcMouseOver.OnMouseMoved(e);
                    return;
                }
            }

            if (GcMouseOver != null) // mouse is not over any components now
            {
                GcMouseOver.OnMouseLeave();
                GcMouseOver = null;
            }
        }
        
        public void OnMouseHover(Point mouse)
        {
            GcMouseOver?.OnMouseHover(mouse);
        }

        public void OnMouseDown(MouseEventArgs e)
        {
            GcMouseOver?.OnMouseDown(e);
        }

        public void OnMouseUp(MouseEventArgs e)
        {
            GcMouseOver?.OnMouseUp(e);
        }
        
        public void OnMouseWheel(MouseEventArgs e)
        {
            GcMouseOver?.OnMouseWheel(e);
        }

        public void Render(IRenderer r)
        {
            // render components from last to first, since mouseover check is from first to last
            for (int i = Components.Length - 1; i >= 0; i--)
                Components[i].Render(r);
        }

        public void Dispose()
        {
            foreach (GComponent c in Components)
                c.Dispose();
        }
    }
}