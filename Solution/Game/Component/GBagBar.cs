﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Component
{
    public class GBagBar : GInventoryMesh
    {
        private const int BAG_SLOTS = 5;
        private const int MIN_BTN_WIDTH = 0;

        public GBagBar() : base("Bag bar", new Point(
                Const.SCREEN_WIDTH - BAG_SLOTS * GSlot.SIZE - MIN_BTN_WIDTH,
                Const.SCREEN_HEIGHT - GSlot.SIZE),
                BAG_SLOTS, 1)
        { }
    }
}
