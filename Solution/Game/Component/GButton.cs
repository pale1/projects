﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Rendering;

namespace Game.Component
{
    public class GButton : GComponent
    {
        public Action Action { get; set; }

        public GButton(string name, Rectangle location) : base(name, location)
        {
            Action = () => { };
        }

        public GButton(string name, Rectangle location, Action action) : base(name, location)
        {
            Action = action;
        }

        public override void Render(IRenderer r)
        {
            r.Render(this);
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            Action();
        }

        public override void Dispose() { }
    }
}
