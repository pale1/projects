﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.IO;
using Game.Rendering;

namespace Game.Component
{
    public abstract class GComponent : IMouseListener, IRenderable, IDisposable
    {
        public string Name { get; }
        public Rectangle Location { get; protected set; }
        public bool IsMouseOver { get; private set; }

        public GComponent() : this("") { }

        public GComponent(string name, Rectangle location = new Rectangle())
        {
            Name = name;
            Location = location;
        }

        public abstract void Render(IRenderer r);

        public virtual void OnMouseEnter()
        {
            IsMouseOver = true;
        }
        public virtual void OnMouseLeave()
        {
            IsMouseOver = false;
        }

        public virtual void OnMouseMoved(MouseEventArgs e) { }
        public virtual void OnMouseHover(Point mouse) { }

        public virtual void OnMouseDown(MouseEventArgs e) { }
        public virtual void OnMouseUp(MouseEventArgs e) { }
        public virtual void OnMouseWheel(MouseEventArgs e) { }

        public virtual void KeyDown(KeyEventArgs e) { }

        public bool ContainsPoint(Point p)
        {
            return Location.Contains(p);
        }

        public abstract void Dispose();
    }
}
