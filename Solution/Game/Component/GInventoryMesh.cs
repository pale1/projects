﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Model;
using Game.Model.Item2;
using Game.Rendering;
using Game.XML;

namespace Game.Component
{
    public class GInventoryMesh : GComponent
    {
        private readonly GSlot[,] _components;
        private GSlot GCMouseOver;
        
        public GInventoryMesh(string name, Point location, int xComponents, int yComponents)
            : base(name, new Rectangle(location.X, location.Y, (int)(xComponents*RTC.SCALED_SLOT_SIZE), (int)(yComponents*GSlot.SIZE*Const.HUD_SCALING_FACTOR)))
        {
            if (xComponents < 1 || yComponents < 1)
                throw new ArgumentException("Invalid arguments!");

            //int x = Math.Min(xComponents, location.Width / GSlot.SIZE);
            //int y = Math.Min(yComponents, location.Height / GSlot.SIZE);
            //xPadd = (location.Width - x * GSlot.SIZE) / 2;
            //yPadd = (location.Height - y * GSlot.SIZE) / 2;
            _components = new GSlot[xComponents, yComponents];
            for (int i = 0; i < xComponents; i++)
            {
                for(int j = 0; j < yComponents; j++)
                {
                    _components[i, j] = new GSlot($"{i} {j}", new Point(
                        location.X + i*(int)RTC.SCALED_SLOT_SIZE, 
                        location.Y + j*(int)RTC.SCALED_SLOT_SIZE));
                }
            }

            _components[0, 0].Contained = new SlotItem(ItemFactory.GetItem(0), 5);
            _components[0, 1].Contained = new SlotItem(ItemFactory.GetItem(1), 1);
            _components[0, 2].Contained = new SlotItem(ItemFactory.GetItem(2), 4);
            _components[0, 3].Contained = new SlotItem(ItemFactory.GetItem(3), 50);
            _components[1, 0].Contained = new SlotItem(ItemFactory.GetItem(4), 29);
            _components[1, 1].Contained = new SlotItem(ItemFactory.GetItem(5), 7);
            _components[1, 2].Contained = new SlotItem(ItemFactory.GetItem(6), 6);
            _components[1, 3].Contained = new SlotItem(ItemFactory.GetItem(7), 15);
            _components[2, 0].Contained = new SlotItem(ItemFactory.GetItem(3), 50);
        }

        public override void Render(IRenderer r)
        {
            foreach (GComponent c in _components)
                c.Render(r);
        }

        public override void OnMouseLeave()
        {
            GCMouseOver?.OnMouseLeave();
            GCMouseOver = null;
            base.OnMouseLeave();
        }

        public override void OnMouseMoved(MouseEventArgs e)
        {
            int x = (int)((e.X - Location.X) / RTC.SCALED_SLOT_SIZE);
            int y = (int)((e.Y - Location.Y) / RTC.SCALED_SLOT_SIZE);
            if (x <= -1 || x >= _components.GetLength(0) || y <= -1 || y >= _components.GetLength(1))
                return;

            if (GCMouseOver != _components[x, y]) // mouse moved to another component
            {
                GCMouseOver?.OnMouseLeave();
                GCMouseOver = _components[x, y];
                GCMouseOver.OnMouseEnter();
            }
            GCMouseOver.OnMouseMoved(e);
        }

        public override void OnMouseHover(Point mouse)
        {
            GCMouseOver?.OnMouseHover(mouse);
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            GCMouseOver?.OnMouseDown(e);
        }
        public override void OnMouseUp(MouseEventArgs e)
        {
            GCMouseOver?.OnMouseUp(e);
        }
        public override void KeyDown(KeyEventArgs e)
        {
            GCMouseOver?.KeyDown(e);
        }

        // executes when PlayState's key handler for item use fires
        public void ItemUsed(GameEnvironment e)
        {
            GCMouseOver?.ItemUsed(e);
        }

        public override void Dispose()
        {
            foreach (GComponent c in _components)
                c.Dispose();
        }
    }
}
