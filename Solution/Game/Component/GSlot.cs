﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Hud;
using Game.Hud.Tooltip;
using Game.IO;
using Game.Model;
using Game.Model.Item2;
using Game.Rendering;

namespace Game.Component
{
    public class GSlot : GComponent
    {
        public const int SIZE = 36;
        private static CustomCursor _cursor = CustomCursor.Instance;

        private static bool _leftClickDown;
        private static bool _rightClickDown;
        
        public SlotItem Contained { get; set; }

        public GSlot(string name, Point location) : base(name, new Rectangle(location, new Size((int)(SIZE*Const.HUD_SCALING_FACTOR), (int)(SIZE*Const.HUD_SCALING_FACTOR)))) { }

        public bool HasItem()
        {
            return Contained != null;
        }
        
        public override void Render(IRenderer r)
        {
            r.Render(this);
        }

        public override void OnMouseHover(Point mouse)
        {
            base.OnMouseHover(mouse);
            if (HasItem() && _cursor.Tooltip == null)
                _cursor.Tooltip = new CustomTooltip(Contained.GetTooltipStyle(), new ItemTooltipContent(Contained.Data));
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            lock (CustomCursor.MouseEventLock)
            {
                // if LCLICK pressed
                if (e.Button == MouseButtons.Left)
                {
                    _leftClickDown = true;
                    // if slot has item and no dragged item
                    if (HasItem() && !_cursor.IsDragging())
                    {
                        // drag all items from the slot
                        DragItems(Contained.Amount);
                    }
                    // if dragging same item as the one in slot
                    else if (HasItem() && Contained.Data.Id == _cursor.DraggedItem.Data.Id)
                    {
                        // stack the two items
                        DropItems(_cursor.DraggedItem.Amount);
                    }
                    // if cursor is dragging
                    else if (_cursor.IsDragging())
                    {
                        // if slot is empty
                        if (!HasItem())
                        {
                            // drop all the dragged items in this slot
                            DropItems(_cursor.DraggedItem.Amount);
                        }
                        // if slot has same item id as dragged item, and can fit more items in stack
                        else if (_cursor.DraggedItem.Data.Id == Contained.Data.Id &&
                                 Contained.Amount < Contained.Data.StackSize)
                        {
                            // drop some or all of dragged items in this slot
                            DropItems(_cursor.DraggedItem.Amount);
                        }
                        else
                        {
                            // swap dragged item with this item
                            SwapItems();
                        }
                    }
                }
                // if RCLICK pressed
                else if (e.Button == MouseButtons.Right)
                {
                    _rightClickDown = true;
                    // if nothing dragged and this slot has item
                    if (_cursor.DraggedItem == null && HasItem())
                    {
                        // drag half of the amount of the item
                        int dragAmount = Contained.Amount / 2 + Contained.Amount % 2;
                        DragItems(dragAmount);
                    }
                    // if item is dragged
                    else if (_cursor.IsDragging())
                    {
                        // if slot is empty
                        // if slot has same item id as dragged item, and can fit more items in stack
                        if (!HasItem() || (_cursor.DraggedItem.Data.Id == Contained.Data.Id &&
                                           Contained.Amount < Contained.Data.StackSize))
                        {
                            // place 1 of item in slot
                            DropItems(1);
                        }
                    }
                }
            }
        }

        public override void OnMouseUp(MouseEventArgs e)
        {
            lock (CustomCursor.MouseEventLock)
            {
                // if LCLICK released when dragging item that is from another slot
                if (e.Button == MouseButtons.Left)
                {
                    _leftClickDown = false;
                    if (_cursor.IsDragging() && _cursor.DraggedSlot != this)
                    {
                        // if dragged item is same as in this slot
                        if (HasItem() && Contained.Data.Id == _cursor.DraggedItem.Data.Id && Contained.Amount < Contained.Data.StackSize)
                        {
                            // stack the two items
                            DropItems(_cursor.DraggedItem.Amount);
                        }
                        // if dragged item is not the same as in this slot
                        else if (!HasItem() || Contained.Data.Id != _cursor.DraggedItem.Data.Id)
                        {
                            // moving item / swapping 2 different items
                            SwapItems();
                        }
                    }
                }
                else if (e.Button == MouseButtons.Right)
                {
                    _rightClickDown = false;
                }
            }
        }

        public new virtual void OnMouseEnter()
        {
            base.OnMouseEnter();
            lock (CustomCursor.MouseEventLock)
            {
                if (_leftClickDown && HasItem()
                                   && (!_cursor.IsDragging() || _cursor.DraggedItem.Data.Id == Contained.Data.Id) // if dragging same items as in slot
                                   && _cursor.DraggedAmount() < Contained.Data.StackSize) // if dragging less than full stack of item
                {
                    DragItems(Contained.Amount);
                }
                else if (_rightClickDown && !HasItem() && _cursor.IsDragging())
                {
                    DropItems(1);
                }
            }
        }
        
        public new virtual void OnMouseLeave()
        {
            base.OnMouseLeave();
            _cursor.Tooltip?.Dispose();
            _cursor.Tooltip = null;
        }
        
        /**
         * Executes when PlayState's key handler for item use fires
         */
        public void ItemUsed(GameEnvironment e)
        {
            Contained?.Data.OnUse(e);
        }
        
        /**
         * Move items from this slot to cursor
         */
        private void DragItems(int dragAmount)
        {
            if (!HasItem())
                throw new Exception("DragSlotItems fired when no items in this slot");
            if (_cursor.IsDragging() && _cursor.DraggedItem.Data.Id != Contained.Data.Id)
                throw new Exception("DragSlotItems fired when dragged item and slot items aren't the same");

            dragAmount = Math.Min(dragAmount, Contained.Data.StackSize - _cursor.DraggedAmount());
            if (dragAmount == 0)
                return;
            Contained.Amount -= dragAmount;

            // add dragAmount to the cursor's dragged item number
            _cursor.SetDraggedSlot(this, itemAmount: _cursor.DraggedAmount() + dragAmount);
            
            Sound.Player.Play(Contained.GetDragSound());
            
            // if dragging all the slot items, delete item from this slot
            if (Contained.Amount == 0)
                DeleteSlotItem();
        }
        
        /**
         * Move items from cursor to this slot
         */
        private void DropItems(int dropAmount)
        {
            if (!_cursor.IsDragging())
                throw new Exception("DropItems fired but nothing dragged by cursor");
            
            dropAmount = Math.Min(dropAmount, _cursor.DraggedItem.Data.StackSize - (Contained?.Amount ?? 0));
            if (dropAmount == 0)
                return;
            
            if (!HasItem())
                Contained = new SlotItem(_cursor.DraggedItem.Data, dropAmount);
            else
                Contained.Amount += dropAmount;
            
            _cursor.SetDraggedAmount(_cursor.DraggedItem.Amount - dropAmount);
            
            Sound.Player.Play(Contained.GetDropSound());
        }

        /**
         * Swaps items dragged by cursor with items in this slot
         */
        private void SwapItems()
        {
            if (!_cursor.IsDragging())
                throw new Exception("SwapItems fired but nothing dragged by cursor");
            
            Item clone = Contained?.Clone();
            int amount = clone?.Amount ?? 0;
            DeleteSlotItem();
            Contained = new SlotItem(_cursor.DraggedItem);
            
            _cursor.SetDraggedSlot(this, clone, amount);
            
            Sound.Player.Play(Contained.GetDropSound());
        }

        private void DeleteSlotItem()
        {
            Contained?.ItemAmountTextLayout?.Dispose();
            Contained = null;
        }
        
        public override void Dispose()
        {
            Contained?.Dispose();
        }
    }
}
