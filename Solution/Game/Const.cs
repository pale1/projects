﻿using System.Drawing;
using System.Windows.Forms;

// ReSharper disable InconsistentNaming
namespace Game
{
    public static class Const
    {
        public static int FPS = 120;

        public const int S_CHUNK = 16;
        
        /**
         * Resolution of terrain tile textures
         */
        public const int S_TILE = 32;
        /**
         * Resolution of cursor texture
         */
        public const int S_CURSOR = 32;

        /**
         * How many screen pixels will each terrain tile texture pixel be wide/tall
         */
        public static float ENVIRONMENT_SCALING_FACTOR = 2f;
        
        /**
         * How many screen pixels will each HUD texture pixel be wide/high
         */
        public static float HUD_SCALING_FACTOR = 2f;
        
        /**
         * How many screen pixels will cursor texture pixel be wide/high
         */
        public static float CURSOR_SCALING_FACTOR = 2f;

        public static float DPI_SCALE_X;
        public static float DPI_SCALE_X_REC;
        public static float DPI_SCALE_Y;
        public static float DPI_SCALE_Y_REC;

        public static int SCREEN_WIDTH = 600;  // set to client screen width
        public static int SCREEN_HEIGHT = 600; // set to client screen height

        public static readonly string SOUNDS_PATH = Application.StartupPath + "/sound/";
        public static readonly string MAPS_PATH = Application.StartupPath + "/maps/";
        public static readonly string XML_PATH = Application.StartupPath + "/xml_data/";
        public static readonly string TEXTURES_PATH = Application.StartupPath + "/textures/";
        public const string ITEM_TEXTURES_FOLDER = "item_textures";
        public const string TILE_TEXTURES_FOLDER = "tile_textures";
        public const string FONT_TEXTURES_FOLDER = "font_textures";
        public const string TOOLTIP_TEXTURES_FOLDER = "tooltip_textures";
        public const string MAP_TEXTURES_FOLDER = "map_textures";

    }
}
