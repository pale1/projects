﻿
namespace Game
{
    partial class Frame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // Frame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(667, 375);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Frame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game";
            this.MouseEnter += new System.EventHandler(this.Frame_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.Frame_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Frame_MouseMove);
            this.MouseHover += new System.EventHandler(this.Frame_MouseHover);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Frame_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Frame_MouseUp);
            this.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.Frame_MouseWheel);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Frame_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Frame_KeyUp);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Frame_FormClosing);
            this.ResumeLayout(false);
        }

        #endregion
    }
}

