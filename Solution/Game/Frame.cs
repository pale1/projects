﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Game.Hud;
using Game.IO;
using Game.Rendering;
using Game.States;
using Game.Text;
using Game.Texture;
using SlimDX.Windows;

namespace Game
{
    public partial class Frame : RenderForm
    {
        public static Frame Instance;
        public static long FrameNum = 0;

        private readonly StateManager _stateManager;

        private bool _mouseInWindow;
        private bool _isFullScreen;
        private bool _skipSleep;

        private Size _windowedSize;

        public static IRenderer Renderer;

        private readonly Thread _gameLoop;
        
        private delegate void WaitFunction(int milliseconds);

        private readonly WaitFunction _waitFunction;

        private Frame()
        {
            // load all used fonts
            FontBank.Init();
            Thread.Sleep(200);
            
            InitializeComponent();

            //Util.CreateChunkCSVs("map1");

            var g = CreateGraphics();
            Const.DPI_SCALE_X = g.DpiX / 96;
            Const.DPI_SCALE_Y = g.DpiY / 96;
            // reciprocal value from actual DPI, so multiply can be always used instead of division
            Const.DPI_SCALE_X_REC = 96 / g.DpiX;
            Const.DPI_SCALE_Y_REC = 96 / g.DpiY;
            g.Dispose();
            
            // hides default windows cursor
            Cursor.Hide();
            
            // WorkingArea is screen size, excluding the windows start bar at the bottom
            Rectangle screen = Screen.PrimaryScreen.WorkingArea;
            Const.SCREEN_WIDTH = screen.Width;
            Const.SCREEN_HEIGHT = screen.Height;
            
            int largerSize = Const.SCREEN_WIDTH > Const.SCREEN_HEIGHT ? Const.SCREEN_WIDTH : Const.SCREEN_HEIGHT;
            int smallerSize = Const.SCREEN_WIDTH < Const.SCREEN_HEIGHT ? Const.SCREEN_WIDTH : Const.SCREEN_HEIGHT;
            
            // set the factor so the screen always shows max 16 tiles of terrain in line, no matter the window size
            Const.ENVIRONMENT_SCALING_FACTOR = largerSize / 16f / Const.S_TILE;
            Const.HUD_SCALING_FACTOR = smallerSize / 500f;      // so the HUD is of similar size regardless of screen resolution
            Const.CURSOR_SCALING_FACTOR = smallerSize / 500f;   // so the cursor is of similar size regardless of screen resolution
            
            RTC.CalculateRuntimeConstants();
                
            EnterFullScreen();

            SetupKeys();
            
            // use SharpDX for rendering
            Renderer = new DXDirectRender(this);
            
            // init all game states
            _stateManager = new StateManager();

            // run game loop in separate thread
            _waitFunction = Const.FPS >= 50 ? (WaitFunction)BusyWait : BusyWaitLessThan50Fps;
            _gameLoop = new Thread(GameLoop);
            _gameLoop.Start();
        }

        private void EnterFullScreen()
        {
            _windowedSize = ClientSize;
            
            //Location = new Point(0, 0); // sets the window location to upper left corner, no effect
            
            // the window will have no border, including the upper edge
            FormBorderStyle = FormBorderStyle.None;
            // maximizes the window to cover whole screen
            WindowState = FormWindowState.Maximized;
            
            // AutoScaleDimensions = new SizeF(6F, 13F); // for design in 96 DPI, but no effect

            _isFullScreen = true;
        }

        private void LeaveFullScreen()
        {
            ClientSize = _windowedSize;
            
            FormBorderStyle = FormBorderStyle.Sizable;
            WindowState = FormWindowState.Normal;
            
            _isFullScreen = false;
        }

        private void SetupKeys()
        {
            int defaultFps = Const.FPS;
            
            // bind key to speed up to 300 FPS
            KeyManager.AddKeyPressedHandler(KeyManager.KeyFor("fast_forward"), () => Const.FPS = 300);
            KeyManager.AddKeyReleasedHandler(KeyManager.KeyFor("fast_forward"), () => Const.FPS = defaultFps);
            
            // bind key to switch to max FPS
            KeyManager.AddKeyPressedHandler(KeyManager.KeyFor("max_fps"), () => _skipSleep = !_skipSleep);
            
            // KeyManager.KeyPressedEh[Keys.Space] += (sender, args) => Const.FPS = 1000;
            // KeyManager.KeyReleasedEh[Keys.Space] += (sender, args) => Const.FPS = oldFps;
            
            // make F12 switch between windowed and fullscreen
            KeyManager.AddKeyPressedHandler(KeyManager.KeyFor("full_screen"), () =>
            {
                if (_isFullScreen) LeaveFullScreen();
                else EnterFullScreen();
            });
            
            // bind I to open inventory
            KeyManager.AddKeyPressedHandler(KeyManager.KeyFor("open_inventory"), () =>
            {
                if (_stateManager.CurrentStateNum == State.Play)
                    _stateManager.ChangeState(State.Inventory);
                else if (_stateManager.CurrentStateNum == State.Inventory)
                    _stateManager.ChangeState(State.Play);
            });
            
            // bind M to open map
            KeyManager.AddKeyPressedHandler(KeyManager.KeyFor("open_map"), () =>
            {
                if (_stateManager.CurrentStateNum == State.Play)
                    _stateManager.ChangeState(State.Map);
                else if (_stateManager.CurrentStateNum == State.Map)
                    _stateManager.ChangeState(State.Play);
            });
            
            // make esc close app
            KeyManager.AddKeyPressedHandler(Keys.Escape, Close);
        }

        private void GameLoop()
        {
            float remainder = 0f;
            var gameLoopTimer = new GameLoopTimer();
            while (true)
            {
                gameLoopTimer.StartTimer();

                // Update keys
                KeyManager.Update();

                // Update game
                _stateManager.CurrentState.Update();

                // Draw screen
                _stateManager.CurrentState.Render(Renderer);
                //BeginInvoke((MethodInvoker)delegate { Refresh(); });
                //Invalidate();
                
                long elapsedMs = gameLoopTimer.ElapsedDebug();
                float fillerMs = 1000f / Const.FPS - elapsedMs + remainder;
                remainder = fillerMs - (int)fillerMs;
                if (fillerMs > 0 && !_skipSleep)
                    _waitFunction((int)fillerMs);
                
                FrameNum++;
            }
        }

        private static void BusyWait(int millis)
        {
            //long untilMillis = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond + millis - 5;
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            while (stopwatch.ElapsedMilliseconds < millis) { }
            stopwatch.Stop();
        }
        
        private static void BusyWaitLessThan50Fps(int millis)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            Thread.Sleep(10);
            while (stopwatch.ElapsedMilliseconds < millis) { }
            stopwatch.Stop();
        }
        
        private void Frame_MouseLeave(object sender, EventArgs e)
        {
            _mouseInWindow = false;
            _stateManager.CurrentState.OnMouseLeave();
        }

        private void Frame_MouseEnter(object sender, EventArgs e)
        {
            _mouseInWindow = true;
            _stateManager.CurrentState.OnMouseEnter();
        }

        private void Frame_MouseHover(object sender, EventArgs e)
        {
            if (!_mouseInWindow) return;
            
            var c = PointToClient(Cursor.Position);
            _stateManager.CurrentState.OnMouseHover(c);
        }

        private void Frame_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_mouseInWindow) return;
            
            _stateManager.CurrentState.OnMouseDown(new MouseEventArgs(e.Button, e.Clicks, e.X * Const.SCREEN_WIDTH / ClientSize.Width, e.Y * Const.SCREEN_HEIGHT / ClientSize.Height, e.Delta));
        }

        private void Frame_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_mouseInWindow) return;
            
            MouseEventArgs ne = new MouseEventArgs(e.Button, e.Clicks, e.X * Const.SCREEN_WIDTH / ClientSize.Width,
                e.Y * Const.SCREEN_HEIGHT / ClientSize.Height, e.Delta);
            CustomCursor.Instance.Location = ne.Location;
            _stateManager.CurrentState.OnMouseMoved(ne);
            ResetMouseEventArgs();
        }

        private void Frame_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_mouseInWindow) return;
            
            _stateManager.CurrentState.OnMouseUp(new MouseEventArgs(e.Button, e.Clicks, e.X * Const.SCREEN_WIDTH / ClientSize.Width, e.Y * Const.SCREEN_HEIGHT / ClientSize.Height, e.Delta));
        }
        
        private void Frame_MouseWheel(object sender, MouseEventArgs e)
        {
            if (!_mouseInWindow) return;
            
            _stateManager.CurrentState.OnMouseWheel(new MouseEventArgs(e.Button, e.Clicks, e.X * Const.SCREEN_WIDTH / ClientSize.Width, e.Y * Const.SCREEN_HEIGHT / ClientSize.Height, e.Delta));
        }

        private void Frame_KeyDown(object sender, KeyEventArgs e)
        {
            KeyManager.SetKeyDown(e.KeyCode);
            //_stateManager.CurrentState.KeyDown(e);
        }

        private void Frame_KeyUp(object sender, KeyEventArgs e)
        {
            KeyManager.SetKeyUp(e.KeyCode);
        }

        private void Frame_FormClosing(object sender, FormClosingEventArgs e)
        {
            _gameLoop.Abort();
            
            Dispose();
            
            Application.Exit();
        }

        private new void Dispose()
        {
            Renderer.Dispose();
            Loaders.Dispose();
            FontBank.Dispose();
            SpriteSheets.Dispose();
            CustomCursor.Instance.Dispose();
            Sound.Player.Dispose();
            _stateManager.Dispose();
            base.Dispose();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // if (Environment.OSVersion.Version.Major >= 6)
            //     SetProcessDPIAware(); // maybe useless?

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Instance = new Frame();
            
            Application.Run(Instance);
        }

        // [DllImport("user32.dll")]
        // private static extern bool SetProcessDPIAware();


    }
}
