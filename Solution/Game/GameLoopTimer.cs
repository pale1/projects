﻿using System;
using System.Diagnostics;

namespace Game
{
    public class GameLoopTimer
    {
        private const int PRINT_ITER = 500;

        private bool _started;
        
        private long _iterations;
        private long _minIterMillis = 10000;
        private long _maxIterMillis;

        private readonly Stopwatch _stopwatch; // measures how long a single frame took to complete
        private readonly Stopwatch _sumStopwatch; // more accurately measures how long bulks of PRINT_ITER frames took to complete

        public GameLoopTimer()
        {
            _stopwatch = new Stopwatch();
            _sumStopwatch = new Stopwatch();
            _sumStopwatch.Restart();
        }

        public void StartTimer()
        {
            _started = true;
            _stopwatch.Restart();
        }
        
        /// <summary>
        /// Returns the elapsed number of milliseconds elapsed since the call of StartTimer method.
        /// <para/>
        /// Each new call of this method requires a prior new call of StartTimer method.
        /// </summary>
        public long Elapsed()
        {
            if (_started == false)
                throw new InvalidOperationException("Elapsed called without prior Start timer call!");
            
            _started = false;
            return _stopwatch.ElapsedMilliseconds;
        }

        /// <summary>
        /// Returns the elapsed number of milliseconds elapsed since the call of StartTimer method.
        /// Does additional debug prints after predetermined number of iterations
        /// <para/>
        /// Each new call of this method requires a prior new call of StartTimer method.
        /// </summary>
        public long ElapsedDebug()
        {
            if (_started == false)
                throw new InvalidOperationException("Elapsed called without prior Start timer call!");
            
            _started = false;
            long elapsedMillis = _stopwatch.ElapsedMilliseconds;

            if (elapsedMillis < _minIterMillis)
                _minIterMillis = elapsedMillis;
            if (elapsedMillis > _maxIterMillis)
                _maxIterMillis = elapsedMillis;
            
            if (++_iterations == PRINT_ITER)
            {
                Debug.WriteLine("Last {0} iterations took {1} milliseconds ({2:0.##} ms per iteration)",
                    _iterations, _sumStopwatch.ElapsedMilliseconds, 1f * _sumStopwatch.ElapsedMilliseconds / _iterations);
                Debug.WriteLine("   Min iteration duration: {0} ms", _minIterMillis);
                Debug.WriteLine("   Max iteration duration: {0} ms", _maxIterMillis);
                Debug.WriteLine("Const  FPS: " + Const.FPS);
                Debug.WriteLine("Actual FPS: " + 1000*PRINT_ITER/_sumStopwatch.ElapsedMilliseconds);

                _iterations = 0;
                _minIterMillis = 10000;
                _maxIterMillis = 0;
                _sumStopwatch.Restart();
            }
            
            return elapsedMillis;
        }
    }
}