using System;
using System.Drawing;
using Game.Component;
using Game.Hud.Tooltip;
using Game.IO;
using Game.Model.Item2;
using Game.Rendering;
using Game.Text;
using SlimDX.DirectWrite;

namespace Game.Hud
{
    public class CustomCursor : IDisposable, IRenderable
    {
        public static readonly CustomCursor Instance = new CustomCursor();

        public static readonly object MouseEventLock = new object();
        
        public Point Location { get; set; }
        
        public GSlot DraggedSlot { get; private set; }

        public Item DraggedItem { get; private set; }

        // reuse the same static TextFormat for all rendering of item amount (cursor)
        private static readonly TextFormat ItemAmountTextFormat = FontBank.CreateTextFormat(FontBank.InventoryFont, fontSize: 8f * Const.HUD_SCALING_FACTOR);
        
        // for rendering item amount in cursor
        public TextLayout ItemAmountTextLayout;

        public CustomTooltip Tooltip { get; set; }
        
        private CustomCursor() { }
        
        public void Render(IRenderer r)
        {
            var texture = DraggedItem == null ? 
                Loaders.TEXTURE_DX.GetCachedResource($"cursor") :
                Loaders.TEXTURE_DX.GetCachedResource($"cursor_held");
            int cursorSize = (int)(Const.S_CURSOR * Const.CURSOR_SCALING_FACTOR);
            
            r.DrawBitmap(texture, new Rectangle(Location.X - cursorSize / 2, Location.Y - cursorSize / 2, cursorSize, cursorSize));
            
            if (IsDragging())
            {
                int itemIconSize = (int)(RTC.SCALED_SLOT_SIZE * 0.5);
                r.DrawBitmap(Loaders.TEXTURE_DX.GetCachedResource($"{Const.ITEM_TEXTURES_FOLDER}/{DraggedItem.Data.Name}"),
                    new Rectangle(Location.X, Location.Y + cursorSize / 2, itemIconSize, itemIconSize));

                if (DraggedItem.Amount > 1)
                    r.DrawTextLayout(ItemAmountTextLayout, new PointF(Location.X, Location.Y + cursorSize / 2 + itemIconSize / 2), Color.White);
            }
            
            r.Render(this);
            Tooltip?.Render(r);
        }
        
        /**
         * Sets the slot dragged by the mouse, item in slot dragged by mouse, and amount of that item specified by itemAmount.
         * Passing null value or 0 item amount will clear mouse's dragging state
         */
        public void SetDraggedSlot(GSlot slot, Item item = null, int itemAmount = 1)
        {
            if (item == null)
                item = slot.Contained.Clone();
            
            if (slot == null || itemAmount == 0)
            {
                DraggedSlot = null;
                DraggedItem = null;
            }
            else
            {
                DraggedSlot = slot;
                DraggedItem = item;
                SetDraggedAmount(itemAmount);
            }
        }
        
        public void SetDraggedAmount(int amount)
        {
            ItemAmountTextLayout?.Dispose();
            if (amount == 0)
            {
                DraggedSlot = null;
                DraggedItem = null;
            }
            else
            {
                DraggedItem.Amount = amount;
                if (DraggedItem.Amount > 1)
                    ItemAmountTextLayout = FontBank.CreateTextLayout(DraggedItem.Amount.ToString(), ItemAmountTextFormat);
            }
        }

        public bool IsDragging()
        {
            return DraggedItem != null;
        }

        public int DraggedAmount()
        {
            return DraggedItem?.Amount ?? 0;
        }

        public void Dispose()
        {
            ItemAmountTextFormat.Dispose();
            ItemAmountTextLayout?.Dispose();
            Tooltip?.Dispose();
        }
    }
}