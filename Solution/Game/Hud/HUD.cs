﻿using System.Drawing;
using System.Windows.Forms;
using Game.Component;
using Game.Model;
using Game.Model.Entity;

namespace Game.Hud
{
    public class HUD : ComponentHolder
    {
        public HUD(Player player)
        {
            Components = new GComponent[2];
            Components[0] = new PlayerFrame(new Point(50, 50), player);
            Components[1] = new GInventoryMesh("bag", new Point(
                    (int)(Const.SCREEN_WIDTH - 5*RTC.SCALED_SLOT_SIZE),
                    (int)(Const.SCREEN_HEIGHT - 5*RTC.SCALED_SLOT_SIZE)),
                5, 5);
            Components[1] = new PlayerFrame(new Point(50, 50), player);
        }

        public void ItemUsed(GameEnvironment e)
        {
            ((GInventoryMesh)Components[0]).ItemUsed(e);
        }
    }
}
