﻿using System.Drawing;
using System.Windows.Forms;
using Game.Component;
using Game.Model.Entity;
using Game.Rendering;
using Game.Text;
using SlimDX.DirectWrite;
using Point = System.Drawing.Point;
using Size = System.Drawing.Size;

// ReSharper disable InconsistentNaming
namespace Game.Hud
{
    public class PlayerFrame : GComponent
    {
        private Player _player;

        private readonly int FRAME_WIDTH = 100;

        private readonly int BAR_WIDTH;
        private readonly int BAR_HEIGHT;
        private readonly int EDGE;

        // if mouse starts dragging the frame, contains vector from mouse location to frame location, otherwise is null
        private Point _mouseToFrameVec;

        private static TextFormat _playerNameTextFormat = FontBank.CreateTextFormat(FontBank.NameFont, fontSize: 10F * Const.HUD_SCALING_FACTOR);
        private TextLayout _playerNameTextLayout;

        public PlayerFrame(Point location, Player player) 
            : base(player.Name)
        {
            _player = player;
            _playerNameTextLayout = FontBank.CreateTextLayout(_player.Name, _playerNameTextFormat);

            FRAME_WIDTH = (int)(FRAME_WIDTH * Const.HUD_SCALING_FACTOR);
            EDGE = (int)(2 * Const.HUD_SCALING_FACTOR);
            BAR_HEIGHT = (int)(5 * Const.HUD_SCALING_FACTOR);
            BAR_WIDTH = FRAME_WIDTH - 2 * EDGE;
            
            Location = new Rectangle(location,
                new Size(FRAME_WIDTH,
                    (int)(EDGE 
                          + _playerNameTextLayout.Metrics.Height 
                          + EDGE
                          + BAR_HEIGHT // health bar
                          + BAR_HEIGHT // mana bar
                          + EDGE)));
        }
        
        public override void Render(IRenderer r)
        {
            // frame background
            r.FillRectangle(Location, Color.DimGray);
            r.DrawRectangle(Location, Color.Black, 2);
            
            // player name
            Point location = new Point(
                (int)(Location.X + Location.Width / 2 - _playerNameTextLayout.Metrics.Width / 2), 
                Location.Y + EDGE);
            r.DrawTextLayout(_playerNameTextLayout, location, Color.White);
            
            // health bar
            Rectangle healthBar = new Rectangle(
                Location.X + EDGE,
                (int)(Location.Y + EDGE + _playerNameTextLayout.Metrics.Height),
                BAR_WIDTH, BAR_HEIGHT
            );
            r.FillRectangle(healthBar, Color.Brown);
            r.FillRectangle(new Rectangle(healthBar.Location, new Size(healthBar.Width * _player.Health / _player.BaseHealth, healthBar.Height)), Color.Red);
            r.DrawRectangle(healthBar, Color.Black);
            
            // mana bar
            Rectangle manaBar = new Rectangle(
                Location.X + EDGE,
                healthBar.Y + BAR_HEIGHT,
                BAR_WIDTH, BAR_HEIGHT
            );
            r.FillRectangle(manaBar, Color.DarkBlue);
            r.FillRectangle(new Rectangle(manaBar.Location, new Size(manaBar.Width * _player.Mana / _player.BaseMana, manaBar.Height)), Color.Blue);
            r.DrawRectangle(manaBar, Color.Black);
            
            
            //r.Render(this);
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            // drag frame
            if (e.Button == MouseButtons.Left)
                _mouseToFrameVec = new Point(Location.X - CustomCursor.Instance.Location.X, Location.Y - CustomCursor.Instance.Location.Y);
        }
        
        public override void OnMouseUp(MouseEventArgs e)
        {
            // drop frame
            if (e.Button == MouseButtons.Left)
                _mouseToFrameVec = default;
        }
        
        public override void OnMouseMoved(MouseEventArgs e)
        {
            if (_mouseToFrameVec != default)
                Location = new Rectangle(new Point(CustomCursor.Instance.Location.X + _mouseToFrameVec.X, CustomCursor.Instance.Location.Y + _mouseToFrameVec.Y), Location.Size);
        }

        public override void Dispose()
        {
            _playerNameTextFormat.Dispose();
            _playerNameTextLayout.Dispose();
        }
    }
}
