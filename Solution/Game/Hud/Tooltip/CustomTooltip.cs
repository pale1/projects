using System;
using Game.Rendering;

namespace Game.Hud.Tooltip
{
    /**
     * Tooltip class that wraps a custom tooltip look and the type of content portrayed in the tooltip
     */
    public class CustomTooltip : IDisposable, IRenderable
    {
        private readonly TooltipStyle _tooltipStyle;
        private readonly TooltipContent _tooltipContent;

        public static readonly int FONT_BORDER = (int)(5 * Const.HUD_SCALING_FACTOR);

        public CustomTooltip(TooltipStyle tooltipStyle, TooltipContent tooltipContent)
        {
            _tooltipStyle = tooltipStyle;
            _tooltipContent = tooltipContent;
            _tooltipStyle.ContentSize = _tooltipContent.ContentSize;
        }
        
        public void Render(IRenderer r)
        {
            _tooltipStyle.Render(r); // render the tooltip box, with all the decorations
            _tooltipContent.Render(r); // render the text and other content inside tooltip
        }

        public void Dispose()
        {
            _tooltipStyle.Dispose();
            _tooltipContent.Dispose();
        }
    }
}