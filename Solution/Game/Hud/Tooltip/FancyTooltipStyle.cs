using System.Drawing;
using Game.Rendering;
using Game.Texture;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Hud.Tooltip
{
    public class FancyTooltipStyle : TooltipStyle
    {
        private SpriteSheet _tooltipSpriteSheet;
        private int[] _spriteLocations;

        private static readonly Size TOOLTIP_SPRITE_SIZE = new Size(CustomTooltip.FONT_BORDER * 2, CustomTooltip.FONT_BORDER * 2);
        
        /// <summary>
        /// Creates new tooltip style from a sprite sheet that can have animations.
        /// used sprite sheet must contain 10 sprites (not including animation frames), one for each tooltip edge/corner + 2 for ornament
        /// </summary>
        /// <param name="tooltipStyleSpriteSheet">sprite sheet used for tooltip</param>
        /// <param name="background">tooltip background color</param>
        /// <param name="spriteLocations">array containing id of sprite used for each of the different tooltip parts. Enables reusing of sprites for different parts of tooltip</param>
        public FancyTooltipStyle(SpriteSheet tooltipStyleSpriteSheet, Color background, int[] spriteLocations = null) : base(background)
        {
            _tooltipSpriteSheet = tooltipStyleSpriteSheet;
            _spriteLocations = spriteLocations ?? new []{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        }
        
        public override void Render(IRenderer r)
        {
            Point location = new Point(CustomCursor.Instance.Location.X, CustomCursor.Instance.Location.Y);
            if (location.X + BORDERED_CONTENT_SIZE.Width + CustomTooltip.FONT_BORDER > Const.SCREEN_WIDTH)
                location.X = Const.SCREEN_WIDTH - BORDERED_CONTENT_SIZE.Width - CustomTooltip.FONT_BORDER;
            location.Y -= (int)(BORDERED_CONTENT_SIZE.Height + CURSOR_HALFSIZE); // move tooltip away from cursor center
            
            
            // draw background
            Rectangle area = new Rectangle(location, BORDERED_CONTENT_SIZE);

            Point upLeftCorner = new Point(area.X - CustomTooltip.FONT_BORDER, area.Y - CustomTooltip.FONT_BORDER);
            Point upRightCorner = new Point(area.X + area.Width - CustomTooltip.FONT_BORDER, area.Y - CustomTooltip.FONT_BORDER);
            Point downLeftCorner = new Point(area.X - CustomTooltip.FONT_BORDER, area.Y + area.Height - CustomTooltip.FONT_BORDER);
            Point downRightCorner = new Point(area.X + area.Width - CustomTooltip.FONT_BORDER, area.Y + area.Height - CustomTooltip.FONT_BORDER);
            
            Bitmap bmp = _tooltipSpriteSheet.GetSpriteSheet();
            
            // draw background
            r.FillRectangle(area, Background);
            
            // draw edges
            DrawHorizontalEdge(r, bmp, GetUpEdge(), ref upLeftCorner, ref upRightCorner);
            DrawHorizontalEdge(r, bmp, GetDownEdge(), ref downLeftCorner, ref downRightCorner);
            DrawVerticalEdge(r, bmp, GetLeftEdge(), ref upLeftCorner, ref downLeftCorner);
            DrawVerticalEdge(r, bmp, GetRightEdge(), ref upRightCorner, ref downRightCorner);

            // draw corners
            r.DrawBitmap(bmp, new Rectangle(upLeftCorner, TOOLTIP_SPRITE_SIZE), GetUpLeftCorner());
            r.DrawBitmap(bmp, new Rectangle(upRightCorner, TOOLTIP_SPRITE_SIZE), GetUpRightCorner());
            r.DrawBitmap(bmp, new Rectangle(downLeftCorner, TOOLTIP_SPRITE_SIZE), GetDownLeftCorner());
            r.DrawBitmap(bmp, new Rectangle(downRightCorner, TOOLTIP_SPRITE_SIZE), GetDownRightCorner());
            
            // draw ornaments
            r.DrawBitmap(bmp, new Rectangle(
                area.X + area.Width / 2 - TOOLTIP_SPRITE_SIZE.Width, 
                downLeftCorner.Y, 
                TOOLTIP_SPRITE_SIZE.Width, TOOLTIP_SPRITE_SIZE.Height), GetLeftOrnament());
            r.DrawBitmap(bmp, new Rectangle(
                area.X + area.Width / 2, 
                downLeftCorner.Y, 
                TOOLTIP_SPRITE_SIZE.Width, TOOLTIP_SPRITE_SIZE.Height), GetRightOrnament());
        }

        private void DrawHorizontalEdge(IRenderer r, Bitmap bmp, Rectangle spriteBounds, ref Point fromCorner, ref Point toCorner)
        {
            int x;
            for (x = fromCorner.X + TOOLTIP_SPRITE_SIZE.Width; x < toCorner.X - TOOLTIP_SPRITE_SIZE.Width; x += TOOLTIP_SPRITE_SIZE.Width)
                r.DrawBitmap(bmp, new Rectangle(new Point(x, fromCorner.Y), TOOLTIP_SPRITE_SIZE), spriteBounds);
            int remainderWidth = toCorner.X - x;
            
            // draw cropped part of edge sprite to fill gap between last edge sprite and right corner sprite
            if (remainderWidth > 0)
                r.DrawBitmap(bmp, 
                    new Rectangle(x, fromCorner.Y, remainderWidth, TOOLTIP_SPRITE_SIZE.Height),
                    new Rectangle(spriteBounds.X, spriteBounds.Y, remainderWidth, spriteBounds.Height));
        }
        
        private void DrawVerticalEdge(IRenderer r, Bitmap bmp, Rectangle spriteBounds, ref Point fromCorner, ref Point toCorner)
        {
            int y;
            for (y = fromCorner.Y + TOOLTIP_SPRITE_SIZE.Height; y < toCorner.Y - TOOLTIP_SPRITE_SIZE.Height; y += TOOLTIP_SPRITE_SIZE.Height)
                r.DrawBitmap(bmp, new Rectangle(new Point(fromCorner.X, y), TOOLTIP_SPRITE_SIZE), spriteBounds);
            int remainderHeight = toCorner.Y - y;
            
            // draw cropped part of edge sprite to fill gap between last edge sprite and right corner sprite
            if (remainderHeight > 0)
                r.DrawBitmap(bmp, 
                    new Rectangle(fromCorner.X, y, TOOLTIP_SPRITE_SIZE.Width, remainderHeight),
                    new Rectangle(spriteBounds.X, spriteBounds.Y, spriteBounds.Width, remainderHeight));
        }
        
        private Rectangle GetUpLeftCorner()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[0]);
        }
        private Rectangle GetUpRightCorner()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[1]);
        }
        private Rectangle GetDownLeftCorner()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[2]);
        }
        private Rectangle GetDownRightCorner()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[3]);
        }
        
        private Rectangle GetLeftEdge()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[4]);
        }
        private Rectangle GetRightEdge()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[5]);
        }
        private Rectangle GetUpEdge()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[6]);
        }
        private Rectangle GetDownEdge()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[7]);
        }
        private Rectangle GetLeftOrnament()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[8]);
        }
        private Rectangle GetRightOrnament()
        {
            return _tooltipSpriteSheet.GetSpriteBounds(_spriteLocations[9]);
        }

        public override void Dispose() { } // SpriteSheets takes care of disposing sprite sheets
    }
}