using System.Drawing;
using Game.Model.Item2;
using Game.Rendering;
using Game.Text;
using SlimDX.DirectWrite;

namespace Game.Hud.Tooltip
{
    public class ItemTooltipContent : TooltipContent
    {
        private readonly PItem _item;
        private readonly TextFormat _textFormat; // contains font info for rendering item info
        private readonly TextLayout _textLayout; // contains textual item info ready to be rendered
        
        public ItemTooltipContent(PItem item)
        {
            _item = item;
            _textFormat = FontBank.CreateTextFormat(FontBank.TooltipFont, fontSize: 10F * Const.HUD_SCALING_FACTOR);
            _textLayout = FontBank.CreateTextLayout(item.Name, _textFormat);
            ContentSize = new Size((int)_textLayout.Metrics.Width, (int)_textLayout.Metrics.Height);
        }

        public override void Render(IRenderer r)
        {
            Point location = new Point(CustomCursor.Instance.Location.X, CustomCursor.Instance.Location.Y);
            location.X += CustomTooltip.FONT_BORDER;
            if (location.X + CustomTooltip.FONT_BORDER + ContentSize.Width > Const.SCREEN_WIDTH)
                location.X = Const.SCREEN_WIDTH - CustomTooltip.FONT_BORDER - ContentSize.Width;
            location.Y -= CustomTooltip.FONT_BORDER + ContentSize.Height + (int)(Const.S_CURSOR*Const.CURSOR_SCALING_FACTOR/2);

            r.DrawTextLayout(_textLayout, location, _item.GetColorForQuality());
        }

        public override void Dispose()
        {
            _textFormat.Dispose();
            _textLayout.Dispose();
        }
    }
}