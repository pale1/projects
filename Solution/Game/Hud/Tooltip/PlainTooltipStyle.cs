using System.Drawing;
using Game.Rendering;

namespace Game.Hud.Tooltip
{
    public class PlainTooltipStyle : TooltipStyle
    {
        private readonly Color Edge;
        private readonly int EdgeThickness;
        
        public PlainTooltipStyle(Color edge, Color background, int edgeThickness) : base(background)
        {
            Edge = edge;
            EdgeThickness = edgeThickness;
        }
        
        public override void Render(IRenderer r)
        {
            
            Point location = new Point(CustomCursor.Instance.Location.X, CustomCursor.Instance.Location.Y);
            if (location.X + BORDERED_CONTENT_SIZE.Width > Const.SCREEN_WIDTH)
                location.X = Const.SCREEN_WIDTH - BORDERED_CONTENT_SIZE.Width;
            location.Y -= (int)(BORDERED_CONTENT_SIZE.Height + CURSOR_HALFSIZE); // move tooltip away from cursor center
            
            Rectangle area = new Rectangle(location, BORDERED_CONTENT_SIZE);
            r.FillRectangle(area, Background);
            r.DrawRectangle(area, Edge, EdgeThickness);
        }

        public override void Dispose() { }
    }
}