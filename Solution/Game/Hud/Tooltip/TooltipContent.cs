using System;
using System.Drawing;
using Game.Rendering;

namespace Game.Hud.Tooltip
{
    public abstract class TooltipContent : IDisposable, IRenderable
    {
        public Size ContentSize { get; protected set; }

        public abstract void Render(IRenderer r);
        public abstract void Dispose();
    }
}