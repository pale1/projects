using System;
using System.Drawing;
using Game.Rendering;
using Game.Texture;

// ReSharper disable InconsistentNaming
namespace Game.Hud.Tooltip
{
    public abstract class TooltipStyle : IDisposable, IRenderable
    {
        public static readonly TooltipStyle Classic = new PlainTooltipStyle(Color.Black, Color.FromArgb(200, 70, 70, 70), 2);

        public static readonly TooltipStyle FancyTooltip = new FancyTooltipStyle(SpriteSheets.FancyTooltipSheet, Color.Brown, new []{0,0,0,0,1,2,3,4,5,6});

        protected readonly Color Background;
        
        protected Size BORDERED_CONTENT_SIZE;
        protected float CURSOR_HALFSIZE = Const.S_CURSOR * Const.CURSOR_SCALING_FACTOR / 2;

        private Size _contentSize;
        public Size ContentSize
        {
            get => _contentSize;
            set
            {
                _contentSize = value;
                BORDERED_CONTENT_SIZE = new Size(ContentSize.Width + 2 * CustomTooltip.FONT_BORDER, ContentSize.Height + 2 * CustomTooltip.FONT_BORDER);
            }
        }

        protected TooltipStyle(Color background)
        {
            Background = background;
        }

        public abstract void Render(IRenderer r);
        public abstract void Dispose();
    }
}