﻿using System.Drawing;
using System.Windows.Forms;

namespace Game.IO
{
    public interface IMouseListener
    {
        void OnMouseEnter();
        void OnMouseLeave();

        void OnMouseMoved(MouseEventArgs e);
        void OnMouseHover(Point mouse);

        void OnMouseDown(MouseEventArgs e);
        void OnMouseUp(MouseEventArgs e);
        void OnMouseWheel(MouseEventArgs e);
    }
}
