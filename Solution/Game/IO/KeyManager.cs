﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

namespace Game.IO
{
    public abstract class KeyManager
    {
        private static readonly Dictionary<string, Keys> _keyBinds = new Dictionary<string, Keys>
        {
            { "fast_forward", Keys.Space },         // key to boost fps to 300
            { "max_fps", Keys.F11 },                // key to boost fps to MAX
            { "full_screen", Keys.F12 },            // key to switch between windowed and fullscreen
            { "use_item", Keys.F },                 // key with which item can be used while hovering over it
            { "open_inventory", Keys.I },           // key to open inventory
            { "open_map", Keys.M }                  // key to open map
        };
        
        // TODO primitive approach, dict entry is made for each different key pressed (no restrictions, can overflow dict capacity of 30)
        private static Dictionary<Keys, bool> _pastKeys = new Dictionary<Keys, bool>(30);
        private static Dictionary<Keys, bool> _keys = new Dictionary<Keys, bool>(30);
        
        public delegate void KeyEventHandler();
        public event KeyEventHandler KeyEvent = delegate {}; // add empty delegate!
        
        public static readonly Dictionary<Keys, KeyEventHandler> KeyPressedEh = new Dictionary<Keys, KeyEventHandler>(30);
        public static readonly Dictionary<Keys, KeyEventHandler> KeyReleasedEh = new Dictionary<Keys, KeyEventHandler>(30);

        private static readonly Dictionary<Keys, HashSet<KeyEventHandler>> KeyPressedHandlers = new Dictionary<Keys, HashSet<KeyEventHandler>>(30);
        private static readonly Dictionary<Keys, HashSet<KeyEventHandler>> KeyReleasedHandlers = new Dictionary<Keys, HashSet<KeyEventHandler>>(30);

        public static void AddKeyPressedHandler(Keys k, KeyEventHandler keyPressedEventHandler)
        {
            if (!KeyPressedHandlers.ContainsKey(k))
            {
                HashSet<KeyEventHandler> handlers = new HashSet<KeyEventHandler>();
                handlers.Add(keyPressedEventHandler);

                KeyPressedHandlers[k] = handlers;
            }
            else
            {
                KeyPressedHandlers[k].Add(keyPressedEventHandler);
            }
        }
        
        public static void RemoveKeyPressedHandler(Keys k, KeyEventHandler keyPressedEventHandler)
        {
            if (KeyPressedHandlers.ContainsKey(k))
            {
                KeyPressedHandlers[k].Remove(keyPressedEventHandler);
            }
        }
        
        public static void AddKeyReleasedHandler(Keys k, KeyEventHandler keyReleasedEventHandler)
        {
            if (!KeyReleasedHandlers.ContainsKey(k))
            {
                HashSet<KeyEventHandler> handlers = new HashSet<KeyEventHandler>();
                handlers.Add(keyReleasedEventHandler);

                KeyReleasedHandlers[k] = handlers;
            }
            else
            {
                KeyReleasedHandlers[k].Add(keyReleasedEventHandler);
            }
        }
        
        public static void RemoveKeyReleasedHandler(Keys k, KeyEventHandler keyReleasedEventHandler)
        {
            if (KeyReleasedHandlers.ContainsKey(k))
            {
                KeyReleasedHandlers[k].Remove(keyReleasedEventHandler);
            }
        }

        public static void AddKeyPressedEventHandler(Keys k, KeyEventHandler h)
        {
            if (KeyPressedEh.ContainsKey(k))
            {
                KeyPressedEh[k] += h;
            }
            else
            {
                KeyPressedEh[k] = h;
            }
        }
        
        public static void RemoveKeyPressedEventHandler(Keys k, KeyEventHandler h)
        {
            if (KeyPressedEh.ContainsKey(k))
            {
                KeyPressedEh[k] -= h;
            }
        }
        
        public static void AddKeyReleasedEventHandler(Keys k, KeyEventHandler h)
        {
            if (KeyReleasedEh.ContainsKey(k))
            {
                KeyReleasedEh[k] += h;
            }
            else
            {
                KeyReleasedEh[k] = h;
            }
        }
        
        public static void RemoveKeyReleasedEventHandler(Keys k, KeyEventHandler h)
        {
            if (KeyReleasedEh.ContainsKey(k))
            {
                KeyReleasedEh[k] -= h;
            }
        }

        public static void Update()
        {
            _pastKeys = _keys;
            try
            {
                _keys = new Dictionary<Keys, bool>(_keys);
            }
            catch (Exception e)
            {
                // TODO temp fix, concurrent modification exception on key event while the dictionary is being cloned
                _keys = new Dictionary<Keys, bool>(_keys);
            }
        }

        public static void SetKeyDown(Keys k)
        {
            if (_keys.TryGetValue(k, out var v) && v) return; // prevent event spam (occurs only in key down event)

            _keys[k] = true;
            // if (KeyPressedEh.ContainsKey(k))
            // {
            //     KeyPressedEh[k]?.Invoke();
            // }
            if (KeyPressedHandlers.ContainsKey(k))
            {
                foreach (var keyListener in KeyPressedHandlers[k])
                {
                    keyListener();
                }
            }
        }

        public static void SetKeyUp(Keys k)
        {
            _keys[k] = false;
            // if (KeyReleasedEh.ContainsKey(k))
            // {
            //     KeyReleasedEh[k]?.Invoke();
            // }
            if (KeyReleasedHandlers.ContainsKey(k))
            {
                foreach (var keyListener in KeyReleasedHandlers[k])
                {
                    keyListener();
                }
            }
        }

        public static bool IsPressed(Keys k)
        {
            _pastKeys.TryGetValue(k, out var pb);
            _keys.TryGetValue(k, out var b);
            return !pb && b;
        }
        
        public static bool IsReleased(Keys k)
        {
            _pastKeys.TryGetValue(k, out var pb);
            _keys.TryGetValue(k, out var b);
            return pb && !b;
        }

        public static bool IsDown(Keys k)
        {
            _keys.TryGetValue(k, out var b);
            return b;
        }
        
        /**
         * Returns the key assicated with keyName action
         */
        public static Keys KeyFor(string keyName)
        {
            return _keyBinds[keyName];
        }

        public static bool ChangeKey(string keyName, Keys newKey)
        {
            if (KeyPressedHandlers.ContainsKey(newKey) || KeyReleasedHandlers.ContainsKey(newKey))
                return false; // key in use, re-bind failed
            
            Keys oldKey = KeyFor(keyName);
            KeyPressedHandlers.Add(newKey, KeyPressedHandlers[oldKey]);
            KeyPressedHandlers.Remove(oldKey);
            
            KeyReleasedHandlers.Add(newKey, KeyReleasedHandlers[oldKey]);
            KeyReleasedHandlers.Remove(oldKey);
            
            _keyBinds[keyName] = newKey;
            
            return true;
        }
    }
}
