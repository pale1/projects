﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Game.IO
{
    public abstract class Loader<TK, TV> : IDisposable
    {
        private readonly Dictionary<TK, TV> _cache = new Dictionary<TK, TV>();

        /**
         * Tries to get resource of type T from cache if present, otherwise creates it using appropriate GetResource function, stores it in cache and returns it
         */
        public TV GetCachedResource(TK key)
        {
            _cache.TryGetValue(key, out TV v);
            return v != null ? v : (_cache[key] = GetResource(key));
        }

        /**
         * Must be implemented by subclasses, defines how to get resource of type TV from parameter of type TK
         */
        public abstract TV GetResource(TK key);

        public void Dispose()
        {
            if (typeof(IDisposable).IsAssignableFrom(typeof(TV)))
            {
                foreach (var value in _cache.Select(pair => (IDisposable)pair.Value))
                {
                    value.Dispose();
                }
            }
        }
    }
}
