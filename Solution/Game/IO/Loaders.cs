﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using Game.Model;
using Game.XML;
using SlimDX;
using SlimDX.Direct2D;
using SlimDX.DirectSound;
using SlimDX.Multimedia;
using BufferFlags = SlimDX.DirectSound.BufferFlags;
using LockFlags = SlimDX.DirectSound.LockFlags;

// ReSharper disable InconsistentNaming
namespace Game.IO
{
    public abstract class Loaders
    {
        public static readonly Loader<string, Image> TEXTURE = new TextureLoader();
        public static readonly Loader<string, SlimDX.Direct2D.Bitmap> TEXTURE_DX = new TextureLoaderDX();

        public static readonly Loader<string, Chunk> CHUNK = new ChunkLoader();
        public static readonly Loader<string, Map> MAP = new MapLoader();
        
        public static readonly Loader<Color, SolidColorBrush> BRUSH = new BrushLoader();
        
        public static readonly Loader<(string, int), SoundBuffer> SOUND_DIRECTSOUND = new DirectSoundLoader();


        private class TextureLoader : Loader<string, Image>
        {
            public override Image GetResource(string key)
            {
                return Image.FromFile($"{Const.TEXTURES_PATH}{key}.png");
            }
        }

        private class TextureLoaderDX : Loader<string, SlimDX.Direct2D.Bitmap>
        {
            public override SlimDX.Direct2D.Bitmap GetResource(string key)
            {
                // Load the bitmap using the System.Drawing.Bitmap class.
                System.Drawing.Bitmap originalImage = new System.Drawing.Bitmap($"{Const.TEXTURES_PATH}{key}.png");
                // Create a rectangle holding the size of the bitmap image.
                Rectangle bounds = new Rectangle(0, 0, originalImage.Width, originalImage.Height);
            
                // Lock the memory holding this bitmap so that only we are allowed to mess with it.
                BitmapData imageData = originalImage.LockBits(
                    bounds,
                    ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            
                // Create a DataStream attached to the bitmap.
                DataStream dataStream = new DataStream(imageData.Scan0, imageData.Stride * imageData.Height, true, false);
            
                // Set the pixel format and properties.
                SlimDX.Direct2D.PixelFormat pFormat = new SlimDX.Direct2D.PixelFormat(SlimDX.DXGI.Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied);
                BitmapProperties bmpProperties = new BitmapProperties()
                {
                    PixelFormat = pFormat,
                    //HorizontalDpi = 96f,
                    //VerticalDpi = 96f
                };
                // SlimDX.Direct3D10.Sprite;
            
                // Copy the image data into a new SlimDX.Direct2D.Bitmap object.
                SlimDX.Direct2D.Bitmap d2dBitmap = new SlimDX.Direct2D.Bitmap(
                    Frame.Renderer.GetRenderTarget(),
                    new Size(bounds.Width, bounds.Height),
                    dataStream,
                    imageData.Stride,
                    bmpProperties);
            
                // Unlock the memory that is holding the original bitmap object.
                originalImage.UnlockBits(imageData);
                // Get rid of the original bitmap object since we no longer need it.
                originalImage.Dispose();
                // Return the Direct2D bitmap.
                return d2dBitmap;
            }
            
            // public override Bitmap GetResource(string key)
            // {
            //     System.Drawing.Bitmap bmp = new System.Drawing.Bitmap($"{Const.TEXTURES_PATH}{key}.png");
            //     BitmapProperties bp = new BitmapProperties{PixelFormat = new PixelFormat(SlimDX.DXGI.Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied)};
            //     int stride = bmp.Width * sizeof(int);
            //
            //     using (DataStream tempStream = new DataStream(bmp.Height * stride, false, true))
            //     {
            //         for (int y = 0; y < bmp.Height; y++)
            //         {
            //             for (int x = 0; x < bmp.Width; x++)
            //             {
            //                 Color c = bmp.GetPixel(x, y);
            //                 int bgra = c.B | (c.G << 8) | (c.R << 16) | (c.A << 24);
            //                 tempStream.Write(bgra);
            //             }
            //         }
            //         return new Bitmap(Frame.Renderer.GetRenderTarget(), new Size(bmp.Width, bmp.Height), tempStream, stride, bp);
            //     }
            // }
        }

        private class ChunkLoader : Loader<string, Chunk>
        {
            public override Chunk GetResource(string key)
            {
                string[] lines = File.ReadAllLines($"{Const.MAPS_PATH}{key}");
                Tile[,] tiles = new Tile[Const.S_CHUNK, Const.S_CHUNK];

                int i = 0;
                foreach (string line in lines)
                {
                    int j = 0;
                    foreach (string s in line.Split(','))
                    {
                        int id = Convert.ToInt32(s);
                        tiles[i, j] = TileFactory.GetTile(id) ?? Tile.Black;
                        j++;
                    }
                    i++;
                }

                return new Chunk(tiles);
            }
        }

        private class MapLoader : Loader<string, Map>
        {
            public override Map GetResource(string key)
            {
                string[] lines = File.ReadAllLines($"{Const.MAPS_PATH}{key}/map_info.txt");
                int id = Convert.ToInt32(key.Substring(3));
                string mapName = lines[0];
                string[] line = lines[1].Split(' ');
                Chunk[,] chunks = new Chunk[int.Parse(line[0]), int.Parse(line[1])];
                
                // return map with empty chunk array. responsibility of loading chunks is on the GameEnvironment
                return new Map(id, mapName, chunks);
            }
        }

        private class BrushLoader : Loader<Color, SolidColorBrush>
        {
            public override SolidColorBrush GetResource(Color color)
            {
                return new SolidColorBrush(Frame.Renderer.GetRenderTarget(), color);
            }
        }

        private class DirectSoundLoader : Loader<(string, int), SoundBuffer>
        {
            public override SoundBuffer GetResource((string, int) key)
            {
                (string fileName, int threadId) = key;
                using (WaveStream wavFile = new WaveStream(Const.SOUNDS_PATH + $"{fileName}.wav"))
                {
                    // Create our SoundBufferDescription and fill it in with properties information for our SecondarySoundBuffer
                    var dSoundBufferDesc = new SoundBufferDescription
                    {
                        SizeInBytes = (int) wavFile.Length,
                        Flags = BufferFlags.ControlVolume, // This flag allows us to change the volume of the sound.  There are other flags and you can use several together
                        Format = wavFile.Format
                    };

                    // Create the SecondarySoundBuffer
                    var secondarySoundBuffer = new SecondarySoundBuffer(SoundDirectSound.Devices[threadId], dSoundBufferDesc);
                
                    // Now load the sound
                    byte[] wavData = new byte[dSoundBufferDesc.SizeInBytes];
                    wavFile.Read(wavData, 0, (int)wavFile.Length);
                    secondarySoundBuffer.Write(wavData, 0, LockFlags.None);

                    return secondarySoundBuffer;
                }
            }
        }

        public static void Dispose()
        {
            TEXTURE.Dispose();
            TEXTURE_DX.Dispose();

            CHUNK.Dispose();

            MAP.Dispose();
            
            BRUSH.Dispose();
            
            SOUND_DIRECTSOUND.Dispose();
        }
    }
}
