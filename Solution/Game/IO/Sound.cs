using System;

namespace Game.IO
{
    public abstract class Sound : IDisposable
    {
        // which implementation of sound player is used
        public static Sound Player = new SoundDirectSound();
        
        public abstract void Play(string sound, bool loop = false);
        public abstract void Dispose();
    }
}