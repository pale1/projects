using System.Collections.Concurrent;
using System.Threading;
using SlimDX.DirectSound;

namespace Game.IO
{
    
    // SOURCE: https://resources.oreilly.com/examples/9781782167389
    public class SoundDirectSound : Sound
    {
        private const int NO_THREADS = 5;
        
        public static readonly DirectSound[] Devices = new DirectSound[NO_THREADS];
        //private static PrimarySoundBuffer PrimaryBuffer;

        private readonly Thread[] _threads = new Thread[NO_THREADS];
        
        private readonly BlockingCollection<(string, bool)> _bc = new BlockingCollection<(string, bool)>(15);

        public SoundDirectSound()
        {
            for (int i = 0; i < NO_THREADS; i++)
            {
                // Create our DirectSound object
                Devices[i] = new DirectSound();
                
                // Set the cooperative level
                Devices[i].SetCooperativeLevel(Frame.Instance.Handle, CooperativeLevel.Priority);
            }

            // Create the primary sound buffer
            // SoundBufferDescription desc = new SoundBufferDescription
            // {
            //     Flags = BufferFlags.PrimaryBuffer
            // };
            //PrimaryBuffer = new PrimarySoundBuffer(DirectSound, desc);
            
            for (int i = 0; i < NO_THREADS; i++)
            {
                _threads[i] = new Thread(k =>
                {
                    int j = (int)k;
                    //directSound.SetCooperativeLevel(Frame.Instance.Handle, CooperativeLevel.Priority);
                    while (true)
                    {
                        (string sound, bool loop) = _bc.Take();
                        
                        var sb = Loaders.SOUND_DIRECTSOUND.GetCachedResource((sound, j));
                        sb.Play(0, loop ? PlayFlags.Looping : PlayFlags.None);
                    }
                });
                _threads[i].Start(i);
            }
        }

        // PROBLEM: sound artifacts when using 'GetCachedResource', slower and disposing problems when using 'GetResource'
        public override void Play(string sound, bool loop = false)
        {
            _bc.Add((sound, loop));
        }

        public override void Dispose()
        {
            //DirectSound.Dispose();
            //PrimaryBuffer.Dispose();
            for (int i = 0; i < NO_THREADS; i++)
            {
                _threads[i].Abort();
                Devices[i].Dispose();
            }
        }
    }
}