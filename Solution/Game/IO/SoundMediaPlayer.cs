﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Windows.Media;

namespace Game.IO
{
    public class SoundMediaPlayer : Sound
    {
        private const int NO_THREADS = 5;

        private static Thread[] threads = new Thread[NO_THREADS];

        private static BlockingCollection<string> bc = new BlockingCollection<string>(15);

        public SoundMediaPlayer()
        {
            for (int i = 0; i < NO_THREADS; i++)
            {
                threads[i] = new Thread(() =>
                {
                    MediaPlayer mp = new MediaPlayer();
                    while (true)
                    {
                        string sound = bc.Take();
                        mp.Open(new Uri(Const.SOUNDS_PATH + $"{sound}.wav"));
                        mp.Play();
                    }
                });
                threads[i].Start();
            }
        }
        
        public override void Play(string sound, bool loop = false)
        {
            bc.Add(sound);
        }

        public override void Dispose()
        {
            foreach (Thread t in threads)
            {
                t.Abort();
            }
        }
    }
}
