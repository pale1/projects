using SlimDX.Multimedia;
using SlimDX.XAudio2;


namespace Game.IO
{
    
    // SOURCE: https://resources.oreilly.com/examples/9781782167389
    public class SoundXAudio2 : Sound
    {
        private readonly XAudio2 _xAudio2;
        private readonly MasteringVoice _masteringVoice;
        
        public SoundXAudio2()
        {
            // Create the XAudio2 object
            _xAudio2 = new XAudio2();

            // Check that we have a valid sound device to use
            if (_xAudio2.DeviceCount == 0)
                return;

            // Create our mastering voice object.  This object represents the sound output device
            _masteringVoice = new MasteringVoice(_xAudio2);
        }

        // PROBLEM: high pitched sound artifacts when using .wav files converted from .ogg
        public override void Play(string sound, bool loop = false)
        {
            using (var wavFile = new WaveStream(Const.SOUNDS_PATH + $"{sound}.wav"))
            {
                // Create the audio buffer and store the audio data from the file in it
                var audioBuffer = new AudioBuffer();
                audioBuffer.AudioData = wavFile;
                audioBuffer.AudioBytes = (int)wavFile.Length;
                audioBuffer.LoopCount = loop ? XAudio2.LoopInfinite : 0;
            
                // Create the source voice object. This is used to submit our audio data to the mastering voice object so we can play it
                var sourceVoice = new SourceVoice(_xAudio2, wavFile.Format);
            
                // Submit the audio data in our audio buffer and then start playing it
                sourceVoice.SubmitSourceBuffer(audioBuffer);
                sourceVoice.Start();
            }

            // // ==================================================================================================================
            // // XAUDIO2 SOUND PANNING SAMPLE CODE
            // // ==================================================================================================================
            //
            // // We create this array with 8 elements so it can support sound configurations up to 7.1 surround sound.
            // float[] outputVolumes = new float[8];
            //
            //
            // float left = 1.0f;
            // float right = 0.0f;
            //
            // // Get the channel mask.
            // Speakers channelMask = _xAudio2.GetDeviceDetails(0).OutputFormat.ChannelMask;
            //
            // // For mono sound, we can't do panning since we only have one output channel,
            // // so we just give our single output channel a value of 1.0 so that our sound will
            // // play fully.
            //
            // if (channelMask.HasFlag(Speakers.Mono))
            // {
            //     outputVolumes[0] = 1.0f;
            //     System.Diagnostics.Debug.WriteLine("SET MONO OUTPUT MATRIX!");
            // }
            //
            // // For stereo, 2.1, and surround, we use the same setup since they all have two
            // // output channels.
            // if (channelMask.HasFlag(Speakers.Stereo) ||
            //          channelMask.HasFlag(Speakers.TwoPointOne) ||
            //          channelMask.HasFlag(Speakers.Surround))
            // {
            //     outputVolumes[0] = left;
            //     outputVolumes[1] = 0.0f;
            //     outputVolumes[2] = 0.0f;
            //     outputVolumes[3] = right;
            // }
            //
            // sourceVoice.SetOutputMatrix(null,
            //     sourceVoice.VoiceDetails.InputChannels,
            //                               _masteringVoice.VoiceDetails.InputChannels,
            //                               outputVolumes);
            //
            // // ==================================================================================================================
            // // Submit the audio data in our audio buffer and then start playing it.
            //
            // sourceVoice.SubmitSourceBuffer(audioBuffer);
            // sourceVoice.Start();
        }

        public override void Dispose()
        {
            _xAudio2.Dispose();
        }
    }
}