﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Game.Model
{
    public class Chunk
    {
        public Tile[,] Tiles { get; }

        public Chunk(Tile[,] tiles)
        {
            Tiles = tiles;
        }
    }
}