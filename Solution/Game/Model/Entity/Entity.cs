﻿using System.Drawing;

namespace Game.Model.Entity
{
    public abstract class Entity
    {
        private PointF _location;
        public PointF Location
        {
            get => _location;
            set
            {
                _location = value;
                ChunkId = new Point((int)_location.X / Const.S_CHUNK, (int)_location.Y / Const.S_CHUNK);
            }
        }
        public Point ChunkId { get; private set; }
        public string Name { get; }

        protected Entity(PointF location, string name)
        {
            Location = location;
            Name = name;
        }
    }
}
