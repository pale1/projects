﻿using System;
using System.Drawing;
using Game.IO;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Model.Entity
{
    public class Player : Entity
    {
        public Bitmap Icon { get; }

        public readonly float MaxMoveSpeed = 6F / Const.FPS;
        public readonly float Acceleration = 0.12F / Const.FPS;
        public readonly float Deceleration = 0.24F / Const.FPS;
        public PointF MoveSpeed = new PointF(0, 0);
        
        private int Money;

        public int BaseHealth { get; } = 100;
        public int BaseMana { get; } = 100;

        public int Health { get; set; }
        public int Mana { get; set; }

        public int Strength { get; }
        public int Agility { get; }
        public int Stamina { get; }
        public int Intellect { get; }
        public int Spirit { get; }

        public event EventHandler MoneyChanged;

        public Player(PointF location, string name) : base(location, name)
        {
            Icon = Loaders.TEXTURE_DX.GetCachedResource("player_icon");
            Health = BaseHealth / 2;
            Mana = BaseMana / 2;
        }

        public bool AddMoney(int money)
        {
            if (money < 0)
                return false;

            Money += money;
            MoneyChanged?.Invoke(this, EventArgs.Empty);
            return true;
        }

        public bool SubtractMoney(int money)
        {
            if (money < 0 || Money < money)
                return false;

            Money -= money;
            MoneyChanged?.Invoke(this, EventArgs.Empty);
            return true;
        }

        public int GetVelocityState()
        {
            float absX = Math.Abs(MoveSpeed.X);
            float absY = Math.Abs(MoveSpeed.Y);
            if (absX >= absY && absX > 0)
            {
                if (MoveSpeed.X < 0)
                    return 0; // left
                if (MoveSpeed.X > 0)
                    return 1; // right
            }
            if (absY > absX)
            {
                if (MoveSpeed.Y < 0)
                    return 2; // up
                if (MoveSpeed.Y >= 0)
                    return 3; // down
            }
            return 4; // idle
        }

    }
}
