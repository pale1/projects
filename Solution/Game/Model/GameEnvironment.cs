﻿using System;
using System.Drawing;
using System.IO;
using Game.IO;
using Game.Model.Entity;
using Game.Rendering;

namespace Game.Model
{
    public class GameEnvironment : IRenderable
    {
        public Map Map { get; }
        public Player Player { get; }

        private PointF _camera;
        public PointF Camera
        {
            get => _camera;
            set => _camera = ValidateNewCameraLocation(value);
        }

        public GameEnvironment(Map map, Player player) : this(map, player, player.Location) { }

        public GameEnvironment(Map map, Player player, PointF camera)
        {
            Map = map;
            Player = player;
            Camera = camera;

            // load a local chunk neighborhood of max size 3x3
            Point playerChunkId = new Point((int)(Player.Location.X / Const.S_CHUNK), (int)(Player.Location.Y / Const.S_CHUNK));
            ReloadRelevantChunks(playerChunkId);
        }

        // TODO no chunk releasing yet, see if it is even needed
        public void ReloadRelevantChunks(Point playerChunkId)
        {
            for (int y = Math.Max(playerChunkId.Y - 1, 0); y <= Math.Min(playerChunkId.Y + 1, Map.Chunks.GetLength(1) - 1); y++)
            {
                for (int x = Math.Max(playerChunkId.X - 1, 0); x <= Math.Min(playerChunkId.X + 1, Map.Chunks.GetLength(0) - 1); x++)
                {
                    var chunkFolderFile = $"map{Map.Id}/chunk_{y}_{x}.csv";
                    if (File.Exists($"{Const.MAPS_PATH}{chunkFolderFile}") && Map.Chunks[y, x] == null)
                    {
                        Map.Chunks[y, x] = Loaders.CHUNK.GetCachedResource(chunkFolderFile);
                    }
                }
            }
        }

        public void Render(IRenderer r)
        {
            r.Render(this);
        }
        
        /**
         * Check whether camera can see past map border on new location, readjust it if it does
         */
        private PointF ValidateNewCameraLocation(PointF newCamera)
        {
            if (newCamera == _camera) return newCamera;

            float scaledTileSize = Const.S_TILE * Const.ENVIRONMENT_SCALING_FACTOR;
            
            float screenTileHeight = Const.SCREEN_HEIGHT / scaledTileSize;
            float screenTileWidth = Const.SCREEN_WIDTH / scaledTileSize;

            int mapTileWidth = Map.Chunks.GetLength(0) * Const.S_CHUNK;
            int mapTileHeight = Map.Chunks.GetLength(1) * Const.S_CHUNK;
            
            if (newCamera.X > _camera.X)
            {
                if (newCamera.X + screenTileWidth / 2 > mapTileWidth)
                    newCamera.X = mapTileWidth - screenTileWidth / 2;
            }
            else if (newCamera.X < _camera.X)
            {
                if (newCamera.X - screenTileWidth / 2 < 0)
                    newCamera.X = screenTileWidth / 2;
            }

            if (newCamera.Y > _camera.Y)
            {
                if (newCamera.Y + screenTileHeight / 2 > mapTileHeight)
                    newCamera.Y = mapTileHeight - screenTileHeight / 2;
            }
            else if (newCamera.Y < _camera.Y)
            {
                if (newCamera.Y - screenTileHeight / 2 < 0)
                    newCamera.Y = screenTileHeight / 2;
            }

            return newCamera;
        }
    }
}
