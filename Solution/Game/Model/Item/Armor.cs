﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model.Item.Prototype;

namespace Game.Model.Item
{
    public class Armor : IInventoryItem
    {
        private PArmor _data;
        public PAbstractItem Data
        {
            get
            {
                return _data;
            }
        }

        public int Enchant { get; set; }

        public Armor(PArmor i)
        {
            _data = i;
        }

        public void OnRClick(GameEnvironment e)
        {
            // equip on player
        }
    }
}
