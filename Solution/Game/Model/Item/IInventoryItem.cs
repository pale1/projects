﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model.Item.Prototype;

namespace Game.Model.Item
{
    public interface IInventoryItem
    {
        PAbstractItem Data { get; }

        void OnRClick(GameEnvironment e);
    }
}