﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Game.IO;

namespace Game.Model.Item.Prototype
{
    public enum Quality
    {
        Trash, Common, Uncommon, Rare, Epic, Legendary
    }

    public abstract class PAbstractItem : IInventoryItem
    {
        private static readonly Dictionary<Quality, Color> ColorForQuality = new Dictionary<Quality, Color>() {
            { Quality.Trash, Color.Gray },
            { Quality.Common, Color.White },
            { Quality.Uncommon, Color.Green },
            { Quality.Rare, Color.Blue },
            { Quality.Epic, Color.Purple },
            { Quality.Legendary, Color.Orange }
        };

        public int Id { get; }
        public string Name { get; }
        public string Description { get; }
        /// <summary>
        /// 32x32 image object depicting the item
        /// </summary>
        public Image Image { get; }
        public Quality Quality { get; }
        public bool QuestItem { get; }
        public Action Action { get; set; }

        public PAbstractItem Data => throw new NotImplementedException();

        public PAbstractItem(int id, string name, string description, Image image, Quality quality, bool questItem)
        {
            Id = id;
            Name = name;
            Description = description;
            Image = image ?? throw new ArgumentNullException($"Texture is null for item id {id}"); ;
            Quality = quality;
            QuestItem = questItem;
        }

        public abstract PAbstractItem clone();

        public static Quality GetQuality(string s)
        {
            switch (s)
            {
                case "Trash": return Quality.Trash;
                case "Common": return Quality.Common;
                case "Uncommon": return Quality.Uncommon;
                case "Rare": return Quality.Rare;
                case "Epic": return Quality.Epic;
                case "Legendary": return Quality.Legendary;
            }
            throw new ArgumentException("Nonexistent quality: " + s);
        }

        private void ReplaceColor(Bitmap bitmap, Color color)
        {
            for (var y = 0; y < bitmap.Height; y++)
            {
                for (var x = 0; x < bitmap.Width; x++)
                {
                    bitmap.SetPixel(x, y, Color.FromArgb(bitmap.GetPixel(x, y).A, color.R, color.G, color.B));
                }
            }
        }

        public abstract void OnRClick(GameEnvironment e);

        public override string ToString()
        {
            // TODO finish
            return Name;
        }
    }
}
