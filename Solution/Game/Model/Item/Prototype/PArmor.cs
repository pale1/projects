﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model.Item.Prototype
{
    public class PArmor : PGear
    {
        public PArmor(int id, string name, string description, Image image, Quality quality, bool questItem, List<(Stat, int)> stats) : base(id, name, description, image, quality, questItem, stats) { }

        public override PAbstractItem clone()
        {
            return new PArmor(Id, Name, Description, Image, Quality, QuestItem, Stats);
        }

        public override void OnRClick(GameEnvironment e)
        {
            // equip armor
        }
    }
}
