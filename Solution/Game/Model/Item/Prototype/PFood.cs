﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model.Item.Prototype
{
    public class PFood : PStackableItem
    {
        public PFood(int id, string name, string description, Image image, Quality quality, bool questItem, int stackSize, short regen, short duration) : base(id, name, description, image, quality, questItem, stackSize)
        {
        }
    }
}
