﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Model.Item.Prototype
{
    public enum Stat
    {
        STR, AGI, STA, INT, SPI
    }
    public abstract class PGear : PAbstractItem
    {
        public List<(Stat, int)> Stats { get; }
        public PGear(int id, string name, string description, Image image, Quality quality, bool questItem, List<(Stat, int)> stats) : base(id, name, description, image, quality, questItem)
        {
            Stats = stats;
        }
    }
}
