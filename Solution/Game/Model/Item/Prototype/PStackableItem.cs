﻿using System.Drawing;

namespace Game.Model.Item.Prototype
{
    public class PStackableItem : PAbstractItem
    {
        public int StackSize { get; }

        public PStackableItem(int id, string name, string description, Image image, Quality quality, bool questItem, int stackSize) 
            : base(id, name, description, image, quality, questItem)
        {
            StackSize = stackSize;
        }

        public override PAbstractItem clone()
        {
            return new PStackableItem(Id, Name, Description, Image, Quality, QuestItem, StackSize);
        }

        public override void OnRClick(GameEnvironment e)
        {
            // consume
        }
    }
}
