﻿using System;
using Game.Model.Item.Prototype;
using Game.Text;
using SlimDX.DirectWrite;

namespace Game.Model.Item
{
    public class StackableItem : IInventoryItem, IDisposable
    {
        private PStackableItem _data;
        public PAbstractItem Data => _data;

        // reuse the same static TextFormat for all rendering of item counts
        private static readonly TextFormat ItemCountTextFormat = FontBank.CreateTextFormat(FontBank.InventoryFont, fontSize: 10f * Const.HUD_SCALING_FACTOR);
        // for rendering item count in inventory
        public TextLayout ItemCountTextLayout;
        
        private int _count;
        public int Count
        {
            get => _count;
            set
            {
                _count = value;
                ItemCountTextLayout = FontBank.CreateTextLayout(_count.ToString(), ItemCountTextFormat);
            }
        }

        public StackableItem(PStackableItem i, int count)
        {
            _data = i;
            Count = count;
        }

        public void OnRClick(GameEnvironment e)
        {
            // use item
        }

        public void Dispose()
        {
            if (!ItemCountTextFormat.Disposed)
                ItemCountTextFormat.Dispose();
            ItemCountTextLayout.Dispose();
        }
    }
}
