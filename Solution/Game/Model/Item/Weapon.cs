﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Model.Item.Prototype;

namespace Game.Model.Item
{
    public class Weapon : IInventoryItem
    {
        private PWeapon _data;
        public PAbstractItem Data
        {
            get
            {
                return _data;
            }
        }

        public int Enchant { get; set; }

        public Weapon(PWeapon i)
        {
            _data = i;
        }

        public void OnRClick(GameEnvironment e)
        {
            // equip on player
        }
    }
}
