﻿using System.Collections.Generic;
using Game.Hud.Tooltip;

namespace Game.Model.Item2
{
    public class Item
    {
        private readonly Dictionary<ItemType, string> _dragSoundForItemType = new Dictionary<ItemType, string>
        {
            { ItemType.Armor, "drag_gear"},
            { ItemType.Weapon, "drag_gear" },
            { ItemType.Tool, "drag_gear" }, 
            { ItemType.Food, "drag_food" },
            { ItemType.Drink, "drag_water" },
            { ItemType.Valuable, "drag_gem" }, 
            { ItemType.Ingredient, "drag_ingredient" }
        };
        
        private readonly Dictionary<ItemType, string> _dropSoundForItemType = new Dictionary<ItemType, string>
        {
            { ItemType.Armor, "drop_gear"},
            { ItemType.Weapon, "drop_gear" },
            { ItemType.Tool, "drop_gear" }, 
            { ItemType.Food, "drop_food" },
            { ItemType.Drink, "drop_water" },
            { ItemType.Valuable, "drop_gem" }, 
            { ItemType.Ingredient, "drop_ingredient" }
        };
        
        private readonly Dictionary<ItemType, TooltipStyle> _tooltipStyleForItemType = new Dictionary<ItemType, TooltipStyle>
        {
            { ItemType.Armor, TooltipStyle.Classic},
            { ItemType.Weapon, TooltipStyle.Classic },
            { ItemType.Tool, TooltipStyle.Classic }, 
            { ItemType.Food, TooltipStyle.Classic },
            { ItemType.Drink, TooltipStyle.Classic },
            { ItemType.Valuable, TooltipStyle.Classic }, 
            { ItemType.Ingredient, TooltipStyle.FancyTooltip }
        };
        
        public readonly PItem Data;

        public int Amount { get; set; }

        public Item(PItem data, int amount)
        {
            Data = data;
            Amount = amount;
        }

        public Item Clone()
        {
            return new Item(Data, Amount);
        }

        public string GetDragSound()
        {
            return _dragSoundForItemType[Data.ItemType];
        }

        public string GetDropSound()
        {
            return _dropSoundForItemType[Data.ItemType];
        }

        public TooltipStyle GetTooltipStyle()
        {
            return _tooltipStyleForItemType[Data.ItemType];
        }
    }
}