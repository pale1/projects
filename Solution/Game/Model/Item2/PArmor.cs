using System.Drawing;

namespace Game.Model.Item2
{
    public class PArmor : PItem
    {
        public PArmor(int id, Quality quality, string name, string description, Image image, bool questItem) : base(id, ItemType.Armor, quality, name, description, 1, image, questItem)
        {
            
        }

        public new void OnRClick(GameEnvironment e)
        {
            // equip
        }
    }
}