﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;

namespace Game.Model.Item2
{
    public enum Quality
    {
        Trash, Common, Uncommon, Rare, Epic, Legendary
    }
    public enum ItemType
    {
        Armor, Weapon, Tool, Food, Drink, Valuable, Ingredient
    }

    public class PItem
    {
        private static readonly Dictionary<Quality, Color> ColorForQuality = new Dictionary<Quality, Color>() {
            { Quality.Trash, Color.Gray },
            { Quality.Common, Color.White },
            { Quality.Uncommon, Color.FromArgb(255, 0, 255, 0) },
            { Quality.Rare, Color.FromArgb(255, 70, 70, 255) },
            { Quality.Epic, Color.FromArgb(255, 220, 0, 220) },
            { Quality.Legendary, Color.FromArgb(255, 200, 100, 0) }
        };

        public int Id { get; }
        public ItemType ItemType { get; }
        public Quality Quality { get; }
        public string Name { get; }
        public string Description { get; }
        public int StackSize { get; }

        /// <summary>
        /// 32x32 image object depicting the item
        /// </summary>
        public Image Image { get; }
        public bool QuestItem { get; }
        
        public PItem(int id, ItemType itemType, Quality quality, string name, string description, int stackSize, Image image, bool questItem)
        {
            Id = id;
            ItemType = itemType;
            Quality = quality;
            Name = name;
            Description = description;
            StackSize = stackSize;
            Image = image ?? throw new ArgumentNullException($"Texture is null for item id {id}");
            QuestItem = questItem;
        }
        
        public static ItemType GetItemType(string s)
        {
            switch (s)
            {
                case "Armor": return ItemType.Armor;
                case "Weapon": return ItemType.Weapon;
                case "Food": return ItemType.Food;
                case "Drink": return ItemType.Drink;
                case "Valuable": return ItemType.Valuable;
                case "Ingredient": return ItemType.Ingredient;
            }
            throw new ArgumentException("Nonexistent item type: " + s);
        }
        public static Quality GetQuality(string s)
        {
            switch (s)
            {
                case "Trash": return Quality.Trash;
                case "Common": return Quality.Common;
                case "Uncommon": return Quality.Uncommon;
                case "Rare": return Quality.Rare;
                case "Epic": return Quality.Epic;
                case "Legendary": return Quality.Legendary;
            }
            throw new ArgumentException("Nonexistent quality: " + s);
        }

        public Color GetColorForQuality()
        {
            return ColorForQuality[Quality];
        }

        public void OnUse(GameEnvironment e)
        {
            Debug.WriteLine($"Used item {Name}");
        }

        public override string ToString()
        {
            // TODO finish
            return Name;
        }
    }
}
