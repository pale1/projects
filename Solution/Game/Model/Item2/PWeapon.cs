
using System.Drawing;

namespace Game.Model.Item2
{
    public class PWeapon : PItem
    {
        public PWeapon(int id, Quality quality, string name, string description, Image image, bool questItem) : base(id, ItemType.Weapon, quality, name, description, 1, image, questItem)
        {
            
        }

        public new void OnRClick(GameEnvironment e)
        {
            // equip
        }
    }
}