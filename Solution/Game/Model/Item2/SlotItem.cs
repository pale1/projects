using System;
using Game.Hud.Tooltip;
using Game.Text;
using SlimDX.DirectWrite;

namespace Game.Model.Item2
{
    public class SlotItem : IDisposable
    {
        private Item _item;
        
        public int Amount
        {
            get => _item.Amount;
            set
            {
                ItemAmountTextLayout?.Dispose();
                _item.Amount = value;
                if (_item.Amount > 1)
                    ItemAmountTextLayout = FontBank.CreateTextLayout(_item.Amount.ToString(), ItemAmountTextFormat);
            }
        }

        public PItem Data => _item.Data;
        
        // reuse the same static TextFormat for all rendering of item amount (inventory)
        private static readonly TextFormat ItemAmountTextFormat = FontBank.CreateTextFormat(FontBank.InventoryFont, fontSize: 10f * Const.HUD_SCALING_FACTOR);
        
        // for rendering item amount in inventory
        public TextLayout ItemAmountTextLayout;

        public SlotItem(Item item)
        {
            _item = item;
            Amount = _item.Amount; // to trigger the setter
        }

        public SlotItem(PItem item, int amount)
        {
            _item = new Item(item, amount);
            Amount = _item.Amount;
        }

        public string GetDragSound()
        {
            return _item.GetDragSound();
        }

        public string GetDropSound()
        {
            return _item.GetDropSound();
        }
        
        public TooltipStyle GetTooltipStyle()
        {
            return _item.GetTooltipStyle();
        }

        public Item Clone()
        {
            return new Item(_item.Data, _item.Amount);
        }
        
        public void Dispose()
        {
            if (!ItemAmountTextFormat.Disposed)
                ItemAmountTextFormat.Dispose();
            ItemAmountTextLayout?.Dispose();
        }
    }
}