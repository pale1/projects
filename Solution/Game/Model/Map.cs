﻿using System.Collections.Generic;
using System.Drawing;
using Game.IO;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Model
{
    public class Map
    {
        public int Id { get; }
        public string Name { get; }
        public Chunk[,] Chunks { get; }
        
        public Bitmap MapTexture { get; }
        
        /**
         * How many pixels is each tile wide/high in the map texture
         */
        public const int PIXELS_PER_TILE = 4;
        
        /**
         * How many chunks is the map high
         */
        public int Height => Chunks.GetLength(1);

        /**
         * How many chunks is the map wide
         */
        public int Width => Chunks.GetLength(0);

        public Map(int id, string name, Chunk[,] chunks)
        {
            Id = id;
            Name = name;
            Chunks = chunks;
            MapTexture = Loaders.TEXTURE_DX.GetCachedResource($"{Const.MAP_TEXTURES_FOLDER}/map{id}");
            // if (Math.Abs(MapTexture.Size.Width - Width * Const.S_CHUNK * PIXELS_PER_TILE) > 0.01 ||
            //     Math.Abs(MapTexture.Size.Height - Height * Const.S_CHUNK * PIXELS_PER_TILE) > 0.01)
            //     throw new Exception("Map texture size must correspond to number of tiles (1 tile = PIXELS_PER_TILE map pixels)");
        }
        
        // steps can be positive or negative
        // return step can be positive or negative
        
        public float CheckHorizontalCollision(PointF loc, float step)
        {
            foreach (Point p in FindTilesUnderEntity(loc.X + step, loc.Y))
            {
                int chunkX = p.X / Const.S_CHUNK;
                int chunkY = p.Y / Const.S_CHUNK;
                if (Chunks[chunkY, chunkX].Tiles[p.Y % Const.S_CHUNK, p.X % Const.S_CHUNK].Collision)
                    return 0.5F - loc.X % 1;
            }

            return step;
        }
        
        public float CheckVerticalCollision(PointF loc, float step)
        {
            foreach (Point p in FindTilesUnderEntity(loc.X, loc.Y + step))
            {
                int chunkX = p.X / Const.S_CHUNK;
                int chunkY = p.Y / Const.S_CHUNK;
                if (Chunks[chunkY, chunkX].Tiles[p.Y % Const.S_CHUNK, p.X % Const.S_CHUNK].Collision)
                    return 0.5F - loc.Y % 1;
            }
            
            return step;
        }

        /// <summary>
        /// Finds all tiles that the entity is standing on
        /// </summary>
        /// <param name="locX">X Location of entity</param>
        /// <param name="locY">Y Location of entity</param>
        /// <returns>Set of points that represent coordinates of tiles that entity is standing on</returns>
        private HashSet<Point> FindTilesUnderEntity(float locX, float locY)
        {
            HashSet<Point> tilesUnderPlayer = new HashSet<Point>(4);

            tilesUnderPlayer.Add(new Point(
                (int)(locX - 0.5F),
                (int)(locY - 0.5F)));
            tilesUnderPlayer.Add(new Point(
                (int)(locX + 0.5F - .01F),
                (int)(locY - 0.5F)));
            tilesUnderPlayer.Add(new Point(
                (int)(locX + 0.5F - .01F),
                (int)(locY + 0.5F - .01F)));
            tilesUnderPlayer.Add(new Point(
                (int)(locX - 0.5F),
                (int)(locY + 0.5F - .01F)));
            return tilesUnderPlayer;
        }
    }
}
