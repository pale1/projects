﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Game.XML;

namespace Game.Model
{
    public class Tile
    {
        public const int SIZE = 32;
        public static Tile Black = TileFactory.GetTile(0);

        public int Id { get; }
        public string Name { get; }
        /// <summary>
        /// 32x32 image object depicting the item
        /// </summary>
        public Image Texture { get; }
        public bool Collision { get; }

        public Tile(int id, string name, Image texture, bool collision)
        {
            Id = id;
            Name = name;
            Texture = texture ?? throw new ArgumentNullException($"Texture is null for tile id {id}");
            Collision = collision;
        }
    }
}
