using System.Drawing;
using Game.Component;

// ReSharper disable InconsistentNaming
namespace Game
{
    /**
     * Runtime Constants
     */
    public static class RTC
    {
        public static Point SCREEN_CENTER;
        public static float SCALED_SLOT_SIZE;

        /**
         * Default duration of animation frames = 0.2 sec
         */
        public static int DEF_FRAME_TIMING;

        public static void CalculateRuntimeConstants()
        {
            SCREEN_CENTER = new Point(Const.SCREEN_WIDTH / 2, Const.SCREEN_HEIGHT / 2);
            SCALED_SLOT_SIZE = GSlot.SIZE * Const.HUD_SCALING_FACTOR;

            DEF_FRAME_TIMING = (int)(Const.FPS * 0.2);
        }
    }
}