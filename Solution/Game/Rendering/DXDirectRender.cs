﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Component;
using Game.Hud;
using Game.Hud.Tooltip;
using Game.IO;
using Game.Model;
using Game.Text;
using Game.Texture;
using SlimDX;
using SlimDX.Direct2D;
using SlimDX.DirectWrite;
using Bitmap = SlimDX.Direct2D.Bitmap;
using FactoryType = SlimDX.Direct2D.FactoryType;
using Format = SlimDX.DXGI.Format;

// ReSharper disable InconsistentNaming
namespace Game.Rendering
{
    class DXDirectRender : IRenderer
    {
        private readonly SlimDX.Direct2D.Factory _factory = new SlimDX.Direct2D.Factory(FactoryType.Multithreaded, DebugLevel.None);
        private readonly WindowRenderTarget _wrt;
        
        private TextFormat _textFormat;
        private TextLayout _textLayout;

        public DXDirectRender(Form form)
        {
            RenderTargetProperties rtProperties = new RenderTargetProperties()
            {
                PixelFormat = new PixelFormat(Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied)
            };
            WindowRenderTargetProperties wrtProperties = new WindowRenderTargetProperties()
            {
                Handle = form.Handle,
                PixelSize = new Size((int)(Const.SCREEN_WIDTH * Const.DPI_SCALE_X), (int)(Const.SCREEN_HEIGHT * Const.DPI_SCALE_Y)),
                PresentOptions = PresentOptions.Immediately
            };
            _wrt = new WindowRenderTarget(
                _factory,
                rtProperties,
                wrtProperties);
            
            _textFormat = FontBank.CreateTextFormat(FontBank.TextFont, /*fontWeight: FontWeight.DemiBold, fontStyle: FontStyle.Italic, fontStretch: FontStretch.Normal, */fontSize: 20f); 
            _textLayout = FontBank.CreateTextLayout("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz", _textFormat, 1000, 1000);
        }

        public void Begin()
        {
            _wrt.BeginDraw();
        }
        
        public void End()
        {
            _wrt.EndDraw();
        }

        public void Clear(Color c)
        {
            _wrt.Clear(new Color4(c));
        }

        public void Render(GButton button)
        {
            SolidColorBrush b = button.IsMouseOver ? 
                Loaders.BRUSH.GetCachedResource(Color.White) :
                Loaders.BRUSH.GetCachedResource(Color.Gray);
            _wrt.FillRectangle(b, button.Location);
            _wrt.DrawRectangle(Loaders.BRUSH.GetCachedResource(Color.Black), button.Location);
        }

        public void Render(GSlot slot)
        {
            // draw slot background
            _wrt.DrawBitmap(
                slot.IsMouseOver
                    ? Loaders.TEXTURE_DX.GetCachedResource("slot_back_active")
                    : Loaders.TEXTURE_DX.GetCachedResource("slot_back_inactive"),
                slot.Location, 1F, InterpolationMode.NearestNeighbor);
            
            // draw item in slot if present
            if (slot.HasItem())
            {
                lock (CustomCursor.MouseEventLock)
                {
                    var item = slot.Contained;
                    int slotSize = (int) RTC.SCALED_SLOT_SIZE;
                    _wrt.DrawBitmap(Loaders.TEXTURE_DX.GetCachedResource($"{Const.ITEM_TEXTURES_FOLDER}/{slot.Contained.Data.Name}"),
                        new Rectangle(slot.Location.X + 2, slot.Location.Y + 2, slotSize, slotSize), 1F, InterpolationMode.NearestNeighbor);
                
                    if (item.Amount > 1)
                    {
                        //float fontSize = .5f * Const.HUD_SCALING_FACTOR;
                        float edgeSize = 3f * Const.HUD_SCALING_FACTOR;
                        // DrawTextFormat(item.Count.ToString(), _textFormat,
                        //     new Rectangle(
                        //         (int)(slot.Location.X + edgeSize), 
                        //         (int)(slot.Location.Y + slotSize - Fonts.FONT1.Height * fontSize - edgeSize), 
                        //         200, 200), Color.White);
                        float digitHeight = item.ItemAmountTextLayout.Metrics.Height;
                        DrawTextLayout(item.ItemAmountTextLayout, new PointF(slot.Location.X + edgeSize, slot.Location.Y + slotSize - digitHeight - edgeSize), Color.White);
                    }
                }
            }
            
            // draw slot foreground
            _wrt.DrawBitmap(
                slot.IsMouseOver
                    ? Loaders.TEXTURE_DX.GetCachedResource("slot_front_active")
                    : Loaders.TEXTURE_DX.GetCachedResource("slot_front_inactive"),
                slot.Location, 1F, InterpolationMode.NearestNeighbor);
        }

        public void Render(GameEnvironment env)
        {
            // how many screen pixels is every tile wide/high
            var scaledTileSize = Const.S_TILE * Const.ENVIRONMENT_SCALING_FACTOR;

            // how many screen pixels is camera far from top left corner
            var scaledCamera = new PointF(env.Camera.X * scaledTileSize, env.Camera.Y * scaledTileSize);
            
            float screenTileHalfHeight = Const.SCREEN_HEIGHT / scaledTileSize / 2;
            float screenTileHalfWidth = Const.SCREEN_WIDTH / scaledTileSize / 2;
            
            Bitmap tileSheet = SpriteSheets.TileSheet.GetSpriteSheet();
            for (int y = Math.Max(0, (int)(env.Camera.Y - screenTileHalfHeight)); y <= Math.Min(env.Map.Height * Const.S_CHUNK - 1, (int)(env.Camera.Y + screenTileHalfHeight)); y++)
            {
                int chunkCoordY = y / Const.S_CHUNK;
                int tileCoordY = y % Const.S_CHUNK;
                for (int x = Math.Max(0, (int)(env.Camera.X - screenTileHalfWidth)); x <= Math.Min(env.Map.Width * Const.S_CHUNK - 1, (int)(env.Camera.X + screenTileHalfWidth)); x++)
                {
                    int chunkCoordX = x / Const.S_CHUNK;
                    int tileCoordX = x % Const.S_CHUNK;
                    
                    // var texture = Loaders.TEXTURE_DX.GetCachedResource($"{Const.TILE_TEXTURES_FOLDER}/{env.Map.Chunks[chunkCoordY, chunkCoordX].Tiles[tileCoordY, tileCoordX].Name}");
                    // _wrt.DrawBitmap(texture,
                    //     new Rectangle(
                    //         (int)(chunkCoordX * Const.S_CHUNK * scaledTileSize + tileCoordX * scaledTileSize - scaledCamera.X + RTC.SCREEN_CENTER.X), 
                    //         (int)(chunkCoordY * Const.S_CHUNK * scaledTileSize + tileCoordY * scaledTileSize - scaledCamera.Y + RTC.SCREEN_CENTER.Y), 
                    //         (int)scaledTileSize,
                    //         (int)scaledTileSize)
                    //     , 1, InterpolationMode.NearestNeighbor);
                    _wrt.DrawBitmap(tileSheet,
                        new Rectangle(
                            (int)(chunkCoordX * Const.S_CHUNK * scaledTileSize + tileCoordX * scaledTileSize - scaledCamera.X + RTC.SCREEN_CENTER.X), 
                            (int)(chunkCoordY * Const.S_CHUNK * scaledTileSize + tileCoordY * scaledTileSize - scaledCamera.Y + RTC.SCREEN_CENTER.Y), 
                            (int)scaledTileSize,
                            (int)scaledTileSize)
                        , 1, InterpolationMode.NearestNeighbor
                        , SpriteSheets.TileSheet.GetSpriteBounds(env.Map.Chunks[chunkCoordY, chunkCoordX].Tiles[tileCoordY, tileCoordX].Id));
                }
            }
            
            _wrt.DrawBitmap(SpriteSheets.PlayerSheet.GetSpriteSheet(),
                new Rectangle(
                    (int)(env.Player.Location.X * scaledTileSize - scaledCamera.X + RTC.SCREEN_CENTER.X - scaledTileSize / 2f), 
                    (int)(env.Player.Location.Y * scaledTileSize - scaledCamera.Y + RTC.SCREEN_CENTER.Y - scaledTileSize / 2f), 
                    (int)scaledTileSize, 
                    (int)scaledTileSize), 
                1, InterpolationMode.NearestNeighbor, SpriteSheets.PlayerSheet.GetSpriteBounds(env.Player.GetVelocityState()));
             
            // _wrt.DrawBitmap(Loaders.TEXTURE_DX.GetCachedResource("player"),
            //     new Rectangle(
            //         (int)(env.Player.Location.X - env.Camera.X + centerOfScreen.X - scaledTileSize / 2f), 
            //         (int)(env.Player.Location.Y - env.Camera.Y + centerOfScreen.Y - scaledTileSize / 2f), 
            //         (int)scaledTileSize, 
            //         (int)scaledTileSize), 
            //     1, InterpolationMode.NearestNeighbor);

            // for (int i = 0; i < 20; i++)
            //     //DrawText("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz", BitmapFont.CLASSIC_BLACK, new Point(100, 360 + 24*i), 20f);
            //     DrawTextLayout(_textLayout, new Point(100, 360 + 24*i), Color.Black);
            //     //DrawTextFormat("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz", _textFormat, new Rectangle(100, 360 + 24*i, 1000, 10000), Color.Black);
            //Const.Stop();
        }
        
        public void Render(PlayerFrame o)
        {
            
        }
        
        public void Render(CustomCursor customCursor)
        {
            var texture = customCursor.IsDragging() ? 
                Loaders.TEXTURE_DX.GetCachedResource($"cursor_held") :
                Loaders.TEXTURE_DX.GetCachedResource($"cursor");
            int cursorSize = (int)(Const.S_CURSOR * Const.CURSOR_SCALING_FACTOR);
            
            _wrt.DrawBitmap(texture,
                new Rectangle(
                    CustomCursor.Instance.Location.X - cursorSize / 2,
                    CustomCursor.Instance.Location.Y - cursorSize / 2,
                    cursorSize,
                    cursorSize)
                , 1, InterpolationMode.NearestNeighbor);
            if (customCursor.DraggedItem != null)
            {
                int itemIconSize = (int)(RTC.SCALED_SLOT_SIZE * 0.5);
                _wrt.DrawBitmap(Loaders.TEXTURE_DX.GetCachedResource($"{Const.ITEM_TEXTURES_FOLDER}/{customCursor.DraggedItem.Data.Name}"),
                    new Rectangle(customCursor.Location.X, customCursor.Location.Y + cursorSize / 2, 
                        itemIconSize, itemIconSize), 1F, InterpolationMode.NearestNeighbor);

                if (customCursor.DraggedItem.Amount > 1)
                {
                    DrawTextLayout(customCursor.ItemAmountTextLayout, new PointF(
                        customCursor.Location.X, 
                        customCursor.Location.Y + cursorSize / 2 + itemIconSize / 2), Color.White);
                }
            }
        }

        public void Render(CustomTooltip customTooltip)
        {
            _wrt.FillRectangle(Loaders.BRUSH.GetCachedResource(Color.Gray), new Rectangle(
                CustomCursor.Instance.Location.X, 
                CustomCursor.Instance.Location.Y,
                200, 50));
        }

        /// <summary>
        /// <para>
        /// VERY SLOW FUNCTION - USE DrawTextLayout
        /// 670 fps max on benchmark
        /// </para>
        /// This function uses a custom font from a PNG file to draw text by iteratively drawing bitmaps containing characters.
        /// </summary>
        public void DrawText(string text, BitmapFont font, PointF location, float fontSize)
        {
            float x = location.X;
            float y = location.Y;
            fontSize /= font.SpriteHeight;

            var characterWidth = font.SpriteWidth * Const.DPI_SCALE_X_REC * fontSize;
            var characterHeight = font.SpriteHeight * Const.DPI_SCALE_Y_REC * fontSize;

            foreach (var line in text.Split('\n'))
            {
                foreach (var c in line)
                {
                    if (c != ' ')
                        _wrt.DrawBitmap(font.GetSpriteSheet(),
                            new RectangleF(x, y, 
                                characterWidth, 
                                characterHeight),
                            1F,
                            InterpolationMode.NearestNeighbor,
                            font.GetCharacterBounds(c));
                    x += characterWidth;
                }
                y += characterHeight;
            }
        }

        /// <summary>
        /// <para>
        /// SLOW FUNCTION - USE DrawTextLayout (this function probably creates and disposes a TextLayout on every call)
        /// 880 fps max on benchmark
        /// </para>
        /// This function uses a defined font contained in TextFormat.FontFamily to draw text using DirectWrite
        /// </summary>
        public void DrawTextFormat(string text, TextFormat textFormat, Rectangle location, Color color)
        {
            _wrt.DrawText(text, textFormat, location, Loaders.BRUSH.GetCachedResource(color));
        }
        
        /// <summary>
        /// <para>
        /// FAST FUNCTION
        /// 1300 fps max on benchmark
        /// </para>
        /// This function draws a defined TextLayout using DirectWrite.
        /// TextLayout should be cached instead of being created and disposed for every call.
        /// </summary>
        public void DrawTextLayout(TextLayout textLayout, PointF origin, Color color, DrawTextOptions drawTextOptions = DrawTextOptions.None)
        {
            _wrt.DrawTextLayout(origin, textLayout, Loaders.BRUSH.GetCachedResource(color), drawTextOptions);
        }

        public void DrawBitmap(Bitmap bitmap, Rectangle location, float opacity = 1F)
        {
            _wrt.DrawBitmap(bitmap, location, opacity, InterpolationMode.NearestNeighbor);
        }
        
        public void DrawBitmap(Bitmap bitmap, Rectangle location, Rectangle sourceLocation, float opacity = 1F)
        {
            _wrt.DrawBitmap(bitmap, location, opacity, InterpolationMode.NearestNeighbor, sourceLocation);
        }

        public void DrawRectangle(Rectangle rectangle, Color c, float strokeWidth = 1F)
        {
            _wrt.DrawRectangle(Loaders.BRUSH.GetCachedResource(c), rectangle, strokeWidth);
        }

        public void FillRectangle(Rectangle rectangle, Color c)
        {
            _wrt.FillRectangle(Loaders.BRUSH.GetCachedResource(c), rectangle);
        }

        public WindowRenderTarget GetRenderTarget()
        {
            return _wrt;
        }

        public void Dispose()
        {
            _factory.Dispose();
            _wrt.Dispose();
            _textFormat.Dispose();
            _textLayout.Dispose();
        }
    }
}
