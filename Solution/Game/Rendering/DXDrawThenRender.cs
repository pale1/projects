﻿using System;
using System.Drawing;
using Game.Component;
using Game.IO;
using Game.Model;
using Game.Model.Item;
using SlimDX;
using SlimDX.Direct2D;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Rendering

{
    class DXDrawThenRender// : IRenderer
    {
        private WindowRenderTarget wrt;

        private SolidColorBrush whiteBrush;
        private SolidColorBrush grayBrush;
        private SolidColorBrush blackBrush;

        private Bitmap screen;

        public DXDrawThenRender(Frame frame)
        {
            RenderTargetProperties rtProperties = new RenderTargetProperties()
            {
                PixelFormat = new PixelFormat(SlimDX.DXGI.Format.B8G8R8A8_UNorm, AlphaMode.Premultiplied)
            };
            WindowRenderTargetProperties wrtProperties = new WindowRenderTargetProperties()
            {
                Handle = frame.Handle,
                PixelSize = new Size((int)(frame.ClientSize.Width * 1.5), (int)(frame.ClientSize.Height * 1.5)),
                PresentOptions = PresentOptions.Immediately
            };
            wrt = new WindowRenderTarget(
                new Factory(FactoryType.Multithreaded, DebugLevel.None),
                rtProperties,
                wrtProperties);

            whiteBrush = new SolidColorBrush(wrt, new Color4(Color.White));
            grayBrush = new SolidColorBrush(wrt, new Color4(Color.Gray));
            blackBrush = new SolidColorBrush(wrt, new Color4(Color.Black));
            screen = new Bitmap(wrt, frame.ClientSize);
        }

        public void Begin()
        {
            wrt.BeginDraw();
        }

        public void Clear(Color c)
        {
            wrt.Clear(new Color4(c));
        }

        public void Render(GButton o)
        {
            SolidColorBrush b = o.IsMouseOver ? whiteBrush : grayBrush;
            wrt.FillRectangle(b, o.Location);
            wrt.DrawRectangle(blackBrush, o.Location);
            //wrt.DrawText(o.Name, new Font("Arial", 10), new SolidBrush(Color.Black), new Point(o.Location.X, o.Location.Y + o.Location.Height / 2 - 5));
        }

        public void Render(GSlot o)
        {
          
            if (o.IsMouseOver)
                screen.FromBitmap(Loaders.TEXTURE_DX.GetCachedResource("slot_selected"),
                    new Point(o.Location.X, o.Location.Y));
            else
                screen.FromBitmap(Loaders.TEXTURE_DX.GetCachedResource("slot"),
                    new Point(o.Location.X, o.Location.Y));

            if (o.Contained != null)
            {
                screen.FromBitmap(Loaders.TEXTURE_DX.GetCachedResource($"item_textures/{o.Name}"),
                    new Point(o.Location.X + 2, o.Location.Y + 2));

                if (o.Contained.GetType() == typeof(StackableItem))
                {
                    var s = o.Contained;
                    //if (s.Count > 1)
                    //    ig.DrawString(s.Count.ToString(), new Font("Arial", 10), new SolidBrush(Color.White), new PointF(o.Location.X, o.Location.Y + o.Location.Height / 2 - 5));
                }
            }
        }

        public void Render(GameEnvironment o)
        {
            for (int i = 0; i < o.Map.Chunks[0, 0].Tiles.GetLength(1); i++)
            {
                for (int j = 0; j < o.Map.Chunks[0, 0].Tiles.GetLength(0); j++)
                {
                    screen.FromBitmap(Loaders.TEXTURE_DX.GetCachedResource($"tile_textures/{o.Map.Chunks[0, 0].Tiles[i, j].Name}"),
                        new Point((int)(j * Const.S_TILE - o.Camera.X + Const.SCREEN_WIDTH / 2),
                        (int)(i * Const.S_TILE - o.Camera.Y + Const.SCREEN_HEIGHT / 2)));
                }
            }
            screen.FromBitmap(Loaders.TEXTURE_DX.GetCachedResource("player"),
                new Point((int)(o.Player.Location.X - o.Camera.X + Const.SCREEN_WIDTH / 2 - Const.S_TILE / 2),
                (int)(o.Player.Location.Y - o.Camera.Y + Const.SCREEN_HEIGHT / 2 - Const.S_TILE / 2)));
        }

        public void End()
        {
            wrt.DrawBitmap(screen, new Rectangle(0, 0, Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT));
            wrt.EndDraw();
            //screen.Dispose();
        }

        public void DrawBitmap(object bitmap, Rectangle location)
        {
            wrt.DrawBitmap((Bitmap)bitmap, location);
        }

        public void DrawRectangle(Rectangle rectangle, Color c)
        {
            wrt.DrawRectangle(new SolidColorBrush(wrt, new Color4(c)), rectangle);
        }

        public void FillRectangle(Rectangle rectangle, Color c)
        {
            wrt.FillRectangle(new SolidColorBrush(wrt, new Color4(c)), rectangle);
        }

        public WindowRenderTarget GetRenderTarget()
        {
            return wrt;
        }

        public void DrawText(string text, Point location)
        {
            throw new NotImplementedException();
        }
    }
}
