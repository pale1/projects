﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable IdentifierTypo

namespace Game.Rendering
{
    public interface IRenderable
    {
        void Render(IRenderer r);
    }
}
