﻿using System;
using System.Drawing;
using Game.Component;
using Game.Hud;
using Game.Hud.Tooltip;
using Game.Model;
using SlimDX.Direct2D;
using SlimDX.DirectWrite;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Rendering
{
    public interface IRenderer : IDisposable
    {
        void Begin();
        void End();

        void Clear(Color c);

        WindowRenderTarget GetRenderTarget();

        void Render(GButton button);
        void Render(GSlot slot);
        void Render(GameEnvironment env);
        void Render(PlayerFrame playerFrame);
        void Render(CustomCursor customCursor);
        void Render(CustomTooltip customTooltip);

        void DrawText(string text, Text.BitmapFont font, PointF location, float fontSize);
        void DrawTextFormat(string text, TextFormat textFormat, Rectangle location, Color color);
        void DrawTextLayout(TextLayout textLayout, PointF origin, Color color, DrawTextOptions drawTextOptions = DrawTextOptions.None);
        void DrawBitmap(Bitmap bitmap, Rectangle location, float opacity = 1F);
        void DrawBitmap(Bitmap bitmap, Rectangle location, Rectangle sourceLocation, float opacity = 1F);
        void DrawRectangle(Rectangle rectangle, Color c, float strokeWidth = 1F);
        void FillRectangle(Rectangle rectangle, Color c);
    }
}
