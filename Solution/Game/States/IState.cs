﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Rendering;
using Game.IO;

namespace Game.States
{
    public interface IState : IMouseListener, IDisposable, IRenderable
    {
        /**
         * Call to this function updates game state by 1 tick
         */
        void Update();

        void OnEnterState();

        void OnLeaveState();
    }
}
