﻿using System.Diagnostics;
using System.Drawing;
using Game.Component;
using Game.Hud;
using Game.IO;
using Game.Rendering;

namespace Game.States
{
    public class InventoryState : ComponentHolder, IState
    {
        public InventoryState()
        {
            InitComponents();
        }
        
        private void InitComponents()
        {
            Components = new GComponent[6];
            Components[0] = new GInventoryMesh("inv", new Point(80, 44), 10, 10);
            for (int i = 1; i <= 5; i++)
            {
                var a = new GButton(i.ToString(), new Rectangle(10, 130 + i * 34, 64, 32));
                a.Action = () =>
                {
                    Sound.Player.Play("click");
                    Debug.WriteLine("Pressed: " + a.Name);
                };
                Components[i] = a;
            }
        }

        public void Update() { }
        
        public void OnEnterState() { }

        public void OnLeaveState() { }

        public new void Render(IRenderer r)
        {
            r.Begin();
            
            r.Clear(Color.FromArgb(50, 50, 50));
            
            // Draw components
            base.Render(r);
            
            // Draw Cursor
            CustomCursor.Instance.Render(r);
            
            r.End();
        }
    }
}
