using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Hud;
using Game.Model;
using Game.Rendering;
using Bitmap = SlimDX.Direct2D.Bitmap;

// ReSharper disable InconsistentNaming
namespace Game.States
{
    public class MapState : IState
    {
        private static readonly Rectangle SCREEN_SIZE = new Rectangle(0, 0, Const.SCREEN_WIDTH, Const.SCREEN_HEIGHT);
        private static readonly float ASPECT_RATIO = 1f * Const.SCREEN_WIDTH / Const.SCREEN_HEIGHT;
        
        private static readonly int MIN_WIDTH = 2 * Const.S_CHUNK * Map.PIXELS_PER_TILE; // minimum view of 2 chunk
        
        private static readonly int ZOOM_AMOUNT_X = 32 * Map.PIXELS_PER_TILE;
        private static readonly int ZOOM_AMOUNT_Y = (int)(ZOOM_AMOUNT_X / ASPECT_RATIO);

        private static readonly float ICON_SCALING_FACTOR = 3f;

        
        private readonly GameEnvironment _env;

        private Rectangle _renderedMapPart;
        
        // point where the mouse started dragging map
        private Point _mouseDragStart;
        // upper left corner of rendered map part at the moment when the mouse started dragging map
        private Point _renderedMapPartDragStart;

        private float _zoomFactorX;
        private float _zoomFactorY;
        
        public MapState(GameEnvironment env)
        {
            _env = env;
            
            int width, height;
            if (env.Map.MapTexture.Size.Width < env.Map.MapTexture.Size.Height * ASPECT_RATIO)
            {
                width = (int)env.Map.MapTexture.Size.Width;
                height = (int)(width / ASPECT_RATIO);
            }
            else
            {
                height = (int)env.Map.MapTexture.Size.Width;
                width = (int)(height * ASPECT_RATIO);
            }
            
            _renderedMapPart = new Rectangle(0, 0, width, height);
            
            _zoomFactorX = 1f * SCREEN_SIZE.Width / width;
            _zoomFactorY = 1f * SCREEN_SIZE.Height / height;
        }
        
        public void OnMouseEnter() { }

        public void OnMouseLeave() { }

        public void OnMouseMoved(MouseEventArgs e)
        {
            if (_mouseDragStart != default)
            {
                int newX = (int)(_renderedMapPartDragStart.X + (_mouseDragStart.X - CustomCursor.Instance.Location.X) / _zoomFactorX);
                int newY = (int)(_renderedMapPartDragStart.Y + (_mouseDragStart.Y - CustomCursor.Instance.Location.Y) / _zoomFactorY);
                
                if (newX + _renderedMapPart.Width > _env.Map.MapTexture.Size.Width)
                    newX = (int)(_env.Map.MapTexture.Size.Width - _renderedMapPart.Width);
                else if (newX < 0) newX = 0;
                
                if (newY + _renderedMapPart.Height > _env.Map.MapTexture.Size.Height)
                    newY = (int)(_env.Map.MapTexture.Size.Height - _renderedMapPart.Height);
                else if (newY < 0) newY = 0;
                
                _renderedMapPart = new Rectangle(new Point(newX, newY), _renderedMapPart.Size);
            }
        }

        public void OnMouseHover(Point mouse) { }

        public void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mouseDragStart = new Point(CustomCursor.Instance.Location.X, CustomCursor.Instance.Location.Y);
                _renderedMapPartDragStart = new Point(_renderedMapPart.X, _renderedMapPart.Y);
            }
        }

        public void OnMouseUp(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                _mouseDragStart = default;
                _renderedMapPartDragStart = default;
            }
        }

        public void OnMouseWheel(MouseEventArgs e)
        {
            float cursorLocX = 1f * CustomCursor.Instance.Location.X / SCREEN_SIZE.Width;
            float cursorLocY = 1f * CustomCursor.Instance.Location.Y / SCREEN_SIZE.Height;
            
            Rectangle newRenderedMapPart = new Rectangle(_renderedMapPart.X, _renderedMapPart.Y, _renderedMapPart.Width, _renderedMapPart.Height);
            if (e.Delta > 0) // zoom in
            {
                if (_renderedMapPart.Width == MIN_WIDTH)
                    return;
                
                newRenderedMapPart.X += (int)(cursorLocX * ZOOM_AMOUNT_X / _zoomFactorX);
                newRenderedMapPart.Y += (int)(cursorLocY * ZOOM_AMOUNT_Y / _zoomFactorY);
                
                newRenderedMapPart.Width -= (int)(ZOOM_AMOUNT_X / _zoomFactorX);
                newRenderedMapPart.Height -= (int)(ZOOM_AMOUNT_Y / _zoomFactorY);
                
                newRenderedMapPart.Width = Math.Max(newRenderedMapPart.Width, MIN_WIDTH);
                newRenderedMapPart.Height = (int)(newRenderedMapPart.Width / ASPECT_RATIO);

                if (newRenderedMapPart.X > _env.Map.MapTexture.Size.Width - newRenderedMapPart.Width)
                    newRenderedMapPart.X = (int)(_env.Map.MapTexture.Size.Width - newRenderedMapPart.Width);
                if (newRenderedMapPart.Y > _env.Map.MapTexture.Size.Height - newRenderedMapPart.Height)
                    newRenderedMapPart.Y = (int)(_env.Map.MapTexture.Size.Height - newRenderedMapPart.Height);
            }
            else // zoom out
            {
                if (_renderedMapPart.Width == (int)_env.Map.MapTexture.Size.Width || _renderedMapPart.Height == (int)_env.Map.MapTexture.Size.Height)
                    return;
                
                newRenderedMapPart.X = Math.Max(0, newRenderedMapPart.X - (int)(cursorLocX * ZOOM_AMOUNT_X / _zoomFactorX));
                newRenderedMapPart.Y = Math.Max(0, newRenderedMapPart.Y - (int)(cursorLocY * ZOOM_AMOUNT_Y / _zoomFactorY));
            
                newRenderedMapPart.Width += (int)(ZOOM_AMOUNT_X / _zoomFactorX);
                newRenderedMapPart.Height += (int)(ZOOM_AMOUNT_Y / _zoomFactorY);
                
                if (newRenderedMapPart.X + newRenderedMapPart.Width > _env.Map.MapTexture.Size.Width)
                {
                    if (newRenderedMapPart.Width > _env.Map.MapTexture.Size.Width)
                    {
                        newRenderedMapPart.X = 0;
                        newRenderedMapPart.Width = (int)_env.Map.MapTexture.Size.Width;
                        newRenderedMapPart.Height = (int)(newRenderedMapPart.Width / ASPECT_RATIO);
                    }
                    else
                    {
                        newRenderedMapPart.X = (int)_env.Map.MapTexture.Size.Width - newRenderedMapPart.Width;
                    }
                }
                
                if (newRenderedMapPart.Y + newRenderedMapPart.Height > _env.Map.MapTexture.Size.Height)
                {
                    if (newRenderedMapPart.Height > _env.Map.MapTexture.Size.Height)
                    {
                        newRenderedMapPart.Y = 0;
                        newRenderedMapPart.Height = (int)_env.Map.MapTexture.Size.Height;
                        newRenderedMapPart.Width = (int)(newRenderedMapPart.Height * ASPECT_RATIO);
                    }
                    else
                    {
                        newRenderedMapPart.Y = (int)_env.Map.MapTexture.Size.Height - newRenderedMapPart.Height;
                    }
                }
            }
            
            _zoomFactorX = 1f * SCREEN_SIZE.Width / newRenderedMapPart.Width;
            _zoomFactorY = 1f * SCREEN_SIZE.Height / newRenderedMapPart.Height;

            _renderedMapPart = newRenderedMapPart;
        }

        public void Dispose() { }

        public void Render(IRenderer r)
        {
            r.Begin();
            
            // Draw map
            r.DrawBitmap(_env.Map.MapTexture, SCREEN_SIZE, _renderedMapPart);
            
            // Draw icons
            RenderIcon(r, _env.Player.Icon, new Point((int)_env.Player.Location.X, (int)_env.Player.Location.Y), importantIcon: true);
            
            // Draw Cursor
            CustomCursor.Instance.Render(r);

            r.End();
        }

        public void Update() { }

        public void OnEnterState() { }

        public void OnLeaveState() { }

        private void RenderIcon(IRenderer r, Bitmap icon, Point mapLocation, bool importantIcon = false)
        {
            int iconX = mapLocation.X * Map.PIXELS_PER_TILE;
            int iconY = mapLocation.Y * Map.PIXELS_PER_TILE;
            int iconSize = (int)(20 * ICON_SCALING_FACTOR);
            if (_renderedMapPart.Contains(iconX, iconY) || importantIcon)
            {
                iconX = (int)((iconX - _renderedMapPart.X) * _zoomFactorX);
                iconY = (int)((iconY - _renderedMapPart.Y) * _zoomFactorY);
                if (importantIcon)
                {
                    iconX = iconX < 0
                        ? 0
                        : Math.Min(SCREEN_SIZE.Width, iconX);
                    iconY = iconY < 0
                        ? 0
                        : Math.Min(SCREEN_SIZE.Height, iconY);
                }
            }
            
            r.DrawBitmap(icon, new Rectangle(
                iconX - iconSize / 2, 
                iconY - iconSize / 2, 
                iconSize, iconSize));
        }
    }
}