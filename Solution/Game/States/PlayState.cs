﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Game.Hud;
using Game.IO;
using Game.Rendering;
using GameEnvironment = Game.Model.GameEnvironment;

namespace Game.States
{
    public class PlayState : IState
    {
        private readonly GameEnvironment _env;
        private readonly HUD _hud;

        private KeyManager.KeyEventHandler _itemUseEventHandler;

        public PlayState(GameEnvironment env)
        {
            _env = env;
            _hud = new HUD(_env.Player);
            _itemUseEventHandler = () =>
            {
                _hud.ItemUsed(_env); // if item use key gets pressed, HUD will handle it
            };
        }

        public void Update()
        {
            PointF newLocation = new PointF(_env.Player.Location.X, _env.Player.Location.Y);
            Point chunkId = _env.Player.ChunkId;

            if (KeyManager.IsDown(Keys.Left))
            {
                _env.Player.MoveSpeed.X = Math.Max(-_env.Player.MaxMoveSpeed, _env.Player.MoveSpeed.X - _env.Player.Acceleration);

                float move = _env.Map.CheckHorizontalCollision(newLocation, _env.Player.MoveSpeed.X);
                if (move < -.01F)
                    newLocation.X += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.X = 0;
            }
            else if (_env.Player.MoveSpeed.X < 0)
            {
                _env.Player.MoveSpeed.X = Math.Min(0, _env.Player.MoveSpeed.X + _env.Player.Deceleration);
                float move = _env.Map.CheckHorizontalCollision(newLocation, _env.Player.MoveSpeed.X);
                if (move < -.01F)
                    newLocation.X += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.X = 0;
            }
            
            if (KeyManager.IsDown(Keys.Right))
            {
                _env.Player.MoveSpeed.X = Math.Min(_env.Player.MaxMoveSpeed, _env.Player.MoveSpeed.X + _env.Player.Acceleration);
                
                float move = _env.Map.CheckHorizontalCollision(newLocation, _env.Player.MoveSpeed.X);
                if (move > .01F)
                    newLocation.X += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.X = 0;
            }
            else if (_env.Player.MoveSpeed.X > 0)
            {
                _env.Player.MoveSpeed.X = Math.Max(0, _env.Player.MoveSpeed.X - _env.Player.Deceleration);
                float move = _env.Map.CheckHorizontalCollision(newLocation, _env.Player.MoveSpeed.X);
                if (move > .01F)
                    newLocation.X += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.X = 0;
            }
            
            if (KeyManager.IsDown(Keys.Up))
            {
                _env.Player.MoveSpeed.Y = Math.Max(-_env.Player.MaxMoveSpeed, _env.Player.MoveSpeed.Y - _env.Player.Acceleration);
                
                float move = _env.Map.CheckVerticalCollision(newLocation, _env.Player.MoveSpeed.Y);
                if (move < -.01F)
                    newLocation.Y += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.Y = 0;
            }
            else if (_env.Player.MoveSpeed.Y < 0)
            {
                _env.Player.MoveSpeed.Y = Math.Min(0, _env.Player.MoveSpeed.Y + _env.Player.Deceleration);
                float move = _env.Map.CheckVerticalCollision(newLocation, _env.Player.MoveSpeed.Y);
                if (move < -.01F)
                    newLocation.Y += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.Y = 0;
            }
            
            if (KeyManager.IsDown(Keys.Down))
            {
                _env.Player.MoveSpeed.Y = Math.Min(_env.Player.MaxMoveSpeed, _env.Player.MoveSpeed.Y + _env.Player.Acceleration);
                
                float move = _env.Map.CheckVerticalCollision(newLocation, _env.Player.MoveSpeed.Y);
                if (move > 0.01F)
                    newLocation.Y += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.Y = 0;
            }
            else if (_env.Player.MoveSpeed.Y > 0)
            {
                _env.Player.MoveSpeed.Y = Math.Max(0, _env.Player.MoveSpeed.Y - _env.Player.Deceleration);
                float move = _env.Map.CheckVerticalCollision(newLocation, _env.Player.MoveSpeed.Y);
                if (move > 0.01F)
                    newLocation.Y += move;
                else if (move == 0) // player has bumped wall
                    _env.Player.MoveSpeed.Y = 0;
            }
            
            _env.Player.Location = newLocation;
            _env.Camera = newLocation;
            
            // if player stepped into new chunk, load new neigboring chunks
            Point newChunkId = _env.Player.ChunkId;
            if (chunkId != newChunkId)
                _env.ReloadRelevantChunks(newChunkId);
        }

        public void OnEnterState()
        {
            KeyManager.AddKeyPressedHandler(KeyManager.KeyFor("use_item"), _itemUseEventHandler);
        }

        public void OnLeaveState()
        {
            KeyManager.RemoveKeyPressedHandler(KeyManager.KeyFor("use_item"), _itemUseEventHandler);
        }

        public void Render(IRenderer r)
        {
            r.Begin();

            r.Clear(Color.FromArgb(0, 162, 232));

            // Draw environment
            _env.Render(r);

            // Draw HUD
            _hud.Render(r);
            
            // Draw Cursor
            CustomCursor.Instance.Render(r);

            r.End();
        }

        public void OnMouseEnter()
        {
            _hud.OnMouseEnter();
        }

        public void OnMouseLeave()
        {
            _hud.OnMouseLeave();
        }

        public void OnMouseMoved(MouseEventArgs e)
        {
            _hud.OnMouseMoved(e);
        }
        public void OnMouseHover(Point mouse)
        {
            _hud.OnMouseHover(mouse);
        }

        public void OnMouseDown(MouseEventArgs e)
        {
            _hud.OnMouseDown(e);
        }
        public void OnMouseUp(MouseEventArgs e)
        {
            _hud.OnMouseUp(e);
        }

        public void OnMouseWheel(MouseEventArgs e)
        {
            _hud.OnMouseWheel(e);
        }

        public void Dispose()
        {
            _hud.Dispose();
        }
    }
}
