namespace Game.States
{
    public abstract class State
    {
        public static readonly int Start = 0;
        public static readonly int Play = 1;
        public static readonly int Pause = 2;
        public static readonly int Inventory = 3;
        public static readonly int Map = 4;
        public static readonly int Options = 5;
        public static readonly int NumStates = 6;
    }
}