﻿using System;
using System.Drawing;
using Game.IO;
using Game.Model;
using Game.Model.Entity;

namespace Game.States
{
    public class StateManager : IDisposable
    {
        private readonly IState[] _states;
        public IState CurrentState { get; private set; }
        public int CurrentStateNum { get; private set; }

        public StateManager()
        {
            Player player = new Player(new PointF(4, 4), "Jebediah");

            GameEnvironment env = new GameEnvironment(
                Loaders.MAP.GetResource("map1"),
                player
            );
            
            _states = new IState[State.NumStates];
            _states[1] = new PlayState(env);
            _states[3] = new InventoryState();
            _states[4] = new MapState(env);

            ChangeState(State.Play);
        }

        public void ChangeState(int i)
        {
            CurrentState?.OnLeaveState();
            
            CurrentState = _states[i];
            CurrentStateNum = i;
            
            CurrentState.OnEnterState();
        }

        public void Dispose()
        {
            foreach (IState state in _states)
                state?.Dispose();
        }
    }
}
