﻿+ 1.) Make resolution independent from window size
+ 2.) Add fullscreen mode
+ 3.) Fix text rendering (DPI)
+   3.1) Support different font sizes
+   3.2) Support colored text
+   3.3) Switch to DirectX custom fonts
+ 4.) BLURRY GRAPHICS
+ 5.) Add HUD scaling
+ 6.) Fix hovering over inventory
+ 7.) Add acceleration of player
  8.) Render first on a small bitmap, then render upscaled bitmap to screen (maybe not needed)
  9.) Proper resource handling
  10.) Separate game loop into tick and rendering threads
+ 11.) Custom cursor and rendering of held item near cursor
+ 12.) Move textures to a single sprite sheet
  13.) Add pop up windows
+ 14.) Add animated player and terrain
  15.) Add ability to save game
+ 16.) Add player frame