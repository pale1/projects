﻿using System;
using System.Drawing;
using Game.Texture;
using Size = System.Drawing.Size;

namespace Game.Text
{
    /**
     * Custom fonts contained in PNG files (slow rendering compared to OTF fonts)
     */
    public class BitmapFont : PlainSpriteSheet
    {
        public static readonly BitmapFont CLASSIC_BLACK = new BitmapFont("classic_black", 14, 24);
        
        public BitmapFont(string fontFileName, int width, int height) : base($"{Const.FONT_TEXTURES_FOLDER}/{fontFileName}", width, height, 94) { }

        public Rectangle GetCharacterBounds(char c)
        {
            return GetSpriteBounds(c - 33);
        }

        public Size GetLineMetrics(string s)
        {
            return new Size(s.Length * SpriteWidth, SpriteHeight);
        }

        public Size GetStringMetrics(string s)
        {
            int cols = 0;
            int rows = 0;
            foreach (string line in s.Split('\n'))
            {
                cols = Math.Max(line.Length, cols);
                rows++;
            }
            return new Size(cols * SpriteWidth, rows * SpriteHeight);
        }
    }
}
