using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using SlimDX.DirectWrite;
using Debug = System.Diagnostics.Debug;
using FontStyle = SlimDX.DirectWrite.FontStyle;
using FontWeight = SlimDX.DirectWrite.FontWeight;

namespace Game.Text
{
    public static class FontBank
    {
        // pairs of (Font family name, Font file name) that are available for use
        private static readonly Dictionary<string, string> AvailableFonts = new Dictionary<string, string>
        {
            // (Font family name, Font file name)
            { "Pixeloid Sans", "pixeloid_sans_bold" }
        };
        
        // Constants with which font size is multiplied so each font is the same size
        private static readonly Dictionary<string, float> FontSizeNormalizationConstants = new Dictionary<string, float>();

        // font family used for text bubbles etc
        public const string TextFont = "Pixeloid Sans";
        // font family used for inventory count numbers
        public const string InventoryFont = "Pixeloid Sans";
        // font family used for tooltip text
        public const string TooltipFont = "Pixeloid Sans";
        // font family used for name in player frame
        public const string NameFont = "Pixeloid Sans";

        private static readonly Factory DwFactory = new Factory();
        private static readonly List<string> UsedFontNames = new List<string>();
        
        public static void Init()
        {
            // get all font names from this class using reflection, add them to os and to UsedFontNames
            FieldInfo[] fields = typeof(FontBank).GetFields(BindingFlags.Public | BindingFlags.Static);
            foreach (FieldInfo field in fields)
            {
                if (field.FieldType == typeof(string))
                {
                    string fontFamilyName = (string)field.GetValue(null);
                    string fontFileName = AvailableFonts[fontFamilyName];
                    if (!UsedFontNames.Contains(fontFileName))
                    {
                        if (AddFontResource($"{Const.TEXTURES_PATH}/{Const.FONT_TEXTURES_FOLDER}/{fontFileName}.ttf") == 0)
                            Debug.WriteLine($"Error adding font resource: {fontFileName}");
                    
                        UsedFontNames.Add(fontFileName);
                        
                        // measure text height to calculate font size normalization constant
                        TextFormat tf = new TextFormat(DwFactory, fontFamilyName, FontWeight.Normal, FontStyle.Normal, FontStretch.Normal, 10f, "en");
                        TextLayout tl = new TextLayout(DwFactory, "a1", tf);
                        FontSizeNormalizationConstants.Add(fontFamilyName, 20f / tl.Metrics.Height);
                        tf.Dispose();
                        tl.Dispose();
                    }
                }
            }
        }

        public static TextFormat CreateTextFormat(string familyName, FontWeight fontWeight = FontWeight.Normal, FontStyle fontStyle = FontStyle.Normal, FontStretch fontStretch = FontStretch.Normal, float fontSize = 10f, string localeName = "en")
        {
            return new TextFormat(DwFactory, familyName, fontWeight, fontStyle, fontStretch, fontSize * FontSizeNormalizationConstants[familyName], localeName);
        }

        public static TextLayout CreateTextLayout(string text, TextFormat textFormat, int maxWidth = Int32.MaxValue, int maxHeight = Int32.MaxValue)
        {
            return new TextLayout(DwFactory, text, textFormat, maxWidth, maxHeight);
        }
        
        public static void Dispose()
        {
            DwFactory.Dispose();
            foreach (string fontFileName in UsedFontNames)
                RemoveFontResource($"{Const.TEXTURES_PATH}/{Const.FONT_TEXTURES_FOLDER}/{fontFileName}.ttf");
        }
        
        [DllImport("gdi32.dll")]
        private static extern int AddFontResource(string name);
        [DllImport("gdi32.dll")]
        private static extern int RemoveFontResource(string name);
    }
}
