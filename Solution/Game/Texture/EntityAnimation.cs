using System.Drawing;

namespace Game.Texture
{
    
    /**
     * Entity animation enables 2 features that single bitmap animations don't have:
     * 1.) Upon changing sprite index, animation resets to first frame of animation for that sprite
     * 2.) First frame of animation lasts just as long as other frames, independent of global FrameNum counter
     */
    public class EntityAnimation : SingleBitmapAnimation
    {
        /**
         * Last animated sprite that was accessed with GetSpriteBounds
         */
        private int _lastSprite = -1;
        
        /**
         * Number of game frame in which currently rendered animation frame started rendering
         */
        private long _frameNum;
        
        /// <summary>
        /// Creates a new entity animation
        /// </summary>
        /// <param name="spriteSheetFileName">name of PNG containing sprites inside textures folder</param>
        /// <param name="spriteWidth">pixel width of each sprite in PNG</param>
        /// <param name="spriteHeight">pixel height of each sprite in PNG</param>
        /// <param name="numAnimations">number of different animated sprites (rows) that the PNG contains</param>
        /// <param name="numAnimationFrames">number of animation frames (columns) that every animated sprite has</param>
        /// <param name="frameTiming">time between animation frames in seconds</param>
        public EntityAnimation(string spriteSheetFileName, int spriteWidth, int spriteHeight, int numAnimations, int numAnimationFrames, int frameTiming = 0) 
            : base(spriteSheetFileName, spriteWidth, spriteHeight, numAnimations, numAnimationFrames, frameTiming) { }
        
        /// <summary>
        /// Creates a new single bitmap animation
        /// </summary>
        /// <param name="spriteSheetFileName">name of PNG containing sprites inside textures folder</param>
        /// <param name="spriteWidth">pixel width of each sprite in PNG</param>
        /// <param name="spriteHeight">pixel height of each sprite in PNG</param>
        /// <param name="numAnimations">number of different animated sprites (rows) that the PNG contains</param>
        /// <param name="numAnimationFrames">number of animation frames (columns) for each animated sprite</param>
        /// <param name="frameTiming">time between animation frames in seconds</param>
        public EntityAnimation(string spriteSheetFileName, int spriteWidth, int spriteHeight, int numAnimations, int[] numAnimationFrames, int frameTiming = 0) 
            : base(spriteSheetFileName, spriteWidth, spriteHeight, numAnimations, numAnimationFrames, frameTiming) { }
        
        public override Rectangle GetSpriteBounds(int spriteNum)
        {
            int animationFrame;
            if (spriteNum != _lastSprite)
            {
                _frameNum = Frame.FrameNum;
                _lastSprite = spriteNum;
                animationFrame = 0;
            }
            else
            {
                animationFrame = (int)((Frame.FrameNum - _frameNum) / _frameTiming % NumSpritesInRow);
            }
            
            int index = spriteNum * NumSpritesInRow + animationFrame;
            
            return new Rectangle(SpriteLocations[index], SpriteSize);
        }
    }
}