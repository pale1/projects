using System.Drawing;
using Game.IO;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Texture
{
    /**
     * Multi bitmap animation assumes that for the name of sprite sheet,
     * there are numAnimationFrames sprite sheet PNG files named like spriteSheetFileName_index that each contain one frame of animation for all the sprites contained in the file
     */
    public class MultiBitmapAnimation : SpriteSheet
    {
        private readonly Bitmap[] _spriteSheetBitmaps;
        protected readonly int NumAnimationFrames;

        /// <summary>
        /// Creates a new multi bitmap animation
        /// </summary>
        /// <param name="spriteSheetFileName">name of PNGs containing sprites inside textures folder</param>
        /// <param name="spriteWidth">pixel width of each sprite in PNGs</param>
        /// <param name="spriteHeight">pixel height of each sprite in PNGs</param>
        /// <param name="numSpritesInBitmap">number of sprites in each PNG</param>
        /// <param name="numSpritesInRow">number of sprites in each row of each PNG</param>
        /// <param name="numAnimationFrames">number of animation frames that every animated sprite has (is also number of PNGs that are part of the animations)</param>
        public MultiBitmapAnimation(string spriteSheetFileName, int spriteWidth, int spriteHeight, int numSpritesInBitmap, int numSpritesInRow, int numAnimationFrames)
            : base(spriteWidth, spriteHeight, numSpritesInBitmap, numSpritesInRow)
        {
            _spriteSheetBitmaps = new Bitmap[numAnimationFrames];
            NumAnimationFrames = numAnimationFrames;
            
            for (int i = 0; i < numAnimationFrames; i++)
                _spriteSheetBitmaps[i] = Loaders.TEXTURE_DX.GetResource($"{spriteSheetFileName}_{i}");
        }
        

        public override Bitmap GetSpriteSheet()
        {
            int bitmapNum = (int)(Frame.FrameNum / RTC.DEF_FRAME_TIMING % NumAnimationFrames);
            return _spriteSheetBitmaps[bitmapNum];
        }

        public override Rectangle GetSpriteBounds(int spriteNum)
        {
            return new Rectangle(SpriteLocations[spriteNum], SpriteSize);
        }

        public override void Dispose()
        {
            foreach (Bitmap b in _spriteSheetBitmaps)
                b.Dispose();
        }
    }
}