using System.Drawing;
using Game.IO;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Texture
{
    public class PlainSpriteSheet : SpriteSheet
    {
        private readonly Bitmap _spriteSheetBitmap;
        
        public PlainSpriteSheet(string spriteSheetFileName, int spriteWidth, int spriteHeight, int numSprites, int numSpritesInRow = 20) 
            : this(Loaders.TEXTURE_DX.GetResource(spriteSheetFileName), spriteWidth, spriteHeight, numSprites, numSpritesInRow) { }

        public PlainSpriteSheet(Bitmap spriteSheetBitmap, int spriteWidth, int spriteHeight, int numSprites, int numSpritesInRow = 20) 
            : base(spriteWidth, spriteHeight, numSprites, numSpritesInRow)
        {
            _spriteSheetBitmap = spriteSheetBitmap;
        }

        public override Bitmap GetSpriteSheet()
        {
            return _spriteSheetBitmap;
        }

        public override Rectangle GetSpriteBounds(int spriteNum)
        {
            return new Rectangle(SpriteLocations[spriteNum], SpriteSize);
        }

        public override void Dispose()
        {
            _spriteSheetBitmap.Dispose();
        }
    }
}