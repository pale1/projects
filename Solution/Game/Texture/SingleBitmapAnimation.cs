using System.Drawing;
using System.Linq;
using Game.IO;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Texture
{
    /**
     * Single bitmap animation assumes that for every sprite in bitmap, there is a row of numSpritesInRow sprites that represent animation of that sprite.
     * Bitmap contains numAnimations different animated sprites, totaling to numAnimations x numSpritesInRow contained sprites
     */
    public class SingleBitmapAnimation : SpriteSheet
    {
        private readonly Bitmap _spriteSheetBitmap;
        
        protected int[] _numAnimationFrames;
        // time between animation frames as number of game frames
        protected int _frameTiming;
        
        /// <summary>
        /// Creates a new single bitmap animation
        /// </summary>
        /// <param name="spriteSheetFileName">name of PNG containing sprites inside textures folder</param>
        /// <param name="spriteWidth">pixel width of each sprite in PNG</param>
        /// <param name="spriteHeight">pixel height of each sprite in PNG</param>
        /// <param name="numAnimations">number of different animated sprites (rows) that the PNG contains</param>
        /// <param name="numAnimationFrames">number of animation frames (columns) that every animated sprite has</param>
        /// <param name="frameTiming">time between animation frames in seconds</param>
        public SingleBitmapAnimation(string spriteSheetFileName, int spriteWidth, int spriteHeight, int numAnimations, int numAnimationFrames, float frameTiming = 0) 
            : base(spriteWidth, spriteHeight, numAnimations * numAnimationFrames, numAnimationFrames)
        {
                
            _spriteSheetBitmap = Loaders.TEXTURE_DX.GetResource(spriteSheetFileName);

            _numAnimationFrames = new int[numAnimations];
            for (int x = 0; x < numAnimations; x++)
                _numAnimationFrames[x] = numAnimationFrames;

            _frameTiming = (int)(frameTiming != 0 ? Const.FPS * frameTiming : RTC.DEF_FRAME_TIMING);
        }
        
        /// <summary>
        /// Creates a new single bitmap animation
        /// </summary>
        /// <param name="spriteSheetFileName">name of PNG containing sprites inside textures folder</param>
        /// <param name="spriteWidth">pixel width of each sprite in PNG</param>
        /// <param name="spriteHeight">pixel height of each sprite in PNG</param>
        /// <param name="numAnimations">number of different animated sprites (rows) that the PNG contains</param>
        /// <param name="numAnimationFrames">number of animation frames (columns) for each animated sprite</param>
        /// <param name="frameTiming">time between animation frames in seconds</param>
        public SingleBitmapAnimation(string spriteSheetFileName, int spriteWidth, int spriteHeight, int numAnimations, int[] numAnimationFrames, float frameTiming = 0) 
            : this(spriteSheetFileName, spriteWidth, spriteHeight, numAnimations, numAnimationFrames.Max(), frameTiming)
        {
            _numAnimationFrames = numAnimationFrames;
        }

        public override Bitmap GetSpriteSheet()
        {
            return _spriteSheetBitmap;
        }

        public override Rectangle GetSpriteBounds(int spriteNum)
        {
            int frame = (int)(Frame.FrameNum / RTC.DEF_FRAME_TIMING % _numAnimationFrames[spriteNum]);
            int index = spriteNum * NumSpritesInRow + frame;
            
            return new Rectangle(SpriteLocations[index], SpriteSize);
        }

        public override void Dispose()
        {
            _spriteSheetBitmap.Dispose();
        }
    }
}