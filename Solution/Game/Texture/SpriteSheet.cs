using System;
using System.Drawing;
using Bitmap = SlimDX.Direct2D.Bitmap;

namespace Game.Texture
{
    
    /**
     * Base class describing a sprite sheet that contains a number of sprites/textures
     */
    public abstract class SpriteSheet : IDisposable
    {
        public int NumSprites { get; }
        public int NumSpritesInRow { get; }

        public int SpriteWidth { get; }
        public int SpriteHeight { get; }
        
        protected Size SpriteSize { get; }
        /**
         * Array containing sprite locations in bitmap, where i-th element of array has the location of i-th sprite in the bitmap
         */
        protected Point[] SpriteLocations { get; }
        
        public SpriteSheet(PlainSpriteSheet pss) : this(pss.SpriteWidth, pss.SpriteHeight, pss.NumSprites, pss.NumSpritesInRow) { }
        
        public SpriteSheet(int spriteWidth, int spriteHeight, int numSprites, int numSpritesInRow)
        {
            SpriteWidth = spriteWidth;
            SpriteHeight = spriteHeight;
            NumSprites = numSprites;
            NumSpritesInRow = numSpritesInRow;
            
            SpriteSize = new Size(SpriteWidth, SpriteHeight);
            SpriteLocations = new Point[NumSprites];
            for (int n = 0; n < NumSprites; n++)
            {
                SpriteLocations[n] = new Point(
                    n % NumSpritesInRow * SpriteWidth,
                    n / NumSpritesInRow * SpriteHeight);
            }
        }

        /**
         * Gets the bitmap that contains sprites
         */
        public abstract Bitmap GetSpriteSheet();
        
        /**
         * Gets the rectangle on sprite sheet that contains sprite of id specified
         */
        public abstract Rectangle GetSpriteBounds(int spriteNum);
        public abstract void Dispose();
    }
}