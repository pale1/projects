namespace Game.Texture
{
    public static class SpriteSheets
    {
        public static SpriteSheet TileSheet = new MultiBitmapAnimation($"{Const.TILE_TEXTURES_FOLDER}/tile_sheet", 32, 32, 17, 20, 4);
        public static SpriteSheet PlayerSheet = new EntityAnimation("player_frames", 64, 64, 5, 4);

        public static SpriteSheet FancyTooltipSheet = new SingleBitmapAnimation("fancy_tooltip_frames", 32, 32, 7, new [] {4,4,4,4,4,1,1});


        public static void Dispose()
        {
            TileSheet.Dispose();
            PlayerSheet.Dispose();
            
            FancyTooltipSheet.Dispose();
        }
    }
}