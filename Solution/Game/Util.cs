﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;

// ReSharper disable once InconsistentNaming
namespace Game
{
    public abstract class Util
    {
        public static void Write(string str)
        {
            Debug.WriteLine(str);
        }

        /**
         * Script that creates .csv file for each chunk present in worldmap.csv. 
         * Execute it only when changing worldmap.csv to generate updated chunk files
         */
        public static void CreateChunkCSVs(string map)
        {
            int chunkSize = Const.S_CHUNK;
            
            string[] lines = File.ReadAllLines($"{Const.MAPS_PATH}{map}/worldmap.csv");
            if (lines.Length % chunkSize != 0)
                throw new Exception($"Bad format: File has {lines.Length} lines!");
            
            // init all chunkBuilders
            StringBuilder[,] chunkBuilders = new StringBuilder[lines.Length / chunkSize, lines[0].Split(',').Length / chunkSize];
            for (int y = 0; y < chunkBuilders.GetLength(1); y++)
                for (int x = 0; x < chunkBuilders.GetLength(0); x++)
                    chunkBuilders[y, x] = new StringBuilder();
            
            int j = 0;
            foreach (string line in lines)
            {
                int i = 0;
                foreach (string s in line.Split(','))
                {
                    chunkBuilders[j / chunkSize, i / chunkSize].Append(s);
                    if (i % chunkSize != chunkSize - 1) // prevents comma after last line element
                        chunkBuilders[j / chunkSize, i / chunkSize].Append(',');
                    else
                        chunkBuilders[j / chunkSize, i / chunkSize].AppendLine();
                    
                    i++;
                }
                j++;
                
                if (i % chunkSize != 0)
                    throw new Exception($"Bad format: Line {j} has {i} elements!");
            }

            // write all chunkBuilders to chunk files
            for (int y = 0; y < chunkBuilders.GetLength(1); y++)
                for (int x = 0; x < chunkBuilders.GetLength(0); x++)
                    File.WriteAllText($"{Const.MAPS_PATH}{map}/chunk_{y}_{x}.csv", chunkBuilders[y,x].ToString());
        }
    }
}