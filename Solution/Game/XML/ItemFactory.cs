﻿using System;
using System.IO;
using System.Xml;
using Game.IO;
using Game.Model.Item2;

namespace Game.XML
{
    public static class ItemFactory
    {
        private static string ITEM_DATA_FILENAME = $"{Const.XML_PATH}Items.xml";

        private static readonly PItem[] _items = new PItem[128];

        static ItemFactory()
        {
            if (File.Exists(ITEM_DATA_FILENAME))
            {
                XmlDocument data = new XmlDocument();
                data.LoadXml(File.ReadAllText(ITEM_DATA_FILENAME));

                LoadItemsFromNodes(data.SelectNodes("/Items/Armor/Armor"), ItemType.Armor);
                LoadItemsFromNodes(data.SelectNodes("/Items/Weapons/Weapon"), ItemType.Weapon);
                LoadItemsFromNodes(data.SelectNodes("/Items/Tools/Tool"), ItemType.Tool);
                LoadItemsFromNodes(data.SelectNodes("/Items/Food/Food"), ItemType.Food);
                LoadItemsFromNodes(data.SelectNodes("/Items/Drinks/Drink"), ItemType.Drink);
                LoadItemsFromNodes(data.SelectNodes("/Items/Valuables/Valuable"), ItemType.Valuable);
                LoadItemsFromNodes(data.SelectNodes("/Items/Ingredients/Ingredient"), ItemType.Ingredient);
            }
            else
            {
                throw new FileNotFoundException($"Missing data file: {ITEM_DATA_FILENAME}");
            }
        }

        private static void LoadItemsFromNodes(XmlNodeList nodes, ItemType itemType)
        {
            if (nodes == null)
                return;

            foreach (XmlNode node in nodes)
            {
                PItem item = new PItem(GetXmlAttributeAsInt(node, "ID"), 
                                itemType,
                                PItem.GetQuality(GetXmlAttribute(node, "Quality")),
                                 GetXmlAttribute(node, "Name"),
                                 GetXmlAttribute(node, "Description"),
                                GetXmlAttributeAsInt(node, "Stack"),
                                Loaders.TEXTURE.GetResource($"item_textures/{GetXmlAttribute(node, "Name")}"),
                                 Convert.ToBoolean(GetXmlAttributeOrElse(node, "Quest", "false")));

                _items[item.Id] = item;
            }
        }

        public static PItem GetItem(int id)
        {
            if (id < 0 || id > _items.Length)
                throw new ArgumentOutOfRangeException($"Item ID out of range: {id}");
            return _items[id];
        }

        private static int GetXmlAttributeAsInt(XmlNode node, string attributeName)
        {
            return Convert.ToInt32(GetXmlAttribute(node, attributeName));
        }

        private static string GetXmlAttribute(XmlNode node, string attributeName)
        {
            XmlAttribute attribute = node.Attributes?[attributeName];

            if (attribute == null)
            {
                throw new ArgumentException($"The attribute '{attributeName}' does not exist");
            }

            return attribute.Value;
        }
        
        private static string GetXmlAttributeOrElse(XmlNode node, string attributeName, string ifNull)
        {
            XmlAttribute attribute = node.Attributes?[attributeName];

            if (attribute == null)
                return ifNull;

            return attribute.Value;
        }
    }
}
