﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using Game.IO;
using Game.Model;

namespace Game.XML
{
    public abstract class TileFactory
    {
        private static string TILE_DATA_FILENAME = Const.XML_PATH + "Tiles.xml";

        private static readonly List<Tile> _tiles = new List<Tile>();

        static TileFactory()
        {
            if (File.Exists(TILE_DATA_FILENAME))
            {
                XmlDocument data = new XmlDocument();
                data.LoadXml(File.ReadAllText(TILE_DATA_FILENAME));

                LoadItemsFromNodes(data.SelectNodes("/Tiles/Tile"));
            }
            else
            {
                throw new FileNotFoundException($"Missing data file: {TILE_DATA_FILENAME}");
            }
        }

        private static void LoadItemsFromNodes(XmlNodeList nodes)
        {
            if (nodes == null)
                return;

            foreach (XmlNode node in nodes)
            {
                Tile tile = new Tile(GetXmlAttributeAsInt(node, "ID"),
                                    GetXmlAttribute(node, "Name"),
                                    Loaders.TEXTURE.GetResource($"tile_textures/{GetXmlAttribute(node, "Name")}"),
                                    Convert.ToBoolean(GetXmlAttribute(node, "Collision")));

                _tiles.Add(tile);
            }
        }

        public static Tile GetTile(int ID)
        {
            if (ID < 0 || ID > _tiles.Count)
                throw new ArgumentOutOfRangeException($"Tile ID out of range: {ID}");
            return _tiles[ID];
        }

        private static int GetXmlAttributeAsInt(XmlNode node, string attributeName)
        {
            return Convert.ToInt32(GetXmlAttribute(node, attributeName));
        }

        private static string GetXmlAttribute(XmlNode node, string attributeName)
        {
            XmlAttribute attribute = node.Attributes?[attributeName];

            if (attribute == null)
            {
                throw new ArgumentException($"The attribute '{attributeName}' does not exist");
            }

            return attribute.Value;
        }
    }
}
