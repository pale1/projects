package app;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JToolBar;
import javax.swing.JToolTip;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import profiles.Profile;
import profiles.ProfileFrame;
import wowapp.model.BodyPart;
import wowapp.model.Gear;
import wowapp.model.Weapon;
import wowapp.model.WeaponType;

public class AppFrame extends JFrame {

	public static Set<Gear> items = new HashSet<>();
	public static File itemsFile = new File(System.getProperty("user.dir")+"/items.ser");

	private static final long serialVersionUID = 1L;
	public static final int ATTRNO = 19;
	public static final String[] ATTRS = {"Strength", "Agility", "Stamina", "Intellect", "Spirit",
			"Attack Power", "Spell Power", "Shield Block", "Haste", "Armor Penetration", "Critical Strike", "Hit",
			"Defense", "Parry", "Spell Penetration", "Dodge", "Expertise", "Resilience", "Mana Per 5"
	};
	private static final String[] SLOTS = {"Head", "Neck", "Shoulder", "Back", "Chest", "Shirt", "Tabard", "Wrist",
			"Hands", "Waist", "Legs", "Feet", "Finger", "Finger", "Trinket", "Trinket",
			"MainHand", "OffHand", "Ranged"
	};
	private static final String[] STATTYPE = {"Base Stats", "Melee", "Ranged", "Spell", "Defenses"
	};
	Gear[] myGear;
	int[] myStats = new int[ATTRNO];
	int myArmor = 0, myMinDmg = 0, myMaxDmg = 0;
	double mySpeed1 = 0, mySpeed2 = 0;
	
	JPanel lStatsPanel = new JPanel(new GridLayout(0, 1));
	JPanel rStatsPanel = new JPanel(new GridLayout(0, 1));
	JPanel lPanel = new JPanel(new GridLayout(0, 1, 4, 4));
	JPanel rPanel = new JPanel(new GridLayout(0, 1, 4, 4));
	JPanel cPanel = new JPanel(new GridLayout(0, 2, 4, 4));
	JPanel sPanel = new JPanel();
	JPanel sInnerPanel = new JPanel(new GridLayout(1, 0, 4, 4));
	JPanel lInnerPanel = new JPanel(new BorderLayout());
	JPanel rInnerPanel = new JPanel(new BorderLayout());
	JPanel pane = new JPanel();
	TitledBorder lBorder = BorderFactory.createTitledBorder(null, "Base Stats", TitledBorder.LEADING, TitledBorder.TOP);
	TitledBorder rBorder = BorderFactory.createTitledBorder(null, "Base Stats", TitledBorder.LEADING, TitledBorder.TOP);
	JToolBar toolbar = new JToolBar();
	
	JButton newBtn = new JButton("New Item");
	JButton itemsBtn = new JButton("Browse Items");
	JButton[] slotBtns = new JButton[19];
	JButton selectedBtn = null;
	
	JComboBox<String> lCombo = new JComboBox<>(STATTYPE);
	JComboBox<String> rCombo = new JComboBox<>(STATTYPE);
	JLabel[] statLabels = new JLabel[12];
	
	JFrame newFrame	= new JFrame("Create new item");
	JFrame itemsFrame = new JFrame("Browse items");
	ItemsPanel itemsPanel; NewPanel newPanel;
	
	JFrame chooserFrame = new JFrame();
	JPanel chooserPanel = new JPanel();
	JRadioButton[] radioBtns;
	ButtonGroup grp = new ButtonGroup();
	JButton equipBtn = new JButton("Equip item");
	
	@SuppressWarnings("unchecked")
	public AppFrame(Profile profile) {
		setLayout(new BorderLayout());
		setIconImage((new ImageIcon(System.getProperty("user.dir")+"/wow.png")).getImage());
		setResizable(false);
		myGear = profile.getGear();
		
		//Buttons
		for(int i = 0; i < 19; i++) {
			slotBtns[i] = new JButton() {
				private static final long serialVersionUID = 1L;
				@Override
				public JToolTip createToolTip() {
			        JToolTip tip = super.createToolTip();
			        tip.setBackground(Color.BLACK);
			        tip.setForeground(Color.WHITE);
			        return tip;
			      }
			};
			slotBtns[i].setToolTipText(SLOTS[i]);
			slotBtns[i].setPreferredSize(new Dimension(60, 60));
		}

		for(int i = 0; i < 8; i++) lPanel.add(slotBtns[i]);
		for(int i = 8; i < 16; i++) rPanel.add(slotBtns[i]);
		for(int i = 16; i < 19; i++) sInnerPanel.add(slotBtns[i]);
		for(int i = 0; i < 12; i++) {
			statLabels[i] = new JLabel("", SwingConstants.LEFT);
			statLabels[i].setFont(new Font(Font.MONOSPACED, Font.BOLD, 13));

		}
		for(int i = 0; i < 6; i++) lStatsPanel.add(statLabels[i]);
		for(int i = 6; i < 12; i++) rStatsPanel.add(statLabels[i]);
		
		//Import
		
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(itemsFile))) {
			items = (Set<Gear>) ois.readObject();
		} catch (FileNotFoundException e1) {
			System.out.println("Items file not found!");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {}
		
		//Export
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent we) {
				try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(itemsFile))) {
					
					ProfileFrame.onClose();
					if(itemsFile.exists()) itemsFile.delete();
					itemsFile.createNewFile();
					if(!items.isEmpty()) oos.writeObject(items);
					
				} catch (FileNotFoundException e) {}
				catch (IOException e) {}
			}
		});
		
		//Adding profile Gear
		
		for(int i = 0; i < 19; i++) {
			if(myGear[i] != null) {
				slotBtns[i].setIcon(new ImageIcon(myGear[i].getIconPath()));
				slotBtns[i].setToolTipText(""+myGear[i]);
				refreshStats(null, myGear[i]);
			}
		}
		
		//Frames
		
		lStatsPanel.setBorder(lBorder);
		rStatsPanel.setBorder(rBorder);
		lInnerPanel.add(lCombo, BorderLayout.NORTH);	lInnerPanel.add(lStatsPanel, BorderLayout.CENTER);
		rInnerPanel.add(rCombo, BorderLayout.NORTH);	rInnerPanel.add(rStatsPanel, BorderLayout.CENTER);
		cPanel.add(lInnerPanel);	cPanel.add(rInnerPanel);
		cPanel.setBorder(new EmptyBorder(new Insets(350, 2, 0, 2)));
		sPanel.add(sInnerPanel);
		
		add(lPanel, BorderLayout.WEST);
		add(rPanel, BorderLayout.EAST);
		add(cPanel, BorderLayout.CENTER);
		add(sPanel, BorderLayout.SOUTH);
		
		toolbar.add(newBtn);
		toolbar.add(itemsBtn);

		add(toolbar, BorderLayout.NORTH);
		
		newFrame.setLocation(300, 50);
		newFrame.setSize(new Dimension(500, 600));
		
		itemsFrame.setLocation(600, 150);
		itemsFrame.setSize(new Dimension(645, 460));
		
		chooserPanel.setLayout(new GridLayout(0, 1));
		pane.add(equipBtn);
		chooserFrame.add(chooserPanel);
		chooserFrame.setIconImage(new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB_PRE));
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize(new Dimension(450, 690));
		setLocation(300, 5);
		setTitle("WoW App - "+profile.getName());
		setVisible(true);
		
		//Listeners
		
		ActionListener comboAL = e -> {
			int index, n = 0;
			if(e.getSource().equals(lCombo)) {
				index = 0;
				lBorder.setTitle((String) lCombo.getSelectedItem());
			}
			else {
				index = 6;
				rBorder.setTitle((String) rCombo.getSelectedItem());
			}
			switch((String) ((JComboBox<String>) e.getSource()).getSelectedItem()) {
			case "Base Stats": {
				lBorder.setTitle("Base Stats");
				for(int i = index; i < index + 5; i++) {
					statLabels[i].setText(String.format("%-10s%8d", ATTRS[n] + ":", myStats[n]));
					n++;
				}
				statLabels[index + 5].setText(String.format("%-10s%8d", "Armor:", myArmor));
				break;
			}
			case "Melee": {
				statLabels[index++].setText(String.format("%-7s%11s", "Damage:", myMinDmg+"-"+myMaxDmg));
				String f;
				if(mySpeed2==0) f = String.format("%.2f", mySpeed1); else f = String.format("%.2f / %.2f", mySpeed1, mySpeed2);
				statLabels[index++].setText(String.format("%-6s%12s", "Speed:", f));
				statLabels[index++].setText(String.format("%-6s%12s", "Power:", myStats[0]*2 + myStats[1] + myStats[5]));
				statLabels[index++].setText(String.format("%-11s%7s", "Hit Rating:", myStats[11]));
				String c = String.format("%.2f", 100.*(1 - Math.exp((myStats[1] + myStats[10])/-2000.)));
				statLabels[index++].setText(String.format("%-12s%5s", "Crit Chance:", c) + "%");
				statLabels[index].setText(String.format("%-10s%8s", "Expertise:", myStats[16]));
				break;
			}
			case "Ranged": {
				break;
			}
			case "Spell": {
				break;
			}
			case "Defenses": {
				break;
			}
			}
		};
		lCombo.addActionListener(comboAL);
		rCombo.addActionListener(comboAL);
		lCombo.setSelectedIndex(0);
		rCombo.setSelectedIndex(0);
		
		ActionListener chooseAL = e -> {
			JButton b = (JButton) e.getSource();
			//If selected button gets pressed again, close window
			if(b.equals(selectedBtn) && chooserFrame.isVisible() == true) chooserFrame.setVisible(false);
			else { 
			selectedBtn = b;
			int noBtn = -1;
			for(int n = 0; n < SLOTS.length; n++)
				if (slotBtns[n].equals(b)) {
					noBtn = n;
					break;
				}
			Point p = b.getLocationOnScreen();
			chooserFrame.setLocation((int) p.getX() + 70, (int) p.getY() - 70);
			equipBtn.setText("Equip "+SLOTS[noBtn]);
			chooserPanel.removeAll();
			
			//Counting number of potential items for this slot
			int noItems = 0;
			for(Gear item : items) {
				if(item.getBodyPart().toString().equals(SLOTS[noBtn])) noItems++;
				//Allowing Two-Hand and One-Hand weapon choices
				else if(item instanceof Weapon) {
					if(noBtn == 16 || noBtn == 17) {
						if(item.getBodyPart().equals(BodyPart.TwoHand) || item.getBodyPart().equals(BodyPart.OneHand)) noItems++;
					}
				}
			}
			radioBtns = new JRadioButton[noItems + 1];
			grp.add(radioBtns[0] = new JRadioButton("None"));
			
			//Creating radiobuttons for items
			int index = 1;
			for(Gear item : items) {
				if(item.getBodyPart().toString().equals(SLOTS[noBtn])) radioBtns[index++] = new JRadioButton(item.getName());
				//Allowing Two-Hand and One-Hand weapon choices
				else if(item instanceof Weapon) {
					if(noBtn == 16 || noBtn == 17) {
						if(item.getBodyPart().equals(BodyPart.TwoHand) || item.getBodyPart().equals(BodyPart.OneHand)) radioBtns[index++] = new JRadioButton(item.getName());
					}
				}
			}
			//Adding radiobuttons to button group and panel
			for(JRadioButton btn : radioBtns) {
				chooserPanel.add(btn);
				grp.add(btn);
			}
			//Checking if theres already selected item in that slot
			if(myGear[noBtn] != null) {
				for(JRadioButton btn : radioBtns) {
					if(btn.getText().equals(myGear[noBtn].getName())) {
						btn.setSelected(true);
						break;
					}
				}
			} else radioBtns[0].setSelected(true);
			chooserPanel.add(pane);
			chooserFrame.pack();
			chooserFrame.setVisible(true);
			}
		},
		equipAL = l -> {
			int i = -1;
			String selected = "";
			//Find selected slot button index
			for(int n = 0; n < slotBtns.length; n++) 
				if(slotBtns[n].equals(selectedBtn)) {
					i = n;
					break;
			}
			//Find selected radio button name
			for(JRadioButton btn : radioBtns) if(btn.isSelected()) {
				selected = btn.getText();
				break;
			}
			//Is the selection different than before?
			boolean isDifferentItem = true;
			if(myGear[i] == null && selected.equals("None")) isDifferentItem = false;
			else if(myGear[i] != null && myGear[i].getName().equals(selected)) isDifferentItem = false;
			if(isDifferentItem) {
				//Search for selected item and add it
				Gear oldItem = myGear[i];
				Gear newItem = null;
				boolean found = false;
				for(Gear item : items) {
					if(item.getName().equals(selected)) {
						myGear[i] = item;
						slotBtns[i].setToolTipText(item.toString());
						slotBtns[i].setIcon(new ImageIcon(item.getIconPath()));
						newItem = item;
						found = true;
						break;
					}
				}
				refreshStats(oldItem, newItem);
				if(!found) {
					slotBtns[i].setToolTipText(SLOTS[i]);
					slotBtns[i].setIcon(null);
					myGear[i] = null;
				}
				lCombo.setSelectedIndex(lCombo.getSelectedIndex());
				rCombo.setSelectedIndex(rCombo.getSelectedIndex());	
			}
			chooserFrame.setVisible(false);
		};
		
		for(int i = 0; i < 19; i++) {
			slotBtns[i].addActionListener(chooseAL);
		}
		equipBtn.addActionListener(equipAL);
		
		itemsFrame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent we) {
				itemsPanel.refresh();
			}
		});
		
		newBtn.addActionListener(e -> {
			if(newPanel == null) newFrame.add(newPanel = new NewPanel(newFrame));
			if(newFrame.isVisible()) newFrame.setVisible(false);
			else newFrame.setVisible(true);
		});
		
		itemsBtn.addActionListener(e -> {
			if(itemsPanel == null) itemsFrame.add(itemsPanel = new ItemsPanel());
			if(itemsFrame.isVisible()) itemsFrame.setVisible(false);
			else itemsFrame.setVisible(true);
		});
	}
	
	private void refreshStats(Gear oldGear, Gear newGear) {
		if(oldGear != null && newGear != null) if(oldGear.equals(newGear)) return;
		
		//Removing old gear stats
		if(oldGear != null) {
			if(oldGear instanceof Weapon) {
				Weapon w = (Weapon) oldGear;
				if(w.getWeaponType().equals(WeaponType.Shield)) {
					//Block rating
					myStats[7] -= w.getBlock();
				}
				else {
					myMinDmg -= w.getMinDmg();
					myMaxDmg -= w.getMaxDmg();
					if(!w.getBodyPart().equals(BodyPart.Ranged)){
						//Speed
						if(myGear[16] != null && w.getName().equals(myGear[16].getName())) mySpeed1 = 0;
						else mySpeed2 = 0;
					}
				}
			}
			myArmor -= oldGear.getArmor();
			int[] stats = oldGear.getStats();
			for(int i = 0; i < 19; i++) myStats[i] -= stats[i];
		}
	
		//Adding new gear stats
		if(newGear != null) {
			if(newGear instanceof Weapon) {
				Weapon w = (Weapon) newGear;
				if(w.getWeaponType().equals(WeaponType.Shield)) {
					//Block rating
					myStats[7] += w.getBlock();
				}
				else {
					myMinDmg += w.getMinDmg();
					myMaxDmg += w.getMaxDmg();
					if(!w.getBodyPart().equals(BodyPart.Ranged)){
						//Speed
						if(myGear[16] != null && w.getName().equals(myGear[16].getName())) mySpeed1 = w.getSpeed();
						else mySpeed2 = w.getSpeed();
					}
				}
			}
			myArmor += newGear.getArmor();
			int[] stats = newGear.getStats();
			for(int i = 0; i < 19; i++) myStats[i] += stats[i];
		}
	}	
}
