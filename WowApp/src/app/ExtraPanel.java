package app;
import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import wowapp.model.GemColor;

public class ExtraPanel extends JPanel {
	
	JTextArea effectField = new JTextArea();
	JTextArea loreField = new JTextArea();
	JTextField amtField = new JTextField(),
			sellPriceField = new JTextField(),
			durabilityField = new JTextField(),
			reqLevelField = new JTextField();
	@SuppressWarnings("unchecked")
	private final JComboBox<GemColor>[] socketCombos = (JComboBox<GemColor>[]) new JComboBox[3];
	private final JComboBox<String> statCombo = new JComboBox<>(AppFrame.ATTRS);
	
	private JPanel np = new JPanel(new GridLayout(0, 4, 8, 8));
	private JPanel cp = new JPanel(new GridLayout(0, 1, 8, 8));
	private JPanel up = new JPanel();
	private JPanel dp = new JPanel();
	private JPanel sp = new JPanel(new GridLayout(1, 0, 8, 8));
	
	private static final long serialVersionUID = -6831913605374459892L;
	
	public ExtraPanel() {
		setLayout(new BorderLayout());
		effectField.setToolTipText("Start with * for 'Use:' version");
		for(int i = 0; i < 3; i++) {
			socketCombos[i] = new JComboBox<>(GemColor.values());
		}
		np.add(new JLabel(" Sockets:"));
		for(int i = 0; i < 3; i++) np.add(socketCombos[i]);
		np.add(new JLabel(" Socket Bonus stat:"));
		np.add(statCombo);
		np.add(new JLabel(" Amount:"));
		np.add(amtField);
		
		up.setBorder(BorderFactory.createTitledBorder(null,
	            "Effect", TitledBorder.LEADING,
	            TitledBorder.TOP));
		dp.setBorder(BorderFactory.createTitledBorder(null,
	            "Lore", TitledBorder.LEADING,
	            TitledBorder.TOP));
		up.setLayout(new GridLayout());
		dp.setLayout(new GridLayout());
		up.add(effectField);
		dp.add(loreField);
		cp.add(up);
		cp.add(dp);
		sp.add(new JLabel(" Sell price:"));
		sp.add(sellPriceField);
		sp.add(new JLabel("Durability:"));
		sp.add(durabilityField);
		sp.add(new JLabel("Min Level:"));
		sp.add(reqLevelField);
		add(np, BorderLayout.NORTH);
		add(cp, BorderLayout.CENTER);
		add(sp, BorderLayout.SOUTH);
	}
	public void resetSockets() {
		for(int i = 0; i < 3; i++) socketCombos[i].setSelectedIndex(0);
		statCombo.setSelectedIndex(0);
	}
	public GemColor[] getSockets() {
		GemColor[] sockets = {(GemColor) socketCombos[0].getSelectedItem(), (GemColor) socketCombos[1].getSelectedItem(), (GemColor) socketCombos[2].getSelectedItem()};
		return sockets;
	}
	public int getBonusStatID() {
		return statCombo.getSelectedIndex();
	}
}
