package app;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingWorker;

public class IconLoader extends SwingWorker<Void, JButton>{
	private String folder;
	private int i;
	private ActionListener l;
	private JPanel[] innerPanel;
	public IconLoader(String folder, int i, ActionListener l, JPanel[] innerPanel) {
		this.folder = folder;
		this.i = i;
		this.l = l;
		this.innerPanel = innerPanel;
	}
	@Override
	protected Void doInBackground() throws Exception {
		try {
		Files.walkFileTree(Path.of(System.getProperty("user.dir")+"/icons/"+folder), new SimpleFileVisitor<Path>() {
			@Override
			public FileVisitResult visitFile(Path path, BasicFileAttributes a) {
				JButton b = new JButton(new ImageIcon(path.toString()));
				b.setActionCommand(path.toString());
				b.setPreferredSize(new Dimension(50, 50));
				b.addActionListener(l);
				publish(b);
				return FileVisitResult.CONTINUE;
			}
		});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Override
	protected void process(List<JButton> chunks) {
		for(JButton b : chunks)
			innerPanel[i].add(b);
	}
}
