package app;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.Set;
import java.util.TreeSet;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;

import wowapp.model.Gear;

public class ItemsPanel extends JPanel {
	
	Set<Gear> items = new TreeSet<>((a, b) -> {
		return a.getName().toLowerCase().compareTo(b.getName().toLowerCase());
	});
	DefaultListModel<String> dataModel = new DefaultListModel<>();
	JList<String> itemList = new JList<>(dataModel);

	JTabbedPane tabPane = new JTabbedPane();
	
	JSplitPane splitPane;
	JScrollPane scrollPane1 = new JScrollPane(itemList);
	
	JPanel innerPanel = new JPanel(),
			outerPanel = new JPanel();
	JScrollPane scrollPane2 = new JScrollPane(outerPanel);
	
	JPanel sp = new JPanel(),
			np = new JPanel(),
			iconTabsPanel = new JPanel();
	
	JTextPane textPane = new JTextPane();
	JButton selected = new JButton();
	JButton deleteBtn = new JButton("Delete Selected Item");
	ActionListener l;
	
	private static final long serialVersionUID = 1L;
	
	public ItemsPanel() {
		setLayout(new BorderLayout());
		
		iconTabsPanel.setLayout(new BorderLayout());
		np.setLayout(new FlowLayout(FlowLayout.LEFT));
		sp.setLayout(new FlowLayout(FlowLayout.LEFT));		
		iconTabsPanel.add(np, BorderLayout.NORTH);
		iconTabsPanel.add(tabPane, BorderLayout.CENTER);
		textPane.setPreferredSize(new Dimension(330, 380));
		textPane.setBackground(Color.BLACK);
		textPane.putClientProperty(JEditorPane.HONOR_DISPLAY_PROPERTIES, true);
		textPane.setEditable(false);
		textPane.setContentType("text/html");
		
		itemList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, iconTabsPanel, textPane);
		splitPane.setDividerLocation(283);
		tabPane.addTab("Browse by Name", scrollPane1);
		
		outerPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		innerPanel.setLayout(new GridLayout(0, 4, 8, 8));
		outerPanel.add(innerPanel);
		tabPane.addTab("Browse by Icon", scrollPane2);
		scrollPane2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		
		selected.setPreferredSize(new Dimension(65, 65));
		np.add(selected);
		sp.add(deleteBtn);
		add(splitPane, BorderLayout.CENTER);
		add(sp, BorderLayout.SOUTH);
		
		//Listeners
		
		l = e -> {
			String name = e.getActionCommand();
			for(Gear i : items)
				if(i.getName().equals(name)) {
					itemList.setSelectedValue((String) i.getName(), true);
					break;
			}
		};
		
		itemList.addListSelectionListener(e -> {
			if(!itemList.isSelectionEmpty()) {
				String name = itemList.getSelectedValue();
				for(Gear item : items) {
					if(name.equals(item.getName())) {
						textPane.setText(""+item);
						selected.setIcon(item.getIcon());
						break;
					}
				}
			}
		});
		
		deleteBtn.addActionListener(e -> {
			for(Gear i : items) {
				if(i.getName().equals(itemList.getSelectedValue())) {
					int selected = itemList.getSelectedIndex();
					AppFrame.items.remove(i);
					items.remove(i);
					refresh();
					textPane.setText("");
					if(!items.isEmpty()) {
						if(selected > 1)
							itemList.setSelectedIndex(selected - 1);
						else {
							itemList.clearSelection();
							itemList.setSelectedIndex(0);
						}
					}
					for(Component c : innerPanel.getComponents()) {
						if(c instanceof JButton) {
							JButton b = (JButton) c;
							if(b.getActionCommand().equals(i.getName())) {
								innerPanel.remove(c);
								innerPanel.revalidate();
								break;
							}
						}
					}
					break;
				}
			}
		});
	}
		
	public void refresh() {
		for(Gear i : AppFrame.items) {
			if(this.items.add(i)) {
				JButton b = new JButton(i.getIcon());
				b.setPreferredSize(new Dimension(60, 60));
				b.setActionCommand(i.getName());
				b.addActionListener(l);
				innerPanel.add(b);
			}
		}
		int i = 0;
		for(Gear item : this.items) dataModel.add(i++, item.getName());
		if(!dataModel.isEmpty()) {
			for(i = dataModel.size(); i > AppFrame.items.size(); i--) dataModel.remove(i - 1);
		}
		else itemList.setSelectedIndex(0);
	}
}
