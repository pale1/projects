package app;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.imageio.IIOException;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import wowapp.model.Armor;
import wowapp.model.ArmorType;
import wowapp.model.BodyPart;
import wowapp.model.GemColor;
import wowapp.model.Quality;
import wowapp.model.Weapon;
import wowapp.model.WeaponType;

public class NewPanel extends JPanel {
	
	private final JComboBox<Quality> qualityCombo = new JComboBox<>(Quality.values());
	private final JComboBox<BodyPart> bodypartsCombo = new JComboBox<>(BodyPart.values());
	private final JComboBox<ArmorType> armortypesCombo = new JComboBox<>(ArmorType.values());
	private final JComboBox<WeaponType> weapontypesCombo = new JComboBox<>(WeaponType.values());
	
	private JTextField nameField = new JTextField();
	private JTextField armorField = new JTextField();
	private JTextField minDmgField = new JTextField();
	private JTextField maxDmgField = new JTextField();
	private JTextField speedField = new JTextField();
	private JTextField blockField = new JTextField();
	private JTextField iLvlField = new JTextField();
	private JTextField[] statFields = new JTextField[AppFrame.ATTRNO];
	
	private JButton textureBtn = new JButton("Add Icon");
	private JButton extraBtn = new JButton("Extra");
	private JButton createBtn = new JButton("Create Item");
	private JRadioButton armorBtn = new JRadioButton("Armor");
	private JRadioButton weaponBtn = new JRadioButton("Weapon");
	private ButtonGroup grp = new ButtonGroup();
	private JPanel np = new JPanel(new BorderLayout());
	private JPanel wp = new JPanel(new GridLayout(0, 2, 5, 5));
	private JPanel ep = new JPanel(new GridLayout(0, 2, 5, 5));
	private JPanel sp = new JPanel();
	private JPanel nep = new JPanel();
	private JPanel pane = new JPanel();
	
	private JFrame texturesFrame = new JFrame("Add icon");
	private TexturesPanel texturesPanel;
	private JFrame extraFrame = new JFrame("Add lore/effect");
	private ExtraPanel extraPanel;
	
	private static final long serialVersionUID = 1L;

	public NewPanel(JFrame newFrame) {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		pane.setLayout(new BorderLayout());
		grp.add(armorBtn);
		grp.add(weaponBtn);
		ep.setBorder(BorderFactory.createTitledBorder(null,
	            "Off-Stats", TitledBorder.LEADING,
	            TitledBorder.TOP));
		extraFrame.setLocation(500, 300);
		extraFrame.setSize(new Dimension(500, 300));
		extraFrame.add(extraPanel = new ExtraPanel());
		texturesFrame.setLocation(500, 100);
		texturesFrame.setSize(new Dimension(520, 550));
		texturesFrame.add(texturesPanel = new TexturesPanel(texturesFrame));

		
		iLvlField.setText("1");
		extraPanel.durabilityField.setText("60");
		extraPanel.reqLevelField.setText("1");
		nameField.setToolTipText("Start with * for Heroic version");
		nameField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 16));
		
		
		//Fields
		
		np.add(new JLabel("       Item Name:  "), BorderLayout.WEST);
		np.add(nameField, BorderLayout.CENTER);
		nep.add(textureBtn);
		nep.add(extraBtn);
		np.add(nep, BorderLayout.EAST);

		wp.add(armorBtn);
		wp.add(weaponBtn);
		
		wp.add(new JLabel("    Quality:"));
		wp.add(qualityCombo);
		
		wp.add(new JLabel("    Body Part:"));
		wp.add(bodypartsCombo);
		
		wp.add(new JLabel("    Armor Type:"));
		wp.add(armortypesCombo);
		
		wp.add(new JLabel("    Weapon Type:"));
		wp.add(weapontypesCombo);
		
		wp.add(new JLabel("    Armor:"));
		wp.add(armorField);
		
		wp.add(new JLabel("    Min Damage:"));
		wp.add(minDmgField);
		
		wp.add(new JLabel("    Max Damage:"));
		wp.add(maxDmgField);
		
		wp.add(new JLabel("    Weapon Speed:"));
		wp.add(speedField);
		
		wp.add(new JLabel("    Block:"));
		wp.add(blockField);
		
		wp.add(new JLabel("    Item Level:"));
		wp.add(iLvlField);
		
		//Attribute fields
		
		for(int i = 0; i < AppFrame.ATTRNO; i++)
			statFields[i] = new JTextField();
				
		for(int i = 0; i < 5; i++) {
			wp.add(new JLabel("    " + AppFrame.ATTRS[i] + ":"));
			wp.add(statFields[i]);
		}
		
		for(int i = 5; i < AppFrame.ATTRNO; i++) {
			ep.add(new JLabel("    " + AppFrame.ATTRS[i] + ":"));
			ep.add(statFields[i]);
		}
		
		sp.add(createBtn);
		pane.add(np, BorderLayout.NORTH);
		pane.add(wp, BorderLayout.WEST);
		pane.add(ep, BorderLayout.EAST);
		pane.add(sp, BorderLayout.SOUTH);
		add(pane);
		
		
		
		//Listeners
		
		armorBtn.setSelected(true);
		minDmgField.setEditable(false);
		maxDmgField.setEditable(false);
		speedField.setEditable(false);
		weapontypesCombo.setEnabled(false);
		blockField.setEditable(false);
		for(int i = 18; i >= 14; i--)
			bodypartsCombo.removeItemAt(i);

		BodyPart[] bp = BodyPart.values();
		
		armorBtn.addItemListener(e -> {
			if(armorBtn.isSelected()) {
				armortypesCombo.setEnabled(true);
				weapontypesCombo.setEnabled(false);
				weapontypesCombo.setSelectedIndex(0);
				minDmgField.setText("");
				minDmgField.setEditable(false);
				maxDmgField.setText("");
				maxDmgField.setEditable(false);
				speedField.setText("");
				speedField.setEditable(false);
				//Removes items that aren't armor
				for(int i = 0; i < 14; i++) bodypartsCombo.addItem(bp[i]);
				bodypartsCombo.setSelectedIndex(5);
				for(int i = 4; i >= 0; i--) bodypartsCombo.removeItemAt(i);
			}
			else {
				weapontypesCombo.setEnabled(true);
				armortypesCombo.setEnabled(false);
				armortypesCombo.setSelectedIndex(0);
				minDmgField.setEditable(true);
				maxDmgField.setEditable(true);
				speedField.setEditable(true);
				//Removes items that aren't weapon
				for(int i = 14; i < 19; i++) bodypartsCombo.addItem(bp[i]);
				bodypartsCombo.setSelectedIndex(14);
				for(int i = 13; i >= 0; i--) bodypartsCombo.removeItemAt(i);
			}
		});
		
		bodypartsCombo.addActionListener(e -> {
			BodyPart bodypart = (BodyPart) bodypartsCombo.getSelectedItem();
			if(bodypart == BodyPart.Neck || bodypart == BodyPart.Shirt || bodypart == BodyPart.Tabard || bodypart == BodyPart.Finger || bodypart == BodyPart.Trinket) {
				armortypesCombo.setEnabled(false);
				armortypesCombo.setSelectedIndex(0);
			} else {
				if(!armorBtn.isEnabled()) armortypesCombo.setEnabled(true);
			}
		});
		
		weapontypesCombo.addActionListener(e -> {
			WeaponType type = (WeaponType) weapontypesCombo.getSelectedItem();
			if(type.equals(WeaponType.Shield)) {
				blockField.setEditable(true);
				minDmgField.setEditable(false);
				minDmgField.setText("");
				maxDmgField.setEditable(false);
				maxDmgField.setText("");
				speedField.setEditable(false);
				speedField.setText("");
				
				bodypartsCombo.setEnabled(false);
				bodypartsCombo.setSelectedIndex(1);
			} else {
				minDmgField.setEditable(true);
				maxDmgField.setEditable(true);
				speedField.setEditable(true);
				blockField.setEditable(false);
				blockField.setText("");
				
				bodypartsCombo.setEnabled(true);
			}
		});
		
		createBtn.setEnabled(false);
		
		nameField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				check();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				check();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				check();
			}
			public void check() {
				if(nameField.getText().isEmpty()) {
					createBtn.setEnabled(false);
				}
				else createBtn.setEnabled(true);
			}
		});
		
		extraBtn.addActionListener(e -> {
			if(extraFrame.isVisible()) extraFrame.setVisible(false);
			else extraFrame.setVisible(true);
		});
		
		textureBtn.addActionListener(e -> {
			if(texturesFrame.isVisible()) texturesFrame.setVisible(false);
			else texturesFrame.setVisible(true);
		});
		
		//Creates Item
		
		createBtn.addActionListener(e -> {
			String name = nameField.getText();
			try {
				int armor = 0;
				int minDmg = 0;
				int maxDmg = 0;
				double speed = 0;
				int block = 0;
				int iLvl = 1;
				int[] stats = new int[AppFrame.ATTRNO];
				String effect = extraPanel.effectField.getText().strip();
				String lore = extraPanel.loreField.getText().strip();
				int amt = 0;
				int sellPrice = 0;
				int durability = 60;
				int reqLevel = 1;
				String iconPath = texturesPanel == null ? TexturesPanel.DEFICONPATH : texturesPanel.getIconPath();
				GemColor[] sockets, rawSockets = extraPanel.getSockets();
				int num = 0;
				for(int i = 0; i < 3; i++) {
					if(!rawSockets[i].equals(GemColor.None)) num++;
				}
				sockets = new GemColor[num]; num = 0;
				for(int i = 0; i < 3; i++) {
					if(!rawSockets[i].equals(GemColor.None)) {
						sockets[num] = rawSockets[i];
						num++;
					}
				}
				
				
				if(!armorField.getText().isEmpty()) armor = Integer.parseInt(armorField.getText());
				if(!minDmgField.getText().isEmpty()) minDmg = Integer.parseInt(minDmgField.getText());
				if(!maxDmgField.getText().isEmpty()) maxDmg = Integer.parseInt(maxDmgField.getText());
				if(!speedField.getText().isEmpty()) speed = Double.parseDouble(speedField.getText());
				if(!blockField.getText().isEmpty()) block = Integer.parseInt(blockField.getText());
				if(!iLvlField.getText().isEmpty()) iLvl = Integer.parseInt(iLvlField.getText());
				if(!extraPanel.amtField.getText().isEmpty()) amt = Integer.parseInt(extraPanel.amtField.getText());
				if(!extraPanel.sellPriceField.getText().isEmpty()) sellPrice = Integer.parseInt(extraPanel.sellPriceField.getText());
				if(!extraPanel.durabilityField.getText().isEmpty()) durability = Integer.parseInt(extraPanel.durabilityField.getText());
				if(!extraPanel.reqLevelField.getText().isEmpty()) reqLevel = Integer.parseInt(extraPanel.reqLevelField.getText());
				for(int i = 0; i < AppFrame.ATTRNO; i++) {
					if(statFields[i].getText().isEmpty()) stats[i] = 0;
					else if (Integer.parseInt(statFields[i].getText()) != 0) stats[i] = Integer.parseInt(statFields[i].getText());
				}
				
				if(armor < 0 || minDmg < 0 || maxDmg < 0 || speed < 0 || block < 0 || iLvl <= 0 || amt < 0 || sellPrice < 0 || durability <= 0 || reqLevel <= 0)
					throw new IllegalArgumentException();
				
				if(armorBtn.isSelected()) {	
					Boolean b = AppFrame.items.add(new Armor(name.strip(), iconPath, (BodyPart) bodypartsCombo.getSelectedItem(), (Quality) qualityCombo.getSelectedItem(), sellPrice, armor, sockets, extraPanel.getBonusStatID(), amt, stats,
						effect, lore, durability, reqLevel, iLvl, (ArmorType) armortypesCombo.getSelectedItem()));
					if(!b) throw new IIOException("Item of the same name already exists!");
				}
				if(weaponBtn.isSelected()) {
					if(weapontypesCombo.getSelectedItem() != WeaponType.Shield && (minDmg > maxDmg || maxDmg <= 0 || speed <= 0 || minDmg < 0))
						throw new IIOException("Invalid arguments: weapon damage/speed must make sense(unless shield)");
					Boolean b = AppFrame.items.add(new Weapon(name.strip(), iconPath, (BodyPart) bodypartsCombo.getSelectedItem(), (Quality) qualityCombo.getSelectedItem(), sellPrice, armor, sockets, extraPanel.getBonusStatID(), amt, stats,
						effect, lore, durability, reqLevel, iLvl, minDmg, maxDmg, speed, block, (WeaponType) weapontypesCombo.getSelectedItem()));
					if(!b) throw new IIOException("Item of the same name already exists!");
				}
				newFrame.setVisible(false);
				if(extraFrame.isVisible()) extraFrame.setVisible(false);
				if(texturesFrame.isVisible()) texturesFrame.setVisible(false);
				nameField.setText("");
				armorField.setText("");
				extraPanel.effectField.setText("");
				extraPanel.loreField.setText("");
				extraPanel.durabilityField.setText("60");
				extraPanel.resetSockets();
				if(texturesPanel != null) texturesPanel.setIcon(TexturesPanel.DEFICONPATH);
				for(int i = 0; i < AppFrame.ATTRNO; i++) statFields[i].setText("");
				armorBtn.setSelected(true);
			} catch(NumberFormatException nfe) {
				JOptionPane.showMessageDialog(new JFrame(), "Speed must be double, everything else must be integer!",
						"Cannot create item", JOptionPane.ERROR_MESSAGE);
			} catch(IllegalArgumentException iae) {
				JOptionPane.showMessageDialog(new JFrame(), "Only stats can have negative values",
						"Cannot create item", JOptionPane.ERROR_MESSAGE);
			} catch(IIOException iio) {
				JOptionPane.showMessageDialog(new JFrame(), iio.getMessage(),
						"Cannot create item", JOptionPane.ERROR_MESSAGE);
			}
		});
	}
}