package app;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;

public class TexturesPanel extends JPanel {
	
	private static final String[] TABS = {"Armor", "Weapon", "Misc"};
	
	public static final String DEFICONPATH = System.getProperty("user.dir")+"/icons/Armor/78.png";
	public static final ImageIcon DEFICON = new ImageIcon(DEFICONPATH);
	
	JButton iconBtn = new JButton();
	private JPanel np = new JPanel();
	private JPanel[] innerPanel = new JPanel[3], outerPanel = new JPanel[3];
	private JScrollPane[] sp = new JScrollPane[3];
	private JPanel pane = new JPanel();
	
	private JTabbedPane cp = new JTabbedPane();
	
	private static final long serialVersionUID = 1L;

	public TexturesPanel(JFrame texturesFrame) {
		setLayout(new FlowLayout(FlowLayout.LEFT));
		np.setLayout(new FlowLayout(FlowLayout.LEFT));
		pane.setLayout(new BorderLayout());
		ActionListener l = e -> {
			iconBtn.setIcon(((JButton) e.getSource()).getIcon());
			iconBtn.setActionCommand(((JButton) e.getSource()).getActionCommand());
		};
		for(int i = 0; i < 3; i++) {
			innerPanel[i] = new JPanel(new GridLayout(0, 8, 8, 8));
			outerPanel[i] = new JPanel(new FlowLayout(FlowLayout.LEFT));
			outerPanel[i].add(innerPanel[i]);
			sp[i] = new JScrollPane(outerPanel[i]);
			sp[i].setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			sp[i].setPreferredSize(new Dimension(490, 400));
			sp[i].getVerticalScrollBar().setUnitIncrement(15);
			IconLoader il = new IconLoader(TABS[i], i, l, innerPanel);
			il.execute();
			cp.addTab(TABS[i], sp[i]);
		}
		
		iconBtn.addActionListener(e -> {
			texturesFrame.setVisible(false);
		});
		
		iconBtn.setPreferredSize(new Dimension(65, 65));
		iconBtn.setIcon(DEFICON);
		np.add(iconBtn);
		pane.add(np, BorderLayout.NORTH);
		pane.add(cp, BorderLayout.CENTER);
		add(pane);
	}
	public void setIcon(String iconPath) {
		iconBtn.setIcon(DEFICON);
	}
	public String getIconPath() {
		return iconBtn.getActionCommand();
	}
}
