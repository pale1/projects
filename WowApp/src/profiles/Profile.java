package profiles;

import java.io.Serializable;

import wowapp.model.Class;
import wowapp.model.Gear;
import wowapp.model.Race;

public class Profile implements Serializable {
	
	private static final long serialVersionUID = -2769196228552576680L;
	
	private String name;
	private int level;
	private Race myRace;
	private Class myClass;
	private boolean myGender;
	private Gear[] myGear = new Gear[19];
	
	public Profile(String name, int level, Race myRace, Class myClass, boolean myGender) {
		this.name = name;
		this.level = level;
		this.myRace = myRace;
		this.myClass = myClass;
		this.myGender = myGender;
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof Profile) {
			return name.strip().equals(((Profile) o).getName().strip());
		}
		return false;
	}
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	public String toString() {
		return name;
	}
	public String getName() {
		return name;
	}
	public int getLevel() {
		return level;
	}
	public Race getMyRace() {
		return myRace;
	}
	public Class getMyClass() {
		return myClass;
	}
	public boolean getGender() {
		return myGender;
	}
	public Gear[] getGear() {
		return myGear;
	}
}
