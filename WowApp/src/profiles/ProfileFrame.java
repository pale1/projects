package profiles;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.IIOException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import app.AppFrame;
import wowapp.model.Class;
import wowapp.model.Race;

public class ProfileFrame extends JFrame {
	
	private static final long serialVersionUID = 1358463305827007671L;
	
	public static File profilesFile = new File(System.getProperty("user.dir")+"/profiles.ser");
	public static Set<Profile> profiles = new HashSet<>();
	
	AppFrame appFrame;
	
	private JButton chooseBtn = new JButton("Choose Profile"),
					createBtn = new JButton("Create new Profile"),
					deleteBtn = new JButton("Delete Profile");
	private JComboBox<Profile> profileCombo = new JComboBox<>();
	
	JFrame createProfile = new JFrame("Create Profile");
	JPanel cp = new JPanel(),
			sp = new JPanel();
	JTextField nameField = new JTextField();
	JTextField levelField = new JTextField();
	JComboBox<Race> raceCombo = new JComboBox<>(Race.values());
	JComboBox<Class> classCombo = new JComboBox<>(Class.values());
	JComboBox<String> genderCombo = new JComboBox<>();
	JButton create = new JButton("Create");

	@SuppressWarnings("unchecked")
	public ProfileFrame() {
		setLayout(new GridLayout(0, 2, 8, 8));
		add(profileCombo);
		add(chooseBtn);
		add(new JLabel());
		add(createBtn);
		add(new JLabel());
		add(deleteBtn);

		genderCombo.addItem("Male");
		genderCombo.addItem("Female");
		
		createProfile.setLayout(new BorderLayout());
		cp.setLayout(new GridLayout(0, 2, 4, 4));
		cp.add(new JLabel(" Name:"));
		cp.add(nameField);
		cp.add(new JLabel(" Level:"));
		cp.add(levelField);
		cp.add(new JLabel(" Race:"));
		cp.add(raceCombo);
		cp.add(new JLabel(" Class:"));
		cp.add(classCombo);
		cp.add(new JLabel(" Gender:"));
		cp.add(genderCombo);
		sp.add(create);
		create.setEnabled(false);
		createProfile.add(cp, BorderLayout.CENTER);
		createProfile.add(sp, BorderLayout.SOUTH);
		createProfile.setLocation(400, 300);
		createProfile.setSize(new Dimension(200, 200));
		
		//Import
		
		try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(profilesFile))) {
			profiles = (Set<Profile>) ois.readObject();
		} catch (FileNotFoundException e1) {
			System.out.println("Profiles file not found!");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {}
		
		//Export
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent we) {
				onClose();
			}
		});
		
		//Add profiles to combo
		for(Profile p : profiles) profileCombo.addItem(p);
		
		if(profileCombo.getItemCount()==0) chooseBtn.setEnabled(false);
		
		//Listeners
		
		chooseBtn.addActionListener(l -> {
			String selected = profileCombo.getSelectedItem().toString();
			for(Profile p : profiles) {
				if(selected.equals(p.getName())) {
					appFrame = new AppFrame(p);
					setVisible(false);
					break;
				}
			}
		});
		
		createBtn.addActionListener(l -> {
			if(createProfile.isVisible()) createProfile.setVisible(false);
			else createProfile.setVisible(true);
		});
		
		create.addActionListener(l -> {
			try {
				String name = nameField.getText();
				int level = Integer.parseInt(levelField.getText());
				if(level < 1) throw new IllegalArgumentException();
				boolean gender = genderCombo.getSelectedIndex()==0? true : false;
				Profile newProfile;
				boolean b = profiles.add(newProfile = new Profile(name.strip(), level, (Race) raceCombo.getSelectedItem(), (Class) classCombo.getSelectedItem(), gender));
				for(Profile p : profiles) System.out.print(p.getName());
				if(!b) throw new IIOException("Profile of the same name already exists!");
				profileCombo.addItem(newProfile);
				profileCombo.setSelectedIndex(0);
				if(!chooseBtn.isEnabled()) chooseBtn.setEnabled(true);
				createProfile.setVisible(false);
				nameField.setText("");
				levelField.setText("");
				raceCombo.setSelectedIndex(0);
				classCombo.setSelectedIndex(0);
				genderCombo.setSelectedIndex(0);
			} catch(NumberFormatException nfe) {
				JOptionPane.showMessageDialog(new JFrame(), "Level must be integer!",
					"Cannot create profile", JOptionPane.ERROR_MESSAGE);
			} catch(IllegalArgumentException iae) {
				JOptionPane.showMessageDialog(new JFrame(), "Level must be positive!",
					"Cannot create profile", JOptionPane.ERROR_MESSAGE);
			} catch (IIOException iioe) {
				JOptionPane.showMessageDialog(new JFrame(), iioe.getMessage(),
						"Cannot create profile", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		deleteBtn.addActionListener(l -> {
			if(profileCombo.getSelectedIndex()>=0) {
				String selected = profileCombo.getSelectedItem().toString();
				for(Profile p : profiles) {
					if(p.getName().equals(selected)) {
						profiles.remove(p);
						int index = profileCombo.getSelectedIndex();
						profileCombo.setSelectedIndex(-1);
						profileCombo.removeItemAt(index);
						if(profileCombo.getItemCount()==0) chooseBtn.setEnabled(false);
						else {
							chooseBtn.setEnabled(true);
							profileCombo.setSelectedIndex(0);
						}
						break;
					}
				}
			}
		});
		
		nameField.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void insertUpdate(DocumentEvent e) {
				check();
			}
			@Override
			public void removeUpdate(DocumentEvent e) {
				check();
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				check();
			}
			private void check() {
				if(nameField.getText().isEmpty()) {
					create.setEnabled(false);
				}
				else create.setEnabled(true);
			}
		});
	}
	public static void onClose() {
		try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(profilesFile))) {
			
			if(profilesFile.exists()) profilesFile.delete();
			profilesFile.createNewFile();
			if(!profiles.isEmpty()) oos.writeObject(profiles);
			
		} catch (FileNotFoundException e) {}
		catch (IOException e) {}
	}
	public static void main(String[] args) throws InvocationTargetException, InterruptedException {
		SwingUtilities.invokeAndWait(() -> {
			ProfileFrame profileFrame = new ProfileFrame();
			profileFrame.setTitle("Select Profile");
			profileFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
			profileFrame.setLocation(350, 200);
			profileFrame.setVisible(true);
			profileFrame.pack();
		});
	}
}
