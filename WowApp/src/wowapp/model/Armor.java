package wowapp.model;

public class Armor extends Gear {

	private static final long serialVersionUID = 1L;
	private final ArmorType armorType;
	public Armor(String name, String iconPath, BodyPart bodypart, Quality quality, int sellPrice, int armor, GemColor[] sockets, int socketBonusStat, int socketBonusAmount, int[] stats, String effect, String lore, int durability, int reqLevel, int iLvl, ArmorType armorType) {
		super(name, iconPath, bodypart, quality, sellPrice, armor, sockets, socketBonusStat, socketBonusAmount, stats, effect, lore, durability, reqLevel, iLvl);
		this.armorType = armorType;
	}
	@Override
	public String toString() {
		return super.toString();
	}
	public ArmorType getArmorType() {
		return armorType;
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof Armor)
			return this.getName().equals(((Armor) o).getName());
		return false;
	}
	@Override
	public int hashCode() {
		return name.hashCode();
	}

}
