package wowapp.model;

public enum ArmorType {
	Cloth, Leather, Mail, Plate
}
