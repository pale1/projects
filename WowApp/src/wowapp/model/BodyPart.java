package wowapp.model;

public enum BodyPart {
	Head, Neck, Shoulder, Back, Chest, Shirt, Tabard, Wrist,
	Hands, Waist, Legs, Feet, Finger, Trinket,
	MainHand, OffHand,  Ranged, TwoHand, OneHand
}
