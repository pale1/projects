package wowapp.model;

import app.AppFrame;

public abstract class Gear extends Item {
	
	private static final long serialVersionUID = 1L;
	
	protected final BodyPart bodypart;
	protected final int armor;
	protected final GemColor[] sockets;
	protected final int socketBonusStat, socketBonusAmount;
	protected final int[] stats = new int[AppFrame.ATTRNO];
	protected final String effect;
	protected final String lore;
	protected final int durability;
	protected final int reqLevel;
	protected final int iLvl;

	public Gear(String name, String iconPath, BodyPart bodypart, Quality quality, int sellPrice, int armor, GemColor[] sockets, int socketBonusStat, int socketBonusAmount, int[] stats, String effect, String lore, int durability, int reqLevel, int iLvl) {
		super(name, iconPath, quality, sellPrice);
		this.bodypart = bodypart;
		this.armor = armor;
		this.sockets = sockets;
		this.socketBonusStat = socketBonusStat;
		this.socketBonusAmount = socketBonusAmount;
		for(int i = 0; i < AppFrame.ATTRNO; i++) this.stats[i] = stats[i];
		this.effect = effect;
		this.lore = lore;
		this.durability = durability;
		this.reqLevel = reqLevel;
		this.iLvl = iLvl;
	}
	
	@Override
	public abstract boolean equals(Object o);
	@Override
	public abstract int hashCode();
	
	/**
	 * Converts item information into a HTML string
	 * @return String in HTML format
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(80);
		sb.append("<html><b>");
		Quality q = getQuality();
		switch(q) {
		case Poor: sb.append("<font color=gray>"); break;
		case Uncommon: sb.append("<font color=#12ff30>"); break;
		case Rare: sb.append("<font color=#585cfe>"); break;
		case Epic: sb.append("<font color=#b93ce6>"); break;
		case Legendary: sb.append("<font color=#fa6e00>"); break;
		default: break;
		}
		String name = getName();
		if(name.charAt(0) == '*') {
			sb.append(name.substring(1) + "<br>");
			sb.append("<font color=#12ff30>Heroic<font color=white><br>");
		} else {
			sb.append(name + "<font color=white><br>");
		}
		if(this instanceof Armor) {
			Armor a = (Armor) this;
			BodyPart bp = (BodyPart) getBodyPart();
			if(bp == BodyPart.Neck || bp == BodyPart.Shirt || bp == BodyPart.Tabard || bp == BodyPart.Finger || bp == BodyPart.Trinket)
				sb.append(a.getBodyPart() + "<br>");
			else sb.append(String.format("%-35s%35s    <br>", a.getBodyPart(), a.getArmorType()).replace(" ", "&nbsp;"));
			if(getArmor() > 0) sb.append(getArmor() + " Armor<br>");
		} else if(this instanceof Weapon) {
			Weapon w = (Weapon) this;
			BodyPart bp = w.getBodyPart();
			switch(bp) {
			case TwoHand: sb.append(String.format("%-20s", "Two-Hand").replace(" ", "&nbsp;")); break;
			case MainHand: sb.append(String.format("%-20s", "Main Hand").replace(" ", "&nbsp;")); break;
			case OffHand: sb.append(String.format("%-20s", "Off Hand").replace(" ", "&nbsp;")); break;
			case OneHand: sb.append(String.format("%-20s", "One-Hand").replace(" ", "&nbsp;")); break;
			case Ranged: sb.append(String.format("%-20s", "Ranged").replace(" ", "&nbsp;")); break;
			default: break;
			}
			if(w.getWeaponType() == WeaponType.Shield) {
				sb.append(String.format("%45s<br>", "Shield").replace(" ", "&nbsp;"));
				sb.append(getArmor() + " Armor<br>");
				if(w.getBlock() > 0) sb.append(w.getBlock() + " Block<br>");
			} else {
				sb.append(String.format( "%45s<br>", w.getWeaponType()).replace(" ", "&nbsp;"));
				sb.append(String.format("%-50sSpeed %.1f<br>", w.getMinDmg() + " - " + w.getMaxDmg() + " Damage", w.getSpeed()).replace(" ", "&nbsp;"));
				sb.append(String.format("(%.1f damage per second)<br>", (w.getMinDmg() + w.getMaxDmg()) / (2 * w.getSpeed())));
				if(w.getArmor() > 0) sb.append(w.getArmor() + " Armor<br>");
			}
		}
		for(int i = 0; i < 5; i++)
			if(stats[i] != 0) sb.append("+" + stats[i] + " " + AppFrame.ATTRS[i] + "<br>");
		for(int i = 0; i < sockets.length; i++) {
			switch(sockets[i]) {
			case Red: {
				sb.append("<font color=#c11a1a>[ ] <font color=gray>Red Socket<br>");
				break;
			}
			case Blue: {
				sb.append("<font color=#305fcd>[ ] <font color=gray>Blue Socket<br>");
				break;
			}
			case Yellow: {
				sb.append("<font color=#dcc846>[ ] <font color=gray>Yellow Socket<br>");
				break;
			}
			default: break;
			}
		}
		if(hasSockets()) sb.append("Socket Bonus: +"+socketBonusAmount+" "+AppFrame.ATTRS[socketBonusStat]+"<br>");
		sb.append("<font color=white>Durability "+durability+" / "+durability+"<br>");
		sb.append("Requires Level "+reqLevel+"<br>");
		sb.append("Item Level "+iLvl+"<br>");
		sb.append("<font color=12ff30>");
		for(int i = 5; i < 7; i++) if(stats[i] != 0) sb.append("Equip: Increases " + AppFrame.ATTRS[i].toLowerCase() + " by " + stats[i] + ".<br>");
		for(int i = 7; i < 13; i++) if(stats[i] != 0) sb.append("Equip: Increases " + AppFrame.ATTRS[i].toLowerCase() + " rating by " + stats[i] + ".<br>");
		for(int i = 13; i < AppFrame.ATTRNO - 1; i++) if(stats[i] != 0) sb.append("Equip: Increases your " + AppFrame.ATTRS[i].toLowerCase() + " rating by " + stats[i] + ".<br>");
		if(stats[AppFrame.ATTRNO - 1] != 0) sb.append("Equip: Restores " + stats[AppFrame.ATTRNO - 1] + " mana per 5 sec.<br>");
		if(!getEffect().isEmpty()) {
			if(getEffect().charAt(0) == '*') {
				sb.append("Use: " + getEffect().substring(1).replace("\n", "<br>") + "<br>");
			} else sb.append("Equip: " + getEffect().replace("\n", "<br>") + "<br>");
		}
		if(!getLore().isEmpty()) {
			sb.append("<font color=#fadc1e>"+ '"' + getLore().replace("\n", "<br>") + '"'+"<br>");
		}
		sb.append("<font color=white>");
		if(sellPrice > 0) {
			sb.append("Sell Price: ");
			if(sellPrice >= 10000) sb.append(sellPrice/10000+"<font color=#dde700>o <font color=white>");
			if(sellPrice >= 100) sb.append((sellPrice/100)%100+"<font color=#bfbfbf>o <font color=white>");
			sb.append(sellPrice%100+"<font color=#c68a00>o<br>");
		}
		return sb.toString();
	}
	public boolean hasSockets() {
		return sockets.length > 0;
	}
	public String getName() {
		return name;
	}
	public BodyPart getBodyPart() {
		return bodypart;
	}
	public int getArmor() {
		return armor;
	}
	public GemColor[] getSockets() {
		return sockets;
	}
	public int[] getStats() {
		return stats;
	}
	public String getEffect() {
		return effect;
	}
	public String getLore() {
		return lore;
	}
	public int getDurability() {
		return durability;
	}
	public int getReqLevel() {
		return reqLevel;
	}
	public int getILvl() {
		return iLvl;
	}
}