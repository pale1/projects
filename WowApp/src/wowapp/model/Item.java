package wowapp.model;

import java.io.Serializable;
import javax.swing.ImageIcon;

public abstract class Item implements Serializable {
	
	private static final long serialVersionUID = 1L;
	protected final String name;
	protected final String iconPath;
	protected final Quality quality;
	protected final int sellPrice;

	public Item(String name, String iconPath, Quality quality, int sellPrice) {
		this.name = name;
		this.iconPath = iconPath;
		this.quality = quality;
		this.sellPrice = sellPrice;
	}
	public String getName() {
		return name;
	}
	public String getIconPath() {
		return iconPath;
	}
	public ImageIcon getIcon() {
		ImageIcon icon = new ImageIcon(iconPath);
		return icon;
	}
	public Quality getQuality() {
		return quality;
	}
	public int getSellPrice() {
		return sellPrice;
	}
	@Override
	public abstract boolean equals(Object o);
	@Override
	public abstract int hashCode();
}
