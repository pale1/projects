package wowapp.model;

public enum Quality {
	Poor, Common, Uncommon, Rare, Epic, Legendary
}
