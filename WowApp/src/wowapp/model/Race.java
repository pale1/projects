package wowapp.model;

public enum Race {
	Human, Dwarf, NightElf, Gnome, Draenei, Orc, Undead, Tauren, Troll, BloodElf
}
