package wowapp.model;

public class Weapon extends Gear {

	private static final long serialVersionUID = 1L;
	private final int minDmg;
	private final int maxDmg;
	private final double speed;
	private final WeaponType weaponType;
	private final int block;
	public Weapon(String name, String iconPath, BodyPart bodypart, Quality quality, int sellPrice, int armor, GemColor[] sockets, int socketBonusStat, int socketBonusAmount, int[] stats, String effect, String lore, int durability, int reqLevel, int iLvl, int minDmg, int maxDmg, double speed, int block, WeaponType weaponType) {
		super(name, iconPath, bodypart, quality, sellPrice, armor, sockets, socketBonusStat, socketBonusAmount, stats, effect, lore, durability, reqLevel, iLvl);
		this.minDmg = minDmg;
		this.maxDmg = maxDmg;
		this.speed = speed;
		this.block = block;
		this.weaponType = weaponType;
	}
	@Override
	public String toString() {
		return super.toString();
	}
	public int getMinDmg() {
		return minDmg;
	}
	public int getMaxDmg() {
		return maxDmg;
	}
	public double getSpeed() {
		return speed;
	}
	public int getBlock() {
		return block;
	}
	public WeaponType getWeaponType() {
		return weaponType;
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof Weapon)
			return this.getName().equals(((Weapon) o).getName());
		return false;
	}
	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
