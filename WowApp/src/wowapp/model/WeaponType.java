package wowapp.model;

public enum WeaponType {
	Sword, Mace, Axe, Staff, Polearm, Shield, Dagger, Bow, Crossbow, Gun, Thrown, Wand
}
